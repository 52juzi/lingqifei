<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="ChannelCofig.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../Include/PW_OrderFlow.asp"-->
<%
TrueName = PE_HTMLEncode(GetForm("truename"))
U_Address = PE_HTMLEncode(GetForm("u_address"))
U_Phone = PE_HTMLEncode(GetForm("u_phone"))
U_Fax = PE_HTMLEncode(GetForm("u_fax"))
U_ZipCode = PE_HTMLEncode(GetForm("u_zipCode"))
U_Email = PE_HTMLEncode(GetForm("u_email"))
Remark = GetForm("remark")
If TrueName = "" Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请输入收货人姓名！</li>"
End If

If U_Address = "" Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请输入收货人地址！</li>"
End If

If U_Phone = "" Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请输入收货人电话！</li>"
Else
    If IsValidPhone(U_Phone) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>收货人电话格式错误！</li>"
    End If
End If
If U_Email <> "" Then
    If IsValidEmail(U_Email) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>收货人Email有错误！</li>"
    End If
End If
If FoundErr = True Then
    Call ShowErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

'判断购物车是否为空
CartID = ReplaceBadChar(Trim(Request.Cookies("Cart" & Site_Sn)("CartID")))
ProductList = SelectCart(CartID)
If ProductList = "" Then       '如果购物车为空，转入提示界面
	FoundErr = True
	ErrMsg = ErrMsg & "<li>您好！目前您的购物车中没有任何商品!</li>"
Else
    ProductList = ReplaceBadChar(ProductList)
End If
If FoundErr = True Then
    Call ShowErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Dim sqlCart, rsItem,trs
Dim OrderFormID, OrderFormNum
OrderFormID = GetNewID("PW_OrderForm", "OrderFormID")
OrderFormNum = GetNumString()
Dim rsOrder, sqlOrder, ItemID
sqlOrder = "select top 1 * from PW_OrderForm"
Set rsOrder = Server.CreateObject("adodb.recordset")
rsOrder.Open sqlOrder, Conn, 1, 3
rsOrder.AddNew
rsOrder("OrderFormID") = OrderFormID
rsOrder("OrderFormNum") = OrderFormNum
rsOrder("UserName") = UserName
rsOrder("ContacterName") = TrueName
rsOrder("Address") = U_Address
rsOrder("Phone") = U_Phone
rsOrder("ZipCode") = U_ZipCode
rsOrder("Email") = U_Email
rsOrder("Remark") = Remark
rsOrder("Remark") = Remark
rsOrder("InputTime") = date()
rsOrder.Update
rsOrder.Close
Set rsOrder = Nothing

Set rsItem = Server.CreateObject("adodb.recordset")
rsItem.Open "select top 1 * from PW_OrderFormItem", Conn, 1, 3
'开始向订单明细表中录入数据
Set rsCart = Conn.Execute("select P.*,C.Quantity from PW_Product P inner join PW_ShoppingCarts C on C.ProductID=P.ProductID where C.CartID='" & CartID & "' order by C.CartItemID desc")
Do While Not rsCart.EOF
    dblAmount = rsCart("Quantity")
    If dblAmount <= 0 Then dblAmount = 1
    
    '添加进订单详细表中
    ItemID = GetNewID("PW_OrderFormItem", "ItemID")
    rsItem.AddNew
    rsItem("ItemID") = ItemID
    rsItem("OrderFormID") = OrderFormID
    rsItem("ProductID") = rsCart("ProductID")
    rsItem("Amount") = dblAmount
    rsItem.Update
    rsCart.MoveNext
Loop
rsCart.Close
Set rsCart = Nothing
rsItem.Close
Set rsItem = Nothing


Call DelCart(CartID)

ErrMsg = "<P><FONT color=#ff0000>恭喜您，您的订单提交成功！</FONT><BR>您的订单号码是：<B>"& OrderFormNum &"</B>"
If FoundErr = False Then
    Call showSuccessMsg(ErrMsg, ComeUrl)
    Response.End
End If

Call CloseConn
%>
