<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="ChannelCofig.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../Include/PW_MD5.asp"-->
<!--#include file="../Include/PW_OrderFlow.asp"-->
<%


Dim trs, rsProductList

CartID = ReplaceBadChar(Trim(Request.Cookies("Cart" & Site_Sn)("CartID")))
If CartID = "" Or IsNull(CartID) Then
    CartID = MD5(Now() & Timer() & session.SessionID, 32)
End If
ProductID = GetValue("ProductID")
If IsValidID(ProductID) = False Then
    ProductID = ""
End If
Select Case Action
Case "Add"
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要购买的产品ID！</li>"
    Else
        ProductID = PE_CLng(ProductID)
        ProductList = SelectCart(CartID)
        Set trs = Conn.Execute("select ProductID from PW_Product where ProductID=" & ProductID & "")
        If trs.BOF And trs.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的商品！</li>"
		end if
        Set trs = Nothing
    End If
    If FoundErr = False Then
        Call AddToCart(CartID, ProductID, 1)
    End If
Case "Clear"
    Call DelCart(CartID)
Case "Modify"
    Call ModifyCart
Case "Payment"
    Call ModifyCart
    Response.Redirect "Payment.asp"
End Select

If FoundErr = True Then
    Call ShowErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Response.Cookies("Cart" & Site_Sn)("CartID") = CartID
Response.Cookies("Cart" & Site_Sn).Expires = Date + 365

ProductList = SelectCart(CartID)

If ProductList = "" Then       '如果购物车为空，转入提示界面
    ShowTips_CartIsEmpty = "<br><br><br><br>您好！目前您的购物车中没有任何商品，是不是<a href='index.asp'><font color='#FF0000'>继续购物</font></a>？<br><br><br><br><br><br>"
Else
    ShowTips_CartIsEmpty = ""
End If

strHtml = ReadFileContent(Template_Dir &"plus/ShopCart.html")
Call ReplaceCommonLabel
strHtml = PE_Replace(strHtml, "{$showtips_login}", ShowTips_Login)

strHtml = PE_Replace(strHtml, "{$showtips_cartisempty}", ShowTips_CartIsEmpty)
strHtml = PE_Replace(strHtml, "{$showcart}", ShowCart())
strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)

Response.Write strHtml
Call CloseConn

Sub ModifyCart()
	Call DelCart(CartID)
    If ProductID = "" Then
        FoundErr=True
		ErrMsg = "请指定ProductID！"
        Exit Sub
    End If
    Dim arrProductList
    Dim arrPresentList
    Dim trs, i
    arrProductList = Split(ProductID, ",")
    For i = 0 To UBound(arrProductList)
        dblAmount = Request("Amount_" & Trim(arrProductList(i)))
        If dblAmount = "" Then
            dblAmount = 1
        Else
            dblAmount = PE_CDbl(dblAmount)
        End If
        If dblAmount <= 0 Then dblAmount = 1

        Set trs = Conn.Execute("select ProductID from PW_Product where ProductID=" & arrProductList(i) & "")
        If Not (trs.BOF And trs.EOF) Then
        	Call AddToCart(CartID, arrProductList(i), dblAmount)
        End If
		trs.close
        Set trs = Nothing
    Next
End Sub
%>
