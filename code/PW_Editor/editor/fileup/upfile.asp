<!--#include file="../../../Sys_Start.asp"-->
<!--#include file="../../../Include/PW_FSO.asp"-->
<%
Const MaxTotalSize = 104857600    '上传数据限制，最大上传100M
Const NoAllowExt = "asa|asax|ascs|ashx|asmx|asp|aspx|axd|cdx|cer|config|cs|csproj|idc|licx|rem|resources|resx|shtm|shtml|soap|stm|vb|vbproj|vsdisco|webinfo|php"    '不允许上传类型(黑名单)
Const NeedCheckFileMimeExt = "gif|jpg|jpeg|jpe|bmp|png|swf|mid|mp3|wmv|asf|avi|mpg|ram|rm|ra|rar|exe|doc|zip"
Dim AdminLogined

If ObjInstalled_FSO = False Then
	SendUploadResults "1", "", "", "您的服务器不支持FSO，或者FSO已经改名，所以不能上传！"
End If	    
If CheckLogin() = False Then
	SendUploadResults "1", "", "", "请先登录！"
End If
Dim Forms, Files
Dim oUpFilestream   '上传的数据流

'********************************************
'以下代码是对提交的数据进行分析
'********************************************
Dim RequestBinDate, sSpace, bCrLf, sInfo, iInfoStart, iInfoEnd, tStream, iStart
Dim sFormValue, sFileName
Dim iFindStart, iFindEnd
Dim iFormstart, iFormEnd, sFormName
Dim FileInfo(6)
	'代码开始
If Request.TotalBytes < 1 Then  '如果没有数据上传
	SendUploadResults "1", "", "", "没有数据上传！"
End If
	
If Request.TotalBytes > MaxTotalSize Then  '如果上传的数据超出限制大小
	SendUploadResults "1", "", "", "上传的数据超出限制大小！"
End If
	
Set Forms = Server.CreateObject("Scripting.Dictionary")
Forms.CompareMode = 1
Set Files = Server.CreateObject("Scripting.Dictionary")
Files.CompareMode = 1
Set tStream = Server.CreateObject("ADODB.Stream")
Set oUpFilestream = Server.CreateObject("ADODB.Stream")
oUpFilestream.Type = 1
oUpFilestream.Mode = 3
oUpFilestream.Open
oUpFilestream.Write Request.BinaryRead(Request.TotalBytes)
oUpFilestream.Position = 0
RequestBinDate = oUpFilestream.Read
iFormEnd = oUpFilestream.size
bCrLf = ChrB(13) & ChrB(10)
'取得每个项目之间的分隔符
sSpace = LeftB(RequestBinDate, InStrB(1, RequestBinDate, bCrLf) - 1)
iStart = LenB(sSpace)
iFormstart = iStart + 2
'分解项目
Do
	iInfoEnd = InStrB(iFormstart, RequestBinDate, bCrLf & bCrLf) + 3
	tStream.Type = 1
	tStream.Mode = 3
	tStream.Open
	oUpFilestream.Position = iFormstart
	oUpFilestream.CopyTo tStream, iInfoEnd - iFormstart
	tStream.Position = 0
	tStream.Type = 2
	tStream.Charset = "gb2312"
	sInfo = tStream.ReadText
			
	'取得表单项目名称
	iFindStart = InStr(22, sInfo, "name=""", 1) + 6
	iFindEnd = InStr(iFindStart, sInfo, """", 1)
	sFormName = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
		
	iFormstart = InStrB(iInfoEnd, RequestBinDate, sSpace) - 1
	If InStr(45, sInfo, "filename=""", 1) > 0 Then   '如果是文件
		'取得文件属性
		iFindStart = InStr(iFindEnd, sInfo, "filename=""", 1) + 10
		iFindEnd = InStr(iFindStart, sInfo, """" & vbCrLf, 1)
		sFileName = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
		FileInfo(0) = sFormName
		FileInfo(1) = GetFileName(sFileName)
		FileInfo(2) = GetFilePath(sFileName)
		FileInfo(3) = GetFileExt(sFileName)
		iFindStart = InStr(iFindEnd, sInfo, "Content-Type: ", 1) + 14
		iFindEnd = InStr(iFindStart, sInfo, vbCr)
		FileInfo(4) = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
		FileInfo(5) = iInfoEnd
		FileInfo(6) = iFormstart - iInfoEnd - 2
		Files.Add sFormName, FileInfo
	Else    '如果是表单项目
		tStream.Close
		tStream.Type = 1
		tStream.Mode = 3
		tStream.Open
		oUpFilestream.Position = iInfoEnd
		oUpFilestream.CopyTo tStream, iFormstart - iInfoEnd - 2
		tStream.Position = 0
		tStream.Type = 2
		tStream.Charset = "gb2312"
		sFormValue = tStream.ReadText
		If Forms.Exists(sFormName) Then
			Forms(sFormName) = Forms(sFormName) & ", " & sFormValue
		Else
			Forms.Add sFormName, sFormValue
		End If
	End If
	tStream.Close
	iFormstart = iFormstart + iStart + 2
	'如果到文件尾了就退出
Loop Until (iFormstart + 2) >= iFormEnd
RequestBinDate = ""
Set tStream = Nothing
'********************************************
'数据分析结束
'********************************************
	
Dim SavePath, dirMonth, tmpPath
If fso.FolderExists(Server.MapPath(InstallDir)) = False Then fso.CreateFolder Server.MapPath(InstallDir)
Dim FileType,FieldName
FileType = LCase(Trim(Forms("Type")))
FieldName = Trim(Forms("NewFile"))
UpFileType = Replace(UpFileType," ","")

Dim arrFileType

arrFileType = Split(UpFileType, "$")
Select Case FileType
Case "image"
	UpFileType = Trim(arrFileType(0))
Case "flash"
	UpFileType = Trim(arrFileType(1))
Case Else
	UpFileType = Trim(arrFileType(0)) & "|" & Trim(arrFileType(1)) & "|" & Trim(arrFileType(2)) & "|" & Trim(arrFileType(3)) & "|" & Trim(arrFileType(4))
End Select	

SavePath = InstallDir & UploadDir& "/"
If fso.FolderExists(Server.MapPath(SavePath)) = False Then fso.CreateFolder Server.MapPath(SavePath)


Dim FileCount,EnableUpload,i
Dim strJS, dtNow,msg
Dim FormNames,oFilestream,oFileInfo
Dim cFileName, cFilePath, cFileExt, cFileMIME, cFileStart, cFileSize

FileCount = 0
FormNames = Files.Keys

For i = 0 To Files.Count - 1
	On Error Resume Next
	EnableUpload = False        
	dtNow = Now()
	oFileInfo = Files.Item(FormNames(i))
	cFileName = oFileInfo(1)
	cFilePath = oFileInfo(2)
	cFileExt = oFileInfo(3)
	cFileMIME = oFileInfo(4)
	cFileStart = oFileInfo(5)
	cFileSize = oFileInfo(6)
	If cFileSize < 10 Then
		SendUploadResults "1", "", "", "请先选择你要上传的文件！"
	Else
		If cFileSize > (MaxFileSize * 1024) Then
			SendUploadResults "1", "", "", "文件大小超过了限制，最大只能上传" & CStr(MaxFileSize) & "K的文件！"
		Else
			If CheckFileExt(UpFileType, cFileExt) = False Or CheckFileExt(NoAllowExt, cFileExt) = True Or IsValidStr(cFileExt) = False Then
				FoundErr = True
				If cFileName <> "" Then
					SendUploadResults "1", "", "", "这种文件类型不允许上传！\n\n只允许上传这几种文件类型：" & UpFileType
				End If
			Else
				If Left(LCase(cFileMIME), 5) = "text/" And CheckFileExt(NeedCheckFileMimeExt, cFileExt) = True Then
					SendUploadResults "1", "", "", "为了系统安全，不允许上传用文本文件伪造的图片文件！"
				Else
					EnableUpload = True
				End If
			End If
		End If
	End If
	If EnableUpload = True Then
		dirMonth = Year(dtNow) & Right("0" & Month(dtNow), 2) & "/"
		tmpPath = SavePath & dirMonth
		If fso.FolderExists(Server.MapPath(tmpPath)) = False Then fso.CreateFolder Server.MapPath(tmpPath)
		Randomize
		strFileName = GetNumString()
		tmpPath = tmpPath & strFileName & "." & cFileExt
		
		Set oFilestream = Server.CreateObject("ADODB.Stream")
		oFilestream.Type = 1
		oFilestream.Mode = 3
		oFilestream.Open
		oUpFilestream.Position = cFileStart
		oUpFilestream.CopyTo oFilestream, cFileSize
		oFilestream.SaveToFile Server.MapPath(tmpPath)   '保存文件
		oFilestream.Close
		Set oFilestream = Nothing
		
		FileCount = FileCount + 1
		
		SendUploadResults 0, tmpPath, "", ""
		
	End If
Next
Forms.RemoveAll
Set Forms = Nothing
Files.RemoveAll
Set Files = Nothing
oUpFilestream.Close
Set oUpFilestream = Nothing
Call ClearAspFile(SavePath & dirMonth)

' This is the function that sends the results of the uploading process.
Sub SendUploadResults( errorNumber, fileUrl, fileName, customMsg )
	Response.Clear
	Response.CodePage=936
	Response.Charset="gb2312"
	Response.Write "<script type=""text/javascript"">"
	Response.Write "(function(){var d=document.domain;while (true){try{var A=window.parent.document.domain;break;}catch(e) {};d=d.replace(/.*?(?:\.|$)/,'');if (d.length==0) break;try{document.domain=d;}catch (e){break;}}})();"
	
	Response.Write "window.parent.OnUploadCompleted(" & errorNumber & ",""" & fileUrl & """,""" &  fileName & """,""" & customMsg & """) ;"
	Response.Write "</script>"
	Response.End
End Sub

Function CheckLogin()
    Dim AdminName, AdminPassword, RndPassword
    Dim sqlUser,rsUser
    AdminName = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminName")))
    AdminPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminPassword")))
    RndPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("RndPassword")))
    
    If AdminName = "" Or AdminPassword = "" Or RndPassword = "" Then
        CheckLogin = False
    Else
        '验证管理员帐号及密码并检测是否为多人同时使用
        sqlUser = "select * from PW_Admin where AdminName='" & AdminName & "' and Password='" & AdminPassword & "'"
        Set rsUser = Conn.Execute(sqlUser)
        If rsUser.BOF And rsUser.EOF Then
            AdminLogined = False
        Else
            If rsUser("EnableMultiLogin") <> True And Trim(rsUser("RndPassword")) <> RndPassword Then
                AdminLogined = False
            Else
                AdminLogined = True
            End If
        End If
        rsUser.Close
        Set rsUser = Nothing
    End If
    If AdminLogined = True Then
        CheckLogin = True
        Exit Function
    End If
End Function
%>