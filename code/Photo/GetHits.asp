<%@language=vbscript codepage=936 %>
<%
Option Explicit

'强制浏览器重新访问服务器下载页面，而不是从缓存读取页面
Response.Buffer = True
Response.Expires = -1
Response.ExpiresAbsolute = Now() - 1
Response.Expires = 0
Response.CacheControl = "no-cache"

Dim ChannelID, PhotoID, Action, sql, rs, Hits
%>
<!--#include file="../conn.asp"-->
<!--#include file="ChannelConfig.asp"-->
<!--#include file="../Include/PW_Common_Security.asp"-->
<%
Call OpenConn

PhotoID = PE_CLng(GetUrl("PhotoID"))
Action = GetValue("Action")
If PhotoID = 0 Then
	Hits = 0
Else
	sql = "select Hits from PW_Photo where Deleted=" & PE_False & " and PhotoID=" & PhotoID & " and ChannelID=" & ChannelID & ""
	Set rs = server.CreateObject("ADODB.recordset")
	rs.open sql, Conn, 1, 3
	If rs.bof And rs.EOF Then
		Hits = 0
	Else
		Hits = rs("Hits") + 1
		rs("Hits") = Hits
		rs.Update
	End If
	rs.Close
	Set rs = Nothing
End If
Response.Write "document.write('" & Hits & "');"
Call CloseConn
%>
