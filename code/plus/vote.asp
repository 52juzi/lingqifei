<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
Dim ID, VoteType, VoteOption, arrOptions, sqlVote, rsVote, VoteNum
Dim Voted, VotedID, arrVotedID, i
ID = PE_CLng(GetValue("ID"))
VoteOption = ReplaceBadChar(GetForm("VoteOption"))
VotedID = ReplaceBadChar(Trim(Request.Cookies("VotedID")))
If IsValidID(VotedID) = False Then
    VotedID = ""
End If
If ID = 0 Then
    FoundErr = True
    ErrMsg = ErrMsg + "<li>不能确定调查ID</li>"
    Call ShowErrMsg(ErrMsg, ComeUrl)
    Response.End
End If
Voted = FoundInArr(VotedID, ID, ",")
If Action = "" Or VoteOption = "" Then
    Action = "Show"
End If


If Action = "Vote" And VoteOption <> "" And Voted = False Then
	set rsVote=conn.execute("Select VoteType,VoteNum from PW_Vote where ID="&ID)
	VoteType=rsVote("VoteType")
	VoteNum = rsVote("VoteNum")
	rsVote.close
	set rsVote=nothing
    If VoteType = 0 Then
        VoteOption = PE_CLng(VoteOption)
        Conn.Execute "Update PW_Vote set answer" & VoteOption & "= answer" & VoteOption & "+1 where ID=" & ID
    Elseif VoteType = 1 then 
        If InStr(VoteOption, ",") > 0 Then
            arrOptions = Split(VoteOption, ",")
			if UBound(arrOptions)+1>VoteNum and VoteNum<>0 then
				FoundErr = True
				ErrMsg = ErrMsg & ("<li>最多只能选"& VoteNum &"项</li>")
				Call ShowErrMsg(ErrMsg, ComeUrl)
				rsVote.Close
				Set rsVote = Nothing
				Response.End
			end if
            For i = 0 To UBound(arrOptions)
                Conn.Execute "Update PW_Vote set answer" & PE_CLng(Trim(arrOptions(i))) & "= answer" & PE_CLng(Trim(arrOptions(i))) & "+1 where ID=" & ID
            Next
        Else
            Conn.Execute "Update PW_Vote set answer" & PE_CLng(VoteOption) & "= answer" & PE_CLng(VoteOption) & "+1 where ID=" & ID
        End If
    End If
    If VotedID = "" Then
        VotedID = Trim(ID)
    Else
        VotedID = VotedID & "," & ID
    End If
    Response.Cookies("VotedID") = VotedID
End if

Set rsVote = Conn.Execute("Select * from PW_Vote Where ID=" & ID)
If rsVote.BOF Or rsVote.EOF Then
    FoundErr = True
    ErrMsg = ErrMsg & ("<li>找不到相应的调查</li>")
    Call ShowErrMsg(ErrMsg, ComeUrl)
    rsVote.Close
    Set rsVote = Nothing
    Response.End
End If

Dim TotalVote
TotalVote = 0
For i = 1 To 20
    If IsNull(rsVote("Select" & i)) Or rsVote("Select" & i) = "" Then Exit For
    TotalVote = TotalVote + PE_CLng(rsVote("answer" & i))
Next

Dim strVoteTitle
strHtml = ReadFileContent(Template_Dir &"plus/vote.html")

Call ReplaceCommonLabel
Call GetModelFront_Html
strHtml = PE_Replace(strHtml, "{$pagetitle}", "网站调查_" & sitename)
strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;网站调查")
strVoteTitle = rsVote("Title")
strHtml = PE_Replace(strHtml, "{$votetitle}", strVoteTitle)
strHtml = PE_Replace(strHtml, "{$votetotal}", TotalVote)

If TotalVote = 0 Then TotalVote = 1

Dim strVoteItems, strVoteItem, strTemp, perVote, lngTemp
regEx.Pattern = "\【voteitem\】([\s\S]*?)\【\/voteitem\】"
Set Matches = regEx.Execute(strHtml)
For Each Match In Matches
    strTemp = Match.value
Next

For i = 1 To 20
    If Trim(rsVote("Select" & i) & "") = "" Then Exit For
    lngTemp = PE_CLng(rsVote("answer" & i))
    perVote = Round(lngTemp / TotalVote, 4)
    strVoteItem = PE_Replace(PE_Replace(strTemp, "【voteitem】", ""), "【/voteitem】", "")
    strVoteItem = PE_Replace(strVoteItem, "{$itemnum}", i)
    strVoteItem = PE_Replace(strVoteItem, "{$itemtitle}", rsVote("Select" & i))
    strVoteItem = PE_Replace(strVoteItem, "{$itemnum}", lngTemp)
    strVoteItem = PE_Replace(strVoteItem, "{$itemper}", perVote * 100)
    strVoteItem = PE_Replace(strVoteItem, "{$itemwidth}", CLng(500 * perVote))
    strVoteItems = strVoteItems & strVoteItem
Next
strHtml = PE_Replace(strHtml, strTemp, strVoteItems)
rsVote.Close
Set rsVote = Nothing

Response.Write strHtml
Call CloseConn
%>