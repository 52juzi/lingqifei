<!--#include file="../Sys_Start.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../Include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
Dim ID,rsdb
 
ID=PE_Clng(GetValue("ID"))
If IsValidID(ID) = False Then
    ID = ""
End If

If ID = "" Then
    Call ShowErrMsg("参数错误", ComeUrl)
	Response.End
End If

set rsdb=conn.execute("Select * from PW_Info where ID="&ID)
if rsdb.eof and rsdb.bof then
    Call ShowErrMsg("内容不存在", ComeUrl)
	Response.End	
end if


templateFile_Url = PE_Replace(rsdb("templateInfo"),"{@TemplateDir}",Template_Dir)
strHtml = ReadFileContent(templateFile_Url)


''替换内容里的图片地址
strHtml = PE_Replace(strHtml,"{$field(content)}",GetContent(rsdb("Content")))
strHtml = PE_Replace(strHtml,"{$pagetitle}",rsdb("Title")&"_"& SiteName)
strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;" & rsdb("Title"))
strHtml = PE_Replace(strHtml,"{$meta_keywords}",rsdb("Meta_Keywords"))
strHtml = PE_Replace(strHtml,"{$meta_description}",rsdb("Meta_Description"))

regEx.Pattern = "\{\$field\((.*?)\)\}"

Set Matches = regEx.Execute(strHtml)
For Each Match In Matches
	strHtml = PE_Replace(strHtml,Match.value,rsdb(Match.SubMatches(0)))
next

Call ReplaceCommonLabel
Call GetModelFront_Html
Response.Write strHtml
Call CloseConn

%>