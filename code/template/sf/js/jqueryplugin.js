﻿/* ------------
Titlte:jQuery插件封装
Author：xiaozj
Created：2012-2-27
Updated：2012-2-27 xiaozj
------------ */
(function($) {
	/**************************
	Title:jQuery插件 - 焦点图轮播	

	** @param nav       : 轮显导航的形式，可选择[number|纯数字, thumb|缩略图, mix|混合型在页面中预加载, none|无]
	** @param effect    : 动画形式，可选择[fade|渐隐渐变, move|平移]
	** @param direction : 方向，仅在effect为move时有效，可选择[left, up]

	<div class="slider" id="J_Slider">
		<div class="slider-content">
			<ul class="items">
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			</ul>
		</div>
	</div>
	$("#J_Slider").slider({});
	**************************/
	$.fn.slider = function(option) {
		var setting = {
			nav                 : "number",
			navItemSelector     : "li",
			contentItemSelector : "li",
			effect              : "move",
			direction           : "left",
			navAction           : "mouseover",
			displayTitle        : false,
			displayNavNumber    : false,
			intervalTime        : 4000,
			speed               : 500
		};
		return this.each(function() {
			var opts          = $.extend({}, setting, option);			
			var $this         = $(this);
			var $content      = $this.find(".slider-content");
			var $contentItems = $content.find(opts.contentItemSelector);
			var n             = $contentItems.size();
			var w             = $contentItems.width();
			var h             = $contentItems.height();
			var x             = (opts.direction == "left") ? n : 1;
			var y             = (opts.direction == "left") ? 1 : n;
			var interval      = 0;
			var current       = 0;
			var $nav          = null;
			var $title        = null;
			
			var init = function() {
				$contentItems.each(function() {
					var _this = $(this);
					_this.find("img").attr("src", _this.find("img").attr("data-src"));
					if (opts.effect === "fade") _this.css("z-index", n - _this.index());
				});
				if (opts.displayTitle) $title = $('<div class="slider-title"><span>' + $contentItems.eq(0).find("img").attr("alt") + '</span></div>').appendTo($this).fadeTo(1, 0.6);
				$content.show();
				showNav();
				startInterval();				
			};

			var startInterval = function() {
				interval = setInterval(function() {
					if (current == n-1) current = -1;
					play(++current);
				}, opts.intervalTime);
			}

			var showNav = function() {
				var _navHTML = "";
				switch ( opts.nav ) {
					case "number" :
						_navHTML = '<div class="slider-nav"><ol class="items">';
						for (var i=0; i < n; i++) {
							if (opts.displayNavNumber) {
								_navHTML += "<li>" + (i+1) + "</li>";
							} else {
								_navHTML += "<li></li>";
							}
						}
						_navHTML += "</ol></div>";
						$nav = $(_navHTML).appendTo($this);
						break;
					case "thumb" :
						_navHTML = '<div class="slider-nav"><ol class="items">';
						for (var i=0; i < n; i++) {
							_navHTML += '<li><img src="'+ $contentItems.eq(i).find("img").attr("data-thumb") +'" /></li>';
						}
						_navHTML += "</ol></div>";
						$nav = $(_navHTML).appendTo($this);
						break;
					case "mix" :
						$nav = $this.find(".slider-nav").fadeIn();
						break;
				};
				if ($nav) $nav.delegate(opts.navItemSelector, opts.navAction, navHandle).find(opts.navItemSelector).eq(0).addClass("c");
			};

			var navHandle = function() {
				var index = $(this).index();
				if (index == current) return;
				clearInterval(interval);
				play(index);
				current = index;
				startInterval();
			};

			var play = function(c) {				
				if (opts.effect == "fade") {
					$contentItems.eq(c).stop(true, true).fadeIn(opts.speed).siblings(opts.contentItemSelector).stop(true, true).fadeOut(opts.speed);
				} else {
					if (opts.direction == "left") {
						$content.find(".items").stop(true, true).animate({ "margin-left": "-" + w * c + "px" }, opts.speed);
					} else {
						$content.find(".items").stop(true, true).animate({ "margin-top": "-" + h * c + "px" }, opts.speed);
					}
				}
				if (opts.nav != "none") $nav.find(opts.navItemSelector).removeClass("c").eq(c).addClass("c");
				if (opts.displayTitle) $title.html("<span>" + $contentItems.eq(c).find("img").attr("alt") + "</span>");
			};

			init();
		});
	};

	
	/**************************
	Title:jQuery插件 - TAB切换	

	** @param

	<div id="J_Tab">
		<ul class='tab-nav'>
			<li class='tab-nav-item'></li>
			<li class='tab-nav-item'></li>
			<li class='tab-nav-item'></li>
		</ul>
		<div class='tab-content-item'></div>
		<div class='hide tab-content-item'></div>
		<div class='hide tab-content-item'></div>
	</div>
	***************************/
	$.fn.tab = function(option) {
		var setting = {
			navSelector         : ".tab-nav",
			navItemSelector     : ".tab-nav-item",
			contentItemSelector : ".tab-content-item"
		};
		return this.each(function() {
			if (option) var opts = $.extend({}, setting, option);
			var $this         = $(this);
			var $nav          = $this.find(opts.navSelector);
			var $contentItems = $this.find(opts.contentItemSelector);

			var clickHandle = function() {
				var _this = $(this);
				var index = _this.index();
				_this.addClass("c").siblings().removeClass("c");
				$contentItems.addClass("hide").eq(index).removeClass("hide");
			};

			$nav.delegate(opts.navItemSelector, "click", clickHandle);
		});
	};


	/***************************
	Title:jQuery插件 - marquee

	** @param loop      : 是否循环 [0, 1]
	** @param direction : 方向，可选择[left, up]

	<div class="list" id="J_Marquee">
		<ul class="items">
			<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			<li><a href="" target="_blank"><img src="" alt="" /></a></li>
		</ul>
	</div>
	$("#J_Marquee").marquee({});
	****************************/
	$.fn.marquee = function(option) {
		var setting = {
			loop: 0,
			direction: 'left',
			scrollAmount: 1,
			scrollDelay: 30,
			wrapSelector : "ul",
			itemSelector : "li"
		};
		return this.each(function() {
			if (option) var opts = $.extend({}, setting, option);

			var $this    = $(this);
			var $wrap    = $this.find(opts.wrapSelector);
			var $item    = $wrap.find(opts.itemSelector);
			var distance = 0;
			var size     = (opts.direction == "left") ? $this.width() : $this.height();

			$item.each(function(i) {
				distance += (opts.direction == "left") ? $(this).outerWidth() : $(this).outerHeight();
			})
			if (distance <= size) return;
			$wrap.append($item.clone()).css((opts.direction == "left") ? "width" : "height", distance * 2.5 + "px");
			function move() {
				if (opts.direction == "left") {
					$this.scrollLeft($this.scrollLeft() + opts.scrollAmount);
					if ($this.scrollLeft() >= distance) {
						$wrap.find("li:lt(" + $item.size() + ")").appendTo($wrap);
						$this.scrollLeft(0);
					}
				} else {
					$this.scrollTop($this.scrollTop() + opts.scrollAmount);
					if ($this.scrollTop() >= distance) {
						$wrap.find("li:lt(" + $item.size() + ")").appendTo($wrap);
						$this.scrollTop(0);
					}
				}
			}
			var play = setInterval(move, opts.scrollDelay);

			$wrap.delegate(opts.itemSelector, "hover", function(e) {
				if (e.type === "mouseenter") {
					clearInterval(play);
				} else {
					clearInterval(play);
					play = setInterval(move, opts.scrollDelay);					
				}
			});
		});
	};


	/***************************
	Titlte:jQuery插件 - clickScroll
	** @param auto  : 是否自动滚动[true, false]

	<div id="J_Scrolling">
		<div class="list">
			<ul class="items">
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
				<li><a href="" target="_blank"><img src="" alt="" /></a></li>
			</ul>
		</div>
		<em class="nav nav-prev J_Prev"></em><em class="nav nav-next J_Next"></em>
	</div>
	$("#J_Scrolling").clickScroll({});
	***************************/
	$.fn.clickScroll = function(option) {
		var setting = {
			speed : 500,
			wrapSelector : "ul",
			itemSelector : "li",
			auto : true
		};
		return this.each(function() {
			var $this = $(this);
			if (option) var opts = $.extend({}, setting, option);
			var $prev    = $this.find(".J_Prev");
			var $next    = $this.find(".J_Next");
			var $wrap    = $this.find(opts.wrapSelector);
			var $items   = $this.find(opts.itemSelector);
			var distance = $items.outerWidth(true);
			var interval = 0;

			if (opts.auto) {
				interval = setInterval(function() {
					$prev.click();
				}, 3000);
			}

			var prevClickHandle = function() {
				clearInterval(interval);
				$wrap.stop(true, true).animate({
					"margin-left" : "-" + distance + "px"
				}, opts.speed, function() {
					$wrap.css("margin-left", 0).find("li:first").appendTo($wrap);
					if (opts.auto) {
						interval = setInterval(function() {
							$prev.click();
						}, 3000)
					}
				});
			};
			var nextClickHandle = function() {
				clearInterval(interval);
				$wrap.css("margin-left", "-" + distance + "px").find("li:last").prependTo($wrap);
				$wrap.stop(true, true).animate({
					"margin-left" : 0
				}, opts.speed, function() {
					if (opts.auto) {
						interval = setInterval(function() {
							$next.click();
						}, 3000)
					}
				});
			};

			$wrap.width( $items.size() * distance );
			$prev.bind("click", prevClickHandle);
			$next.bind("click", nextClickHandle);
		});
	};


	/***************************
	Titlte:jQuery插件 - 文本框占位符提示

	<input type="text" id="J_Inputbox" tips="这里是文本提示" />
	$("#J_Inputbox").inputPlaceHolder({});
	***************************/
	$.fn.inputPlaceHolder = function(option) {
		var setting = {
			fontsize : "12px",
			fontcolor : "#ccc"
		};
		return this.each(function(i) {
			var $this = $(this);
			if (option) {
				var opts = $.extend({}, setting, option);
			}
			var t = $this.attr("tips");
			if (!t) return; // 没有tips属性则退出
			var _l = $this.offset().left;
			var _t = $this.offset().top;
			var $tips = $("<div>" + t + "</div>").appendTo($("body"));
			if ($.trim($this.val())) $tips.hide(); // 防止Firefox刷新记忆功能
			$tips.css({
				"position" : "absolute",
				"left" : parseInt(_l) + 5,
				"top" : _t,
				"height" : $this.outerHeight() + "px",
				"line-height" : $this.outerHeight() + "px",
				"width" : $this.width(),
				"overflow" : "hidden",
				"color" : opts.fontcolor,
				"font-size" : opts.fontsize,
				"font-family" : "\\5b8b\\4f53"
			});
			$this.bind({
				focus : function() {
					$tips.hide();
				},
				blur : function() {
					if (!$.trim($this.val())) $tips.show(); // 失去焦点时判断是否有输入，没有则恢复TIPS
				},
				keyup : function() {
					if (!!$.trim($this.val())) $tips.hide();
				}
			});
			$tips.bind("click focus", function() {
				$this.focus(); // 解决IE点击TIPS时文本框未获得焦点
			});
		});
	};


	/***************************
	Titlte:jQuery插件 - 图片高度控自适应

	** @param : boxWidth 容器宽度
	** @param : boxHeight 容器高度
	** @param : isFill 是否填充容器
	***************************/
	$.fn.resizeImage = function(option) {
		var setting = {
			boxWidth : 100,
			boxHeight : 100,
			isFill : true
		};
		return this.each(function(i) {
			var $this = $(this);
			if (option) var opts = $.extend({}, setting, option);
			if ("img" != $this.get(0).tagName.toLowerCase()) return;
			var _tempImg    = new Image();
			_tempImg.src    = $this.attr("src");
			var _tempWidth  = _tempImg.width;
			var _tempHeight = _tempImg.height;
			if ( _tempWidth / _tempHeight >= opts.boxWidth / opts.boxHeight ) {
				if ( opts.isFill ) {
					$this.width(opts.boxHeight * _tempWidth / _tempHeight).height(opts.boxHeight);
				} else {
					if ( _tempWidth > opts.boxWidth ) {
						$this.width(opts.boxWidth).height(opts.boxWidth * _tempHeight / _tempWidth);
					}
				}
			} else {				
				if ( opts.isFill ) {
					$this.width(opts.boxWidth).height(opts.boxWidth * _tempHeight / _tempWidth);
				} else {	
					if ( _tempHeight > opts.boxHeight ) {
						$this.width(opts.boxHeight * _tempWidth / _tempHeight).height(opts.boxHeight);
					}
				}
			}
		});
	};
})(jQuery);