function $(id)
{
	return document.getElementById(id);	
}
function lTrim(str)
{
  if (str.charAt(0) == " ")
  {
    //如果字串左边第一个字符为空格
    str = str.slice(1);//将空格从字串中去掉
    //这一句也可改成 str = str.substring(1, str.length);
    str = lTrim(str);    //递归调用
  }
  return str;
}

//去掉字串右边的空格
function rTrim(str)
{
  var iLength;
  
  iLength = str.length;
  if (str.charAt(iLength - 1) == " ")
  {
    //如果字串右边第一个字符为空格
    str = str.slice(0, iLength - 1);//将空格从字串中去掉
    //这一句也可改成 str = str.substring(0, iLength - 1);
    str = rTrim(str);    //递归调用
  }
  return str;
}

//去掉字串两边的空格
function trim(str)
{
  return lTrim(rTrim(str));
}
//非空验证
// 参数 el_name 需要验证的表单的名称
// 参数 el_title 标题名称
// 参数 el_error 出错提示的表单的名称

function isNotEmpty(el_name,el_title,el_error)
{
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if (trim(e.value)==""){
		err.innerHTML=el_title+"不能为空!";
		e.className="input_error";  
		e.focus();
		return false;
	}else{
		return true;
	}
}

//检查Email地址
function checkEmail(el_name,el_title,el_error) {
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if (e.value=="")
	{
		return true;
	}
	if(!validateEmailByReg(e.value)) {
		err.innerHTML=el_title+"不合法，请重新填写！";
		e.className="input_error";
		e.focus();
		return false;
	}
	return true;
}
function validateEmailByReg(str){
    return(/^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(str));
}

//判断是否是数字 
function checkIsInteger(el_name,el_title,el_error)
{
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if(e.value==""){
		return true;
	}
	if(/^(\-?)(\d+)$/.test(e.value)){
		return true;
	}
	else{
		err.innerHTML=el_title+"不合法，请重新填写！";
		e.className="input_error";
		e.focus();
		return false;
	}
	
}

//判断是否为电话
function isTel(el_name,el_title,el_error)     
{
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if(e.value==""){
		return true;
	}
	var   patrn=/^([\d-_+]*)$/ ;     
	if(!patrn.exec(e.value)){
		err.innerHTML=el_title+"不合法，请重新填写！";
		e.className="input_error";
		e.focus();
	  return   false;
	}
	else{
	  return   true  
	}
}  

//判断是否为验证码
function IsCheckCode(el_name,el_title,el_error){
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if (e.value==""){
		err.innerHTML=el_title+"不能为空!";
		e.className="input_error";  
		e.focus();
		return false;
	}else if(e.value.length!=6){
		err.innerHTML="请输入正确的"+ el_title +"!";
		e.className="input_error";  
		e.focus();
		return false;
	}else{
		return true	
	}
}    

// 密码验证
// 参数 el_name1 需要验证的表单的名称
//      el_name2 标题名称
//      el_title 出错提示
//      el_error 出错的显示名称
function isSamePass(el_name1,el_name2 ,el_error)
{
	var e1=$(el_name1);
	var e2=$(el_name2);
	var err=$(el_error);
	err.innerHTML="";
	el_name2.className="input_pass";  
	if (trim(e1.value) != trim(e2.value) ){
		err.innerHTML="两次密码输入不一样！";
		e2.className="input_error";  
		e2.focus();
		return false;
	}else{
		return true;
	}
}
//获取单选框的值

function GetRadio(el_name){
	var sel = "";
	for (var i = 0; i < document.getElementsByName(el_name).length; i++)
	　{
	　　if(document.getElementsByName(el_name)[i].checked)
	　　　{
	　　　　sel = document.getElementsByName(el_name)[i].value;
	　　}
	　}
	return sel;
}

function isSelected(el_name,el_title,el_error)
{
	var e=$(el_name);
	var err=$(el_error);
	err.innerHTML="";
	e.className="input_pass";  
	if (trim(e.value)==""){
		err.innerHTML=el_title+"必须选择!";
		e.className="input_error";  
		return false;
	}else{
		return true;
	}
}


function refreshimg(Installdir){
  document.all.check_Code.src=Installdir+'Inc/CheckCode.asp?'+Math.random();
}

function checkLogin(){
	var f=document.userlogin;
	if(trim(f.UserId.value)==""){
		alert("用户名不能为空！");
		f.UserId.focus();
		return false;
	}
	if(trim(f.UserPass.value)==""){
		alert("密码不能为空！");
		f.UserPass.focus();
		return false;
	}
	if(trim(f.CheckCode.value)==""){
		alert("验证码不能为空！");
		f.CheckCode.focus();
		return false;
	}
	if(f.CheckCode.value.length != 6){
		alert("请输入6位验证码字符！");
		f.CheckCode.selected;
		f.CheckCode.focus();
		return false;
	}
}