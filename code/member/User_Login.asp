<!--#include file="../Sys_Start.asp"-->
<!--#include file="../Include/PW_MD5.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
'Call Logout
If Action = "ChkLogin" then
 	Call ChkLogin
	If FoundErr = True Then
		Call ShowErrMsg(ErrMsg, ComeUrl)
		Response.End()
	End If
	Call ShowSuccessMsg("登录成功！", InstallDir&"index.asp")
Else
	Dim strParameter,strList,strTemp
	UserID=PE_Clng(GetValue("UserID"))
	strFileName="User_login.asp"
	
	if UserID>0 then
		strFileName=strFileName&"?UserID="&UserID
	End if
	
	strHtml = ReadFileContent(Template_Dir &"member/login.html")
	
	Call ReplaceCommonLabel
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "用户登录_"&sitename)
	strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;用户登录")
	Call GetModelFront_Html
	Response.Write strHtml
	
	Call CloseConn
end If



Sub ChkLogin()
    Dim sql, rs
    Dim UserName, Password, CheckCode, RndPassword, AdminLoginCode
    UserName = ReplaceBadChar(GetForm("UserName"))
    Password = ReplaceBadChar(GetForm("Password"))
	
    If UserName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>用户名不能为空！</li>"
    End If
    If Password = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>密码不能为空！</li>"
    End If

    If FoundErr = True Then
        Exit Sub
    End If
    
    ComeUrl = Trim(Request.ServerVariables("HTTP_REFERER"))

    Password =  MD5(Password, 16)
    Set rs = Server.CreateObject("adodb.recordset")
    sql = "select * from PW_User where UserPassword='" & Password & "' and UserName='" & UserName & "'"
    rs.Open sql, Conn, 1, 3
    If rs.bof And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>用户名或密码错误！！！</li>"
    Else
        If Password <> rs("UserPassword") Then
            FoundErr = True
            ErrMsg = ErrMsg & "<br><li>用户名或密码错误！！！</li>"
        End If
    End If
    If FoundErr = True Then
        Session("UserName") = ""
        Session("UserPassword") = ""
        Session("RndPassword") = ""
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If
    RndPassword = GetRndPassword(16)
    rs("LastLoginIP") = UserTrueIP
    rs("LastLoginTime") = Now()
    rs("LoginTimes") = rs("LoginTimes") + 1
    rs.Update
    Response.Cookies(Site_Sn)("UserName") = rs("UserName")
    Response.Cookies(Site_Sn)("UserPassword") = rs("UserPassword")
    rs.Close
    set rs = Nothing

    Call CloseConn
End Sub


Function GetUserGroup_Option(CurrentGroupID)
    Dim strGroup, rsGroup
    Set rsGroup = Conn.Execute("select GroupID,GroupName from PW_UserGroup order by GroupID asc")
    Do While Not rsGroup.EOF
        strGroup = strGroup & "<option value='" & rsGroup(0) & "'"
        If rsGroup(0) = CurrentGroupID Then
            strGroup = strGroup & " selected"
        End If
        strGroup = strGroup & ">" & rsGroup(1) & "</option>"
        rsGroup.MoveNext
    Loop
    rsGroup.Close
    Set rsGroup = Nothing
    
    GetUserGroup_Option = strGroup
End Function

Sub Logout()
    Response.Cookies(Site_Sn)("UserName") = ""
    Response.Cookies(Site_Sn)("UserPassword") = ""
    'Response.Redirect strInstallDir&"Index.asp"
End Sub


%>