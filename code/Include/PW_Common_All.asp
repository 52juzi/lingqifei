<%
'判断当前访问者是否已经登录，若已登录，则读取数据并做必要赋值
Function CheckUserLogined()
    Dim rsUser, sqlUser
    UserID = 0
    GroupID = 0
    LoginTimes = 0
    CheckUserLogined = False
    UserName = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("UserName")))
    UserPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("UserPassword")))
	If (UserName = "" Or UserPassword = "") Then
        CheckUserLogined = False
        Exit Function
    End If
    
    sqlUser = "SELECT UserID,UserName,GroupID,LoginTimes FROM PW_User WHERE UserName='" & UserName & "' AND UserPassword='" & UserPassword & "'"
    Set rsUser = Conn.Execute(sqlUser)
    If rsUser.BOF And rsUser.EOF Then
        CheckUserLogined = False
    Else
        UserName = rsUser("UserName")
        UserID = rsUser("UserID")
        GroupID = rsUser("GroupID")
        LoginTimes = rsUser("LoginTimes")
        CheckUserLogined = True
    End If
    Set rsUser = Nothing
End Function

'给用户的相应变量赋值
Sub GetUser(sUserID)
    Dim rsUser, rsGroup
	sUserID = PE_Clng(sUserID)
    Set rsUser = Conn.Execute("SELECT * FROM PW_User Where UserID=" & sUserID & "")
    If Not (rsUser.BOF And rsUser.EOF) Then
        UserID = rsUser("UserID")
        GroupID = rsUser("GroupID")
		TrueName = rsUser("TrueName")
		Sex = rsUser("Sex")
		U_Address = rsUser("Address")
		U_Phone = rsUser("Phone")
		U_Fax = rsUser("Fax")
		U_Email = rsUser("Email")
		U_ZipCode = rsUser("ZipCode")
		RegTime = rsUser("RegTime")
		LoginTimes = rsUser("LoginTimes")
		LastLoginTime = rsUser("LastLoginTime")
		LastLoginIP = rsUser("LastLoginIP")
        Set rsGroup = Conn.Execute("select * from PW_UserGroup where GroupID=" & GroupID & "")
        GroupName = rsGroup("GroupName")
        GroupIntro = rsGroup("GroupIntro")
        rsGroup.Close
        Set rsGroup = Nothing
		
		Select Case Sex
			Case 1
				Sexstr = "男"
			case 2
				Sexstr = "女"
			case else
				Sexstr = "保密"
		end select
    End If
	rsUser.close
    Set rsUser = Nothing
End Sub


'**************************************************
'函数名：GetSubStr
'作  用：截字符串，汉字一个算两个字符，英文算一个字符
'参  数：str   ----原字符串
'        strlen ----截取长度
'        bShowPoint ---- 是否显示省略号
'返回值：截取后的字符串
'**************************************************
Function GetSubStr(ByVal str, ByVal strlen, bShowPoint)
    If IsNull(str) Or str = ""  Then
        GetSubStr = ""
        Exit Function
    End If
    Dim l, t, c, i, strTemp
    str = Replace(Replace(Replace(Replace(str, "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
    l = Len(str)
    t = 0
    strTemp = str
    strlen = PE_CLng(strlen)
    For i = 1 To l
        c = Abs(Asc(Mid(str, i, 1)))
        If c > 255 Then
            t = t + 2
        Else
            t = t + 1
        End If
        If t >= strlen Then
            strTemp = Left(str, i)
            Exit For
        End If
    Next
    str = Replace(Replace(Replace(Replace(str, " ", "&nbsp;"), Chr(34), "&quot;"), ">", "&gt;"), "<", "&lt;")
    strTemp = Replace(Replace(Replace(Replace(strTemp, " ", "&nbsp;"), Chr(34), "&quot;"), ">", "&gt;"), "<", "&lt;")
    If strTemp <> str And bShowPoint = True Then
        strTemp = strTemp & "…"
    End If
    GetSubStr = strTemp
End Function
'**************************************************
'函数名：GetStrLen
'作  用：求字符串长度。汉字算两个字符，英文算一个字符。
'参  数：str  ----要求长度的字符串
'返回值：字符串长度
'**************************************************
Function GetStrLen(str)
    On Error Resume Next
    Dim WINNT_CHINESE
    WINNT_CHINESE = (Len("中国") = 2)
    If WINNT_CHINESE Then
        Dim l, t, c
        Dim i
        l = Len(str)
        t = l
        For i = 1 To l
            c = Asc(Mid(str, i, 1))
            If c < 0 Then c = c + 65536
            If c > 255 Then
                t = t + 1
            End If
        Next
        GetStrLen = t
    Else
        GetStrLen = Len(str)
    End If
    If Err.Number <> 0 Then Err.Clear
End Function

Function Charlong(ByVal str)
    If str = "" Then
        Charlong = 0
        Exit Function
    End If
    str = Replace(Replace(Replace(Replace(str, "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
    
    Charlong = GetStrLen(str)
End Function

'**************************************************
'函数名：JoinChar
'作  用：向地址中加入 ? 或 &
'参  数：strUrl  ----网址
'返回值：加了 ? 或 & 的网址
'**************************************************
Function JoinChar(ByVal strUrl)
    If strUrl = "" Then
        JoinChar = ""
        Exit Function
    End If
    If InStr(strUrl, "?") < Len(strUrl) Then
        If InStr(strUrl, "?") > 1 Then
            If InStr(strUrl, "&") < Len(strUrl) Then
                JoinChar = strUrl & "&"
            Else
                JoinChar = strUrl
            End If
        Else
            JoinChar = strUrl & "?"
        End If
    Else
        JoinChar = strUrl
    End If
End Function

'**************************************************
'函数名：ShowPage
'作  用：显示“上一页 下一页”等信息
'参  数：sFileName  ----链接地址
'        TotalNumber ----总数量
'        MaxPerPage  ----每页数量
'        CurrentPage ----当前页
'        ShowTotal   ----是否显示总数量
'        ShowAllPages ---是否用下拉列表显示所有页面以供跳转。
'        strUnit     ----计数单位
'        ShowMaxPerPage  ----是否显示每页信息量选项框
'返回值：“上一页 下一页”等信息的HTML代码
'**************************************************
Function ShowPage(sfilename, totalnumber, MaxPerPage, CurrentPage, ShowTotal, ShowAllPages, strUnit, ShowMaxPerPage)
    Dim TotalPage, strTemp, strUrl, i

    If totalnumber = 0 Or MaxPerPage = 0 Or IsNull(MaxPerPage) Then
        ShowPage = ""
        Exit Function
    End If
    If totalnumber Mod MaxPerPage = 0 Then
        TotalPage = totalnumber \ MaxPerPage
    Else
        TotalPage = totalnumber \ MaxPerPage + 1
    End If
    If CurrentPage > TotalPage Then CurrentPage = TotalPage
    strTemp = "<div class=""show_page"">"
    If ShowTotal = True Then
        strTemp = strTemp & "共 <b>" & totalnumber & "</b> " & strUnit & "&nbsp;&nbsp;&nbsp;"
    End If
    
    If ShowMaxPerPage = True Then
        strUrl = JoinChar(sfilename) & "MaxPerPage=" & MaxPerPage & "&"
    Else
        strUrl = JoinChar(sfilename)
    End If
    If CurrentPage = 1 Then
        strTemp = strTemp & "首页 | 上一页 |"
    Else
        strTemp = strTemp & "<a href='" & strUrl & "page=1'>首页</a> |"
        strTemp = strTemp & "  <a href='" & strUrl & "page=" & (CurrentPage - 1) & "'>上一页</a> | "
    End If
    strTemp = strTemp & " "
    If ShowAllPages = True Then
        Dim Jmaxpages
        If (CurrentPage - 4) <= 0 Or TotalPage < 10 Then
            Jmaxpages = 1
            Do While (Jmaxpages < 10)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    End If
                End If
                If Jmaxpages = TotalPage Then Exit Do
                Jmaxpages = Jmaxpages + 1
            Loop
        ElseIf (CurrentPage + 4) >= TotalPage Then
            Jmaxpages = TotalPage - 8
            Do While (Jmaxpages <= TotalPage)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    End If
                End If
                Jmaxpages = Jmaxpages + 1
            Loop
        Else
            Jmaxpages = CurrentPage - 4
            Do While (Jmaxpages < CurrentPage + 5)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    End If
                End If
                Jmaxpages = Jmaxpages + 1
            Loop
        End If
    End If
    If CurrentPage >= TotalPage Then
        strTemp = strTemp & "| 下一页 | 尾页"
    Else
        strTemp = strTemp & " | <a href='" & strUrl & "page=" & (CurrentPage + 1) & "'>下一页</a> |"
        strTemp = strTemp & "<a href='" & strUrl & "page=" & TotalPage & "'>  尾页</a>"
    End If
	If ShowMaxPerPage = True Then
        strTemp = strTemp & "&nbsp;&nbsp;&nbsp;<Input type='text' name='MaxPerPage' size='3' maxlength='4' value='" & MaxPerPage & "' onKeyPress=""if (event.keyCode==13) window.location='" & JoinChar(sfilename) & "page=" & CurrentPage & "&MaxPerPage=" & "'+this.value;"">" & strUnit & "/页"
    Else
        strTemp = strTemp & "&nbsp;<b>" & MaxPerPage & "</b>" & strUnit & "/页"
    End If
    If ShowAllPages = True Then
            strTemp = strTemp & "&nbsp;&nbsp;转到第<Input type='text' name='page' size='3' maxlength='5' value='" & CurrentPage & "' onKeyPress=""if (event.keyCode==13) window.location='" & strUrl & "page=" & "'+this.value;"">页"
    End If
    strTemp = strTemp & "</div>"
    ShowPage = strTemp
End Function

'**************************************************
'函数名：ShowPage_en
'作  用：显示英文“上一页 下一页”等信息
'参  数：sFileName  ----链接地址
'        TotalNumber ----总数量
'        MaxPerPage  ----每页数量
'        CurrentPage ----当前页
'        ShowTotal   ----是否显示总数量
'        ShowAllPages ---是否用下拉列表显示所有页面以供跳转。
'        strUnit     ----计数单位
'        ShowMaxPerPage  ----是否显示每页信息量选项框
'返回值：“上一页 下一页”等信息的HTML代码
'**************************************************
Function ShowPage_en(sfilename, totalnumber, MaxPerPage, CurrentPage, ShowTotal, ShowAllPages, strUnit, ShowMaxPerPage)
    Dim TotalPage, strTemp, strUrl, i

    If totalnumber = 0 Or MaxPerPage = 0 Or IsNull(MaxPerPage) Then
        ShowPage_en = ""
        Exit Function
    End If
    If totalnumber Mod MaxPerPage = 0 Then
        TotalPage = totalnumber \ MaxPerPage
    Else
        TotalPage = totalnumber \ MaxPerPage + 1
    End If
    If CurrentPage > TotalPage Then CurrentPage = TotalPage
        
    strTemp = "<div class=""show_page"">"
    If ShowTotal = True Then
        strTemp = strTemp & "Total <b>" & totalnumber & "</b> " & strUnit & "&nbsp;&nbsp;"
    End If
	
    If ShowMaxPerPage = True Then
        strUrl = JoinChar(sfilename) & "MaxPerPage=" & MaxPerPage & "&"
    Else
        strUrl = JoinChar(sfilename)
    End If
    If CurrentPage = 1 Then
        strTemp = strTemp & "FirstPage PreviousPage&nbsp;"
    Else
        strTemp = strTemp & "<a href='" & strUrl & "page=1'>FirstPage</a>&nbsp;"
        strTemp = strTemp & "<a href='" & strUrl & "page=" & (CurrentPage - 1) & "'>PreviousPage</a>&nbsp;"
    End If

    If CurrentPage >= TotalPage Then
        strTemp = strTemp & "NextPage LastPage"
    Else
        strTemp = strTemp & "<a href='" & strUrl & "page=" & (CurrentPage + 1) & "'>NextPage</a>&nbsp;"
        strTemp = strTemp & "<a href='" & strUrl & "page=" & TotalPage & "'>LastPage</a>"
    End If
    strTemp = strTemp & " CurrentPage: <strong><font color=red>" & CurrentPage & "</font>/" & TotalPage & "</strong> "
    If ShowMaxPerPage = True Then
        strTemp = strTemp & " <Input type='text' name='MaxPerPage' size='3' maxlength='4' value='" & MaxPerPage & "' onKeyPress=""if (event.keyCode==13) window.location='" & JoinChar(sfilename) & "page=" & CurrentPage & "&MaxPerPage=" & "'+this.value;"">" & strUnit & "/Page"
    Else
        strTemp = strTemp & " <b>" & MaxPerPage & "</b>" & strUnit & "/Page"
    End If
    If ShowAllPages = True Then
        If TotalPage > 20 Then
            strTemp = strTemp & "&nbsp;&nbsp;GoTo Page:<Input type='text' name='page' size='3' maxlength='5' value='" & CurrentPage & "' onKeyPress=""if (event.keyCode==13) window.location='" & strUrl & "page=" & "'+this.value;"">"
        Else
            strTemp = strTemp & "&nbsp;GoTo:<select name='page' size='1' onchange=""javascript:window.location='" & strUrl & "page=" & "'+this.options[this.selectedIndex].value;"">"
            For i = 1 To TotalPage
               strTemp = strTemp & "<option value='" & i & "'"
               If PE_CLng(CurrentPage) = PE_CLng(i) Then strTemp = strTemp & " selected "
               strTemp = strTemp & ">Page" & i & "</option>"
            Next
            strTemp = strTemp & "</select>"
        End If
    End If
    strTemp = strTemp & "</div>"
    ShowPage_en = strTemp
End Function

'**************************************************
'函数名：ShowPage_Html
'作  用：显示“上一页 下一页”等信息
'参  数：strPath ----HTMl文件的路径
'        iClassID  ----栏目ID
'        FileExt ----- 扩展名
'        sfilename  ---- 文件名
'        TotalNumber ----总数量
'        MaxPerPage  ----每页数量
'        ShowTotal   ----是否显示总数量
'        ShowAllPages ---是否用下拉列表显示所有页面以供跳转。有某些页面不能使用，否则会出现JS错误。
'        strUnit     ----计数单位
'返回值：“上一页 下一页”等信息的HTML代码
'**************************************************
Function ShowPage_Html(ByVal strPath, iClassID, FileExt, sfilename, totalnumber, MaxPerPage, CurrentPage, ShowTotal, ShowAllPages, strUnit)
    Dim NextPage, PrevPage, EndPage
    Dim TotalPage, strTemp, strUrl, i
    If totalnumber = 0 Or MaxPerPage = 0 Or IsNull(MaxPerPage) Then
        ShowPage_Html = ""
        Exit Function
    End If
    If totalnumber Mod MaxPerPage = 0 Then
        TotalPage = totalnumber \ MaxPerPage
    Else
        TotalPage = totalnumber \ MaxPerPage + 1
    End If
    If CurrentPage > TotalPage Then CurrentPage = TotalPage
    PrevPage = TotalPage - CurrentPage + 2
    NextPage = TotalPage - CurrentPage
    EndPage = 1
    If sfilename <> "" Then
        strUrl = JoinChar(sfilename)
    Else
        strUrl = ""
    End If
    If Right(strPath, 1) = "/" Then
        strPath = Left(strPath, Len(strPath) - 1)
    End If
    strTemp = strTemp & "<div class=""showpage"">"
    If ShowTotal = True Then
        strTemp = strTemp & "共 <b>" & totalnumber & "</b> "& strUnit & "&nbsp;&nbsp;"
    End If
    If CurrentPage = 1 Then
        strTemp = strTemp & "首页 | 上一页 |"
    Else
        If iClassID > 0 Then
            strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & FileExt & "'>首页</a> |"
        Else
            strTemp = strTemp & "<a href='" & strPath & "/" & "'>首页</a> |"
        End If
        If CurrentPage = 2 Then
            If iClassID > 0 Then
                strTemp = strTemp & " <a href='" & strPath & "/list_" & iClassID & FileExt & "'>上一页</a> |"
            Else
                strTemp = strTemp & " <a href='" & strPath & "/" & "'>上一页</a> |"
            End If
        Else
            If strUrl <> "" Then
                strTemp = strTemp & " <a href='" & strUrl & "page=" & (CurrentPage - 1) & "'>上一页</a> |"
            Else
                If iClassID > 0 Then
                    strTemp = strTemp & " <a href='" & strPath & "/list_" & iClassID & "_" & PrevPage & FileExt & "'>上一页</a> |"
                Else
                    strTemp = strTemp & " <a href='" & strPath & "/list_" & PrevPage & FileExt & "'>上一页</a> |"
                End If
            End If
        End If
    End If
    strTemp = strTemp & " "
    If ShowAllPages = True Then
        Dim Jmaxpages
        If (CurrentPage - 4) <= 0 Or TotalPage < 10 Then
            Jmaxpages = 1
            Do While (Jmaxpages < 10)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                ElseIf Jmaxpages = 1 Then
                    If iClassID > 0 Then
                        strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & FileExt & """>" & Jmaxpages & "</a> "
                    Else
                        strTemp = strTemp & "<a href=""" & strPath & "/" & """>" & Jmaxpages & "</a> "
                    End If
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    Else
                        If iClassID > 0 Then
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & "_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        Else
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        End If
                    End If
                End If
                If Jmaxpages = TotalPage Then Exit Do
                Jmaxpages = Jmaxpages + 1
            Loop
        ElseIf (CurrentPage + 4) >= TotalPage Then
            Jmaxpages = TotalPage - 8
            Do While (Jmaxpages <= TotalPage)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                ElseIf Jmaxpages = 1 Then
                    If iClassID > 0 Then
                        strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & FileExt & """>" & Jmaxpages & "</a> "
                    Else
                        strTemp = strTemp & "<a href=""" & strPath & "/" & """>" & Jmaxpages & "</a> "
                    End If
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    Else
                        If iClassID > 0 Then
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & "_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        Else
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        End If
                    End If
                End If
                Jmaxpages = Jmaxpages + 1
            Loop
        Else
            Jmaxpages = CurrentPage - 4
            Do While (Jmaxpages < CurrentPage + 5)
                If Jmaxpages = CurrentPage Then
                    strTemp = strTemp & "<font color=""FF0000"">" & Jmaxpages & "</font> "
                ElseIf Jmaxpages = 1 Then
                    If iClassID > 0 Then
                        strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & FileExt & """>" & Jmaxpages & "</a> "
                    Else
                        strTemp = strTemp & "<a href=""" & strPath & "/" & """>" & Jmaxpages & "</a> "
                    End If
                Else
                    If strUrl <> "" Then
                        strTemp = strTemp & "<a href=""" & strUrl & "page=" & Jmaxpages & """>" & Jmaxpages & "</a> "
                    Else
                        If iClassID > 0 Then
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & iClassID & "_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        Else
                            strTemp = strTemp & "<a href=""" & strPath & "/list_" & TotalPage - Jmaxpages + 1 & FileExt & """>" & Jmaxpages & "</a> "
                        End If
                    End If
                End If
                Jmaxpages = Jmaxpages + 1
            Loop
        End If
    End If

    If CurrentPage >= TotalPage Then
        strTemp = strTemp & "| 下一页 | 尾页  "
    Else
        If strUrl <> "" Then
            strTemp = strTemp & "| <a href='" & strUrl & "page=" & (CurrentPage + 1) & "'>下一页</a> "
            strTemp = strTemp & "| <a href='" & strUrl & "page=" & TotalPage & "'>尾页  </a>"
        Else
            If iClassID > 0 Then
                strTemp = strTemp & "| <a href='" & strPath & "/list_" & iClassID & "_" & NextPage & FileExt & "'>下一页</a> "
                strTemp = strTemp & "| <a href='" & strPath & "/list_" & iClassID & "_" & EndPage & FileExt & "'>尾页  </a>"
            Else
                strTemp = strTemp & "| <a href='" & strPath & "/list_" & NextPage & FileExt & "'>下一页</a> "
                strTemp = strTemp & "| <a href='" & strPath & "/list_" & EndPage & FileExt & "'>尾页  </a>"
            End If
        End If
    End If
	strTemp = strTemp & "&nbsp;<b>" & MaxPerPage & "</b>" & strUnit & "/页"
    If ShowAllPages = True Then
        strTemp = strTemp & "&nbsp;&nbsp;转到第<Input type='text' name='page' size='3' maxlength='5' value='" & CurrentPage & "' onKeyPress=""gopage(this.value,"&TotalPage&")"">页"
        strTemp = strTemp & "<script language='javascript'>" & vbCrLf
        strTemp = strTemp & "function gopage(page,totalpage){" & vbCrLf
        strTemp = strTemp & "  if (event.keyCode==13){" & vbCrLf
        strTemp = strTemp & "    if(Math.abs(page)>totalpage) page=totalpage;" & vbCrLf
        If iClassID > 0 Then
            If strUrl <> "" Then
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strUrl & "page='" & "+Math.abs(page);" & vbCrLf
            Else
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strPath & "/list_" & iClassID & "_'" & "+(totalpage-Math.abs(page)+1)+'" & FileExt & "';" & vbCrLf
            End If
            strTemp = strTemp & "    else  window.location='" & strPath & "/list_" & iClassID & FileExt & "';" & vbCrLf
        Else
            If strUrl <> "" Then
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strUrl & "page='" & "+Math.abs(page);" & vbCrLf
            Else
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strPath & "/list_'+(totalpage-Math.abs(page)+1)+'" & FileExt & "';" & vbCrLf
            End If
            strTemp = strTemp & "    else  window.location='" & strPath & "/index" & FileExt & "';" & vbCrLf
        End If
        strTemp = strTemp & "  }" & vbCrLf
        strTemp = strTemp & "}" & vbCrLf
        strTemp = strTemp & "</script>" & vbCrLf
    End If
    strTemp = strTemp & "</div>" & vbCrLf
   ShowPage_Html = strTemp
End Function

'**************************************************
'函数名：ShowPage_en_Html
'作  用：显示英文“上一页 下一页”等信息
'参  数：strPath ----HTMl文件的路径
'        iClassID  ----栏目ID
'        FileExt ----- 扩展名
'        sfilename  ---- 文件名
'        TotalNumber ----总数量
'        MaxPerPage  ----每页数量
'        ShowTotal   ----是否显示总数量
'        ShowAllPages ---是否用下拉列表显示所有页面以供跳转。有某些页面不能使用，否则会出现JS错误。
'        strUnit     ----计数单位
'返回值：“上一页 下一页”等信息的HTML代码
'**************************************************
Function ShowPage_en_Html(ByVal strPath, iClassID, FileExt, sfilename, totalnumber, MaxPerPage, CurrentPage, ShowTotal, ShowAllPages, strUnit)
    Dim NextPage, PrevPage, EndPage
    Dim TotalPage, strTemp, strUrl, i
    If totalnumber = 0 Or MaxPerPage = 0 Or IsNull(MaxPerPage) Then
        ShowPage_en_Html = ""
        Exit Function
    End If
    If totalnumber Mod MaxPerPage = 0 Then
        TotalPage = totalnumber \ MaxPerPage
    Else
        TotalPage = totalnumber \ MaxPerPage + 1
    End If
    If CurrentPage > TotalPage Then CurrentPage = TotalPage
    
    PrevPage = TotalPage - CurrentPage + 2
    NextPage = TotalPage - CurrentPage
    EndPage = 1

    If sfilename <> "" Then
        strUrl = JoinChar(sfilename)
    Else
        strUrl = ""
    End If
    
    If Right(strPath, 1) = "/" Then
        strPath = Left(strPath, Len(strPath) - 1)
    End If
    
    strTemp = "<!-- ShowPage Begin -->"
    strTemp = strTemp & "<div class=""show_page"">"
    If ShowTotal = True Then
        strTemp = strTemp & "Total <b>" & totalnumber & "</b> " & strUnit & "&nbsp;&nbsp;"
    End If
    If CurrentPage = 1 Then
        strTemp = strTemp & "FirstPage PreviousPage&nbsp;"
    Else
        If iClassID > 0 Then
            strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & FileExt & "'>FirstPage</a>&nbsp;"
        Else
            strTemp = strTemp & "<a href='" & strPath & "/index" & FileExt & "'>FirstPage</a>&nbsp;"
        End If
        If CurrentPage = 2 Then
            If iClassID > 0 Then
                strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & FileExt & "'>PreviousPage</a>&nbsp;"
            Else
                strTemp = strTemp & "<a href='" & strPath & "/index" & FileExt & "'>PreviousPage</a>&nbsp;"
            End If
        Else
            If strUrl <> "" Then
                strTemp = strTemp & "<a href='" & strUrl & "page=" & (CurrentPage - 1) & "'>PreviousPage</a>&nbsp;"
            Else
                If iClassID > 0 Then
                    strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & "_" & PrevPage & FileExt & "'>PreviousPage</a>&nbsp;"
                Else
                    strTemp = strTemp & "<a href='" & strPath & "/list_" & PrevPage & FileExt & "'>PreviousPage</a>&nbsp;"
                End If
            End If
        End If
    End If

    If CurrentPage >= TotalPage Then
        strTemp = strTemp & "NextPage LastPage"
    Else
        If strUrl <> "" Then
            strTemp = strTemp & "<a href='" & strUrl & "page=" & (CurrentPage + 1) & "'>NextPage</a>&nbsp;"
            strTemp = strTemp & "<a href='" & strUrl & "page=" & TotalPage & "'>LastPage</a>"
        Else
            If iClassID > 0 Then
                strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & "_" & NextPage & FileExt & "'>NextPage</a>&nbsp;"
                strTemp = strTemp & "<a href='" & strPath & "/list_" & iClassID & "_" & EndPage & FileExt & "'>LastPage</a>"
            Else
                strTemp = strTemp & "<a href='" & strPath & "/List_" & NextPage & FileExt & "'>NextPage</a>&nbsp;"
                strTemp = strTemp & "<a href='" & strPath & "/List_" & EndPage & FileExt & "'>LastPage</a>"
            End If
        End If
    End If
    strTemp = strTemp & "&nbsp;CurrentPage:<strong><font color=red>" & CurrentPage & "</font>/" & TotalPage & "</strong> "
    strTemp = strTemp & "&nbsp;<b>" & MaxPerPage & "</b>" & strUnit & "/Page"
    If ShowAllPages = True Then
        If TotalPage > 20 Then
            strTemp = strTemp & "&nbsp;&nbsp;GoTo Page:<Input type='text' name='page' size='3' maxlength='5' value='" & CurrentPage & "' onKeyPress='gopage(this.value);'>"
        Else
            strTemp = strTemp & "&nbsp;Goto:<select name='page' size='1' onchange=""javascript:window.location=this.options[this.selectedIndex].value;"">"
            If iClassID > 0 Then
                strTemp = strTemp & "<option value='" & strPath & "/list_" & iClassID & FileExt & "'>Page1</option>"
            Else
                strTemp = strTemp & "<option value='" & strPath & "/index" & FileExt & "'>Page1</option>"
            End If
            For i = 2 To TotalPage
                If strUrl <> "" Then
                   strTemp = strTemp & "<option value='" & strUrl & "page=" & i & "'"
                Else
                    If iClassID > 0 Then
                        strTemp = strTemp & "<option value='" & strPath & "/list_" & iClassID & "_" & TotalPage - i + 1 & FileExt & "'"
                    Else
                        strTemp = strTemp & "<option value='" & strPath & "/list_" & TotalPage - i + 1 & FileExt & "'"
                    End If
                End If
                If CurrentPage = i Then strTemp = strTemp & " selected "
                strTemp = strTemp & ">Page" & i & "</option>"
            Next
            strTemp = strTemp & "</select>"
        End If
    End If
    strTemp = strTemp & "</div>" & vbCrLf
    If ShowAllPages = True And TotalPage > 20 Then
        strTemp = strTemp & "<script language='javascript'>" & vbCrLf
        strTemp = strTemp & "function gopage(page){" & vbCrLf
        strTemp = strTemp & "  if (event.keyCode==13){" & vbCrLf
        strTemp = strTemp & "    if(Math.abs(page)>totalpage) page=totalpage;" & vbCrLf
        If iClassID > 0 Then
            If strUrl <> "" Then
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strUrl & "page='" & "+Math.abs(page);" & vbCrLf
            Else
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strPath & "/list_" & iClassID & "_'" & "+(totalpage-Math.abs(page)+1)+'" & FileExt & "';" & vbCrLf
            End If
            strTemp = strTemp & "    else  window.location='" & strPath & "/list_" & iClassID & FileExt & "';" & vbCrLf
        Else
            If strUrl <> "" Then
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strUrl & "page='" & "+Math.abs(page);" & vbCrLf
            Else
                strTemp = strTemp & "    if(Math.abs(page)>1) window.location='" & strPath & "/list_'+(totalpage-Math.abs(page)+1)+'" & FileExt & "';" & vbCrLf
            End If
            strTemp = strTemp & "    else  window.location='" & strPath & "/index" & FileExt & "';" & vbCrLf
        End If
        strTemp = strTemp & "  }" & vbCrLf
        strTemp = strTemp & "}" & vbCrLf
        strTemp = strTemp & "</script>" & vbCrLf
    End If
    strTemp = strTemp & "<!-- ShowPage End -->"
    ShowPage_en_Html = strTemp
End Function

'**************************************************
'函数名：IsObjInstalled
'作  用：检查组件是否已经安装
'参  数：strClassString ----组件名
'返回值：True  ----已经安装
'        False ----没有安装
'**************************************************
Function IsObjInstalled(strClassString)
    On Error Resume Next
    IsObjInstalled = False
    Err = 0
    Dim xTestObj
    Set xTestObj = CreateObject(strClassString)
    If Err.Number = 0 Then IsObjInstalled = True
    Set xTestObj = Nothing
    Err = 0
End Function

'**************************************************
'过程名：WriteErrMsg
'作  用：显示错误提示信息
'参  数：无
'**************************************************
Sub WriteErrMsg(sErrMsg, sComeUrl)
    Response.Write "<html><head><title>错误信息</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link href='" & strInstallDir & "images/Style.css' rel='stylesheet' type='text/css'></head><body><br><br>" & vbCrLf
    Response.Write "<table cellpadding=2 cellspacing=1 border=0 width=400 class='border' align=center>" & vbCrLf
    Response.Write "  <tr align='center' class='title'><td height='22'><strong>错误信息</strong></td></tr>" & vbCrLf
    Response.Write "  <tr class='tdbg'><td height='100' valign='top'><b>产生错误的可能原因：</b>" & sErrMsg & "</td></tr>" & vbCrLf
    Response.Write "  <tr align='center' class='tdbg'><td>"
    If sComeUrl <> "" Then
        Response.Write "<a href='javascript:history.go(-1)'>&lt;&lt; 返回上一页</a>"
    Else
        Response.Write "<a href='javascript:window.close();'>【关闭】</a>"
    End If
    Response.Write "</td></tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</body></html>" & vbCrLf
End Sub

'**************************************************
'过程名：WriteSuccessMsg
'作  用：显示成功提示信息
'参  数：无
'**************************************************
Sub WriteSuccessMsg(sSuccessMsg, sComeUrl)
    Response.Write "<html><head><title>成功信息</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link href='" & strInstallDir & "images/Style.css' rel='stylesheet' type='text/css'></head><body><br><br>" & vbCrLf
    Response.Write "<table cellpadding=2 cellspacing=1 border=0 width=400 class='border' align=center>" & vbCrLf
    Response.Write "  <tr align='center' class='title'><td height='22'><strong>恭喜你！</strong></td></tr>" & vbCrLf
    Response.Write "  <tr class='tdbg'><td height='100' valign='top'><br>" & sSuccessMsg & "</td></tr>" & vbCrLf
    Response.Write "  <tr align='center' class='tdbg'><td>"
    If sComeUrl <> "" Then
        Response.Write "<a href='" & sComeUrl & "'>&lt;&lt; 返回上一页</a>"
    Else
        Response.Write "<a href='javascript:window.close();'>【关闭】</a>"
    End If
    Response.Write "</td></tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</body></html>" & vbCrLf
End Sub

'**************************************************
'过程名：ShowErrMsg
'作  用：显示错误提示信息,用于前台
'参  数：无
'**************************************************
Sub ShowErrMsg(sErrMsg, sComeUrl)
    Response.Write "<html><head><title>错误信息</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link href='" & strInstallDir & "images/error.css' rel='stylesheet' type='text/css'></head><body><br><br>" & vbCrLf
    Response.Write "<table cellpadding=2 cellspacing=1 border=0 width=400 class='border_m' align=center>" & vbCrLf
    Response.Write "  <tr align='center'><td height='25' class='title_m' align='center'><strong>错误信息</strong></td></tr>" & vbCrLf
    Response.Write "  <tr><td height='100' valign='top' class='font12' style='padding:5px; line-height:20px;'><b>产生错误的可能原因：</b>" & sErrMsg & "</td></tr>" & vbCrLf
    Response.Write "  <tr align='center'><td class='font12'>"
    If sComeUrl <> "" Then
        Response.Write "<a href='javascript:history.go(-1)'>&lt;&lt; 返回上一页</a>"
    Else
        Response.Write "<a href='javascript:window.close();'>【关闭】</a>"
    End If
    Response.Write "</td></tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</body></html>" & vbCrLf
End Sub

'**************************************************
'过程名：WriteSuccessMsg
'作  用：显示成功提示信息,用于前台
'参  数：无 
'**************************************************
Sub ShowSuccessMsg(sSuccessMsg, sComeUrl)
    Response.Write "<html><head><title>成功信息</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link href='" & strInstallDir & "images/error.css' rel='stylesheet' type='text/css'></head><body><br><br>" & vbCrLf
    Response.Write "<table cellpadding=2 cellspacing=1 border=0 width=400 class='border_m' align=center>" & vbCrLf
    Response.Write "  <tr align='center'><td height='25' class='title_m' align='center'><strong>恭喜你！</strong></td></tr>" & vbCrLf
    Response.Write "  <tr><td height='100' valign='top' style='padding:5px; line-height:20px;'><br>" & sSuccessMsg & "</td></tr>" & vbCrLf
    Response.Write "  <tr align='center'><td class='font12'>"
    If sComeUrl <> "" Then
        Response.Write "<a href='" & sComeUrl & "'>&lt;&lt; 返回上一页</a>"
    Else
        Response.Write "<a href='javascript:window.close();'>【关闭】</a>"
    End If
    Response.Write "</td></tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</body></html>" & vbCrLf
End Sub

'**************************************************
'函数名：FoundInArr
'作  用：检测数组中是否有指定的数值
'参  数：strArr ----- 调入的数组
'        strItem  ----- 检测的字符
'        strSplit  ----- 分割字符
'返回值：True  ----有
'        False ----没有
'**************************************************
Function FoundInArr(strArr, strItem, strSplit)
    Dim arrTemp, arrTemp2, i, j
    FoundInArr = False
    If IsNull(strArr) Or IsNull(strItem) Or Trim(strArr) = "" Or Trim(strItem) = "" Then
        Exit Function
    End If
    If IsNull(strSplit) Or strSplit = "" Then
        strSplit = ","
    End If
	
    If InStr(Trim(strArr), strSplit) > 0 Then
        If InStr(Trim(strItem), strSplit) > 0 Then
            arrTemp = Split(strArr, strSplit)
            arrTemp2 = Split(strItem, strSplit)
            For i = 0 To UBound(arrTemp)
                For j = 0 To UBound(arrTemp2)
                    If LCase(Trim(arrTemp2(j))) <> "" And LCase(Trim(arrTemp(i))) <> "" And LCase(Trim(arrTemp2(j))) = LCase(Trim(arrTemp(i))) Then
                        FoundInArr = True
                        Exit Function
                    End If
                Next
            Next
        Else
            arrTemp = Split(strArr, strSplit)
            For i = 0 To UBound(arrTemp)
                If LCase(Trim(arrTemp(i))) = LCase(Trim(strItem)) Then
                    FoundInArr = True
                    Exit Function
                End If
            Next
        End If
    Else
        If LCase(Trim(strArr)) = LCase(Trim(strItem)) Then
            FoundInArr = True
        End If
    End If
End Function


Function RemoveStr(str1, str2, strSplit)
    If IsNull(str1) Or str1 = "" Then
        RemoveStr = ""
        Exit Function
    End If
    If IsNull(str2) Or str2 = "" Then
        RemoveStr = str1
        Exit Function
    End If
    If InStr(str1, strSplit) > 0 Then
        Dim arrStr, tempStr, i
        arrStr = Split(str1, strSplit)
        For i = 0 To UBound(arrStr)
            If arrStr(i) <> str2 Then
                If tempStr = "" Then
                    tempStr = arrStr(i)
                Else
                    tempStr = tempStr & strSplit & arrStr(i)
                End If
            End If
        Next
        RemoveStr = tempStr
    Else
        If str1 = str2 Then
            RemoveStr = ""
        Else
            RemoveStr = str1
        End If
    End If
End Function


Function AppendStr(str1, str2, strSplit)
    If IsNull(str2) Or str2 = "" Then
        AppendStr = str1
        Exit Function
    End If
    If IsNull(str1) Or str1 = "" Then
        AppendStr = str2
        Exit Function
    End If
    Dim Foundstr, arrStr, i
    Foundstr = False
    If InStr(str1, strSplit) > 0 Then
        arrStr = Split(str1, strSplit)
        For i = 0 To UBound(arrStr)
            If arrStr(i) = str2 Then
                Foundstr = True
                Exit For
            End If
        Next
    Else
        If str1 = str2 Then
            Foundstr = True
        End If
    End If
    If Foundstr = False Then
        AppendStr = str1 & strSplit & str2
    Else
        AppendStr = str1
    End If
End Function

'**************************************************
'函数名：GetRndPassword
'作  用：得到指定位数的随机数密码
'参  数：PasswordLen ---- 位数
'返回值：密码字符串
'**************************************************
Function GetRndPassword(PasswordLen)
    Dim Ran, i, strPassword
    strPassword = ""
    For i = 1 To PasswordLen
        Randomize
        Ran = CInt(Rnd * 2)
        Randomize
        If Ran = 0 Then
            Ran = CInt(Rnd * 25) + 97
            strPassword = strPassword & UCase(Chr(Ran))
        ElseIf Ran = 1 Then
            Ran = CInt(Rnd * 9)
            strPassword = strPassword & Ran
        ElseIf Ran = 2 Then
            Ran = CInt(Rnd * 25) + 97
            strPassword = strPassword & Chr(Ran)
        End If
    Next
    GetRndPassword = strPassword
End Function


'**************************************************
'函数名：GetMinID
'作  用：取某一表某一字段中的最小值
'参  数：SheetName ----查询表
'        FieldName ----查询字段
'返回值：该字段最小值
'**************************************************
Function GetMinID(SheetName, FieldName, QuerySql)
    Dim mrs
    Set mrs = Conn.Execute("select min(" & FieldName & ") from " & SheetName & QuerySql&"")
    If IsNull(mrs(0)) Then
        GetMinID = 1
    Else
        GetMinID = mrs(0)
    End If
    Set mrs = Nothing
End Function


'**************************************************
'函数名：GetNewID
'作  用：取某一表某一字段中的最大值+1
'参  数：SheetName ----查询表
'        FieldName ----查询字段
'返回值：该字段最大值+1
'**************************************************
Function GetNewID(SheetName, FieldName)
    Dim mrs
    Set mrs = Conn.Execute("select max(" & FieldName & ") from " & SheetName & "")
    If IsNull(mrs(0)) Then
        GetNewID = 1
    Else
        GetNewID = mrs(0) + 1
    End If
    Set mrs = Nothing
End Function


'**************************************************
'函数名：GetNumString
'作  用：获得项目随即数
'返回值：随机无重复的数字(用于上传,生成)
'**************************************************
Function GetNumString()
    Dim v_ymd, v_hms, v_mmm
    v_ymd = Year(Now) & Right("0" & Month(Now), 2) & Right("0" & Day(Now), 2)
    v_hms = Right("0" & Hour(Now), 2) & Right("0" & Minute(Now), 2) & Right("0" & Second(Now), 2)
    Randomize
    v_mmm = Right("0" & CStr(CLng(99 * Rnd) + 1), 2)
    GetNumString = v_ymd & v_hms & v_mmm
End Function


'**************************************************
'函数名：GetFirstSeparatorToEnd
'作  用：截取从第一个分隔符到结尾的字符串
'参  数：str   ----原字符串
'        separator ----分隔符
'返回值：截取后的字符串
'**************************************************
Function GetFirstSeparatorToEnd(ByVal str, separator)
    GetFirstSeparatorToEnd = Right(str, Len(str) - InStr(str, separator))
End Function


'**************************************************
'函数名：PE_Replace
'作  用：容错替换
'参  数：expression ---- 主数据
'        find ---- 被替换的字符
'        replacewith ---- 替换后的字符
'返回值：容错后的替换字符串,如果 replacewith 空字符,被替换的字符 替换成空
'**************************************************
Function PE_Replace(ByVal expression, ByVal find, ByVal replacewith)
    If IsNull(expression) Or IsNull(find) Then
        PE_Replace = expression
    ElseIf IsNull(replacewith) Then
        PE_Replace = Replace(expression, find, "")
    Else
        PE_Replace = Replace(expression, find, replacewith)
    End If
End Function


'**************************************************
'函数名：Refresh
'作  用：等待特定时间后跳转到指定的网址
'参  数：url ---- 跳转网址
'        refreshTime ---- 等待跳转时间
'**************************************************
Sub Refresh(url,refreshTime)
		Response.write "<script>setTimeout(""location.href='"& url &"'"","& refreshTime*1000 &")</script>"
End Sub

'**************************************************
'函数名：GetNumber_Option
'作  用：显示数字下拉菜单
'参  数：MinNum ---- 初始数
'        MaxNum ---- 最大数
'        CurrentNum ----selected 默认数
'返回值：下拉菜单数据
'**************************************************
Public Function GetNumber_Option(MinNum, MaxNum, CurrentNum)
    Dim strNumber, i
    For i = MinNum To MaxNum
        If i = CurrentNum Then
            strNumber = strNumber & "<option value='" & i & "' selected>&nbsp;&nbsp;" & i & "&nbsp;&nbsp;</option>"
        Else
            strNumber = strNumber & "<option value='" & i & "'>&nbsp;&nbsp;" & i & "&nbsp;&nbsp;</option>"
        End If
    Next
    GetNumber_Option = strNumber
End Function

'**************************************************
'函数名：StyleDisplay
'作  用：根据条件显示或隐藏
'参  数：Compare1 ---- 比较字符串一
'        Compare2 ---- 比较字符串二
'**************************************************
Function StyleDisplay(Compare1, Compare2)
    If Compare1 = Compare2 Then
        StyleDisplay = ""
    Else
        StyleDisplay = "none"
    End If
End Function


'**************************************************
'函数名：IsRadioChecked
'作  用：根据条件判断选框是否选中
'参  数：Compare1 ---- 比较字符串一
'        Compare2 ---- 比较字符串二
'**************************************************
Function IsRadioChecked(Compare1, Compare2)
    If Compare1 = Compare2 Then
        IsRadioChecked = " checked"
    Else
        IsRadioChecked = ""
    End If
End Function


'**************************************************
'函数名：IsOptionSelected
'作  用：根据条件判断下拉框是否选中
'参  数：Compare1 ---- 比较字符串一
'        Compare2 ---- 比较字符串二
'**************************************************
Function IsOptionSelected(Compare1, Compare2)
    If Compare1 = Compare2 Then
        IsOptionSelected = " selected"
    Else
        IsOptionSelected = ""
    End If
End Function

%>