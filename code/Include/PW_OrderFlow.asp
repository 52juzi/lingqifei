<%
Dim Remark
Dim ProductList, ProductID
Dim rsCart, CartID
Dim ShowTips_Login, ShowTips_CartIsEmpty
Dim dblAmount
ChannelShortName = "产品"
MaxPerPage = 20


If UserLogined = False Then
    ShowTips_Login = "<font color='#0000FF'>温馨提示：</font><font color='#006600'>您还没有注册或登录，请先<a href='../member/Reg.asp'>注册</a>或<a href='../member/User_Login.asp'>登录</a>，以获得更多优惠！</font>"
Else
    Call GetUser(UserID)
    ShowTips_Login = ""
End If
strPageTitle = SiteName

Call GetChannel(ChannelID)

strPageTitle = strPageTitle & " >> " & ChannelName


Function SelectCart(CartID)
    Dim rsCartItem, ProductList
    ProductList = ""
    Set rsCartItem = Conn.Execute("select ProductID from PW_ShoppingCarts where CartID='" & CartID & "' order by CartItemID desc")
    Do While Not rsCartItem.EOF
        If ProductList = "" Then
            ProductList = rsCartItem("ProductID")
        Else
            ProductList = ProductList & "," & rsCartItem("ProductID")
        End If
        rsCartItem.MoveNext
    Loop
    SelectCart = ProductList
    Set rsCartItem = Nothing
End Function



Sub AddToCart(CartID, iProductID, Quantity)
    Dim CartItemID, IsExistential
	Conn.Execute ("update PW_ShoppingCarts set Quantity=Quantity+0,UserName='" & UserName & "', UpdateTime=" & PE_Now & " where CartID='" & CartID & "' and ProductID=" & iProductID & ""), IsExistential
	
	If IsExistential = 0 Then
		Conn.Execute ("insert into PW_ShoppingCarts (CartID,ProductID,Quantity,UserName,UpdateTime) values ('" & CartID & "'," & iProductID & "," & Quantity & ",'" & UserName & "'," & PE_Now & ")")
	End If
End Sub



Sub DelCart(CartID)
    Conn.Execute ("delete from PW_ShoppingCarts where CartID='" & CartID & "'")
End Sub


Function ShowCart()
    If IsNull(ProductList) Or ProductList = "" Then
        ShowCart = ""
        Exit Function
    End If
    If IsValidID(ProductList) = False Then
        ShowCart = "<li>ProductList数据非法！</li>"
        Exit Function
    End If
    
    Dim strCart
    strCart = strCart & "              <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border3'>" & vbCrLf
    strCart = strCart & "                <tr align='center' class='tdbg2' height='25'>" & vbCrLf
    strCart = strCart & "                  <td width='32'><b>购买</b></td>" & vbCrLf
    strCart = strCart & "                  <td><b>商 品 名 称</b></td>" & vbCrLf
    strCart = strCart & "                  <td width='54'><b>数 量</b></td>" & vbCrLf
    strCart = strCart & "                </tr>" & vbCrLf
    
    Set rsCart = Conn.Execute("select P.*,C.Quantity,C.CartItemID from PW_Product P inner join PW_ShoppingCarts C on C.ProductID=P.ProductID where C.CartID='" & CartID & "' order by C.CartItemID desc")
    Do While Not rsCart.EOF
        dblAmount = rsCart("Quantity")
        If dblAmount <= 0 Then dblAmount = 1
        If dblAmount <> rsCart("Quantity") Then
            Call UpdateAmount(rsCart("CartItemID"), dblAmount)
        End If
        
        strCart = strCart & "                <tr valign='middle' class='tdbg3' height='20'>" & vbCrLf
        strCart = strCart & "                  <td width='32' align='center'><input type='CheckBox' name='ProductID' value='" & rsCart("ProductID") & "' Checked></td>" & vbCrLf
        strCart = strCart & "                  <td align='left'>" & rsCart("ProductName") & "</td>" & vbCrLf
        strCart = strCart & "                  <td width='54' align='center'><input name='Amount_" & rsCart("ProductID") & "' type='Text' value='" & dblAmount & "' size='5' maxlength='10' style='text-align: center;'></td>" & vbCrLf
       strCart = strCart & "                </tr>" & vbCrLf
        rsCart.MoveNext
    Loop
    rsCart.Close
    Set rsCart = Nothing

    strCart = strCart & "              </table>" & vbCrLf
    ShowCart = strCart
End Function

Function ShowCart2()
    If IsNull(ProductList) Or ProductList = "" Then
        ShowCart2 = ""
        Exit Function
    End If
    If IsValidID(ProductList) = False Then
        ShowCart2 = "<li>ProductList数据非法！</li>"
        Exit Function
    End If
    
    Dim strCart
    strCart = strCart & "              <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border3'>" & vbCrLf
    strCart = strCart & "                <tr align='center' class='tdbg2' height='25'>" & vbCrLf
    strCart = strCart & "                  <td><b>商 品 名 称</b></td>" & vbCrLf
    strCart = strCart & "                  <td width='54'><b>数 量</b></td>" & vbCrLf
    strCart = strCart & "                </tr>" & vbCrLf
    
    Set rsCart = Conn.Execute("select P.*,C.Quantity,C.CartItemID from PW_Product P inner join PW_ShoppingCarts C on C.ProductID=P.ProductID where C.CartID='" & CartID & "' order by C.CartItemID desc")
    Do While Not rsCart.EOF
        dblAmount = rsCart("Quantity")
        If dblAmount <= 0 Then dblAmount = 1
        If dblAmount <> rsCart("Quantity") Then
            Call UpdateAmount(rsCart("CartItemID"), dblAmount)
        End If
        
        strCart = strCart & "                <tr valign='middle' class='tdbg3' height='20'>" & vbCrLf
        strCart = strCart & "                  <td align='left'>" & rsCart("ProductName") & "</td>" & vbCrLf
        strCart = strCart & "                  <td width='54' align='center'>" & dblAmount & "</td>" & vbCrLf
       strCart = strCart & "                </tr>" & vbCrLf
        rsCart.MoveNext
    Loop
    rsCart.Close
    Set rsCart = Nothing

    strCart = strCart & "              </table>" & vbCrLf
    ShowCart2 = strCart
End Function

Sub UpdateAmount(CartItemID, Amount)
    Conn.Execute ("Update PW_ShoppingCarts set Quantity=" & Amount & " where CartItemID=" & CartItemID & "")
End Sub

%>