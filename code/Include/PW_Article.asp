<%
Dim ArticleID, ArticleTitle, ArticleUrl
Dim rsArticle


Class Article
	Private ArticlePro1, ArticlePro2, ArticlePro3, ArticlePro4
	Private rsClass
	Public totalPage
	Public Sub Init()
		FoundErr = False
		ErrMsg = ""
		ChannelShortName = "文章"
		PrevChannelID = ChannelID
		strListStr_Font = "color=""red"""
		ArticlePro1 = "[图文]"
		ArticlePro2 = "[组图]"
		ArticlePro3 = "[推荐]"
		ArticlePro4 = "[注意]"
		Call GetChannel(ChannelID)
		strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;<a href='"
        If UseCreateHTML > 0 Then
            strNavPath = strNavPath & ChannelUrl & "/Index.html"
        Else
            strNavPath = strNavPath & ChannelUrl_ASPFile & "/Index.asp"
        End If
		strNavPath = strNavPath & "'>" & ChannelName & "</a>"
		strPageTitle = ChannelName&"_"&SiteName
	End Sub
	
	Private Function GetInfoList_GetStrIncludePic(IncludePic)
		Dim strIncludePic
		strIncludePic = ""
		Select Case IncludePic
		Case 1
			strIncludePic = strIncludePic & "<span class=""S_headline1"">" & ArticlePro1 & "</span>"
		Case 2
			strIncludePic = strIncludePic & "<span class=""S_headline2"">" & ArticlePro2 & "</span>"
		Case 3
			strIncludePic = strIncludePic & "<span class=""S_headline3"">" & ArticlePro3 & "</span>"
		Case 4
			strIncludePic = strIncludePic & "<span class=""S_headline4"">" & ArticlePro4 & "</span>"
		End Select
		GetInfoList_GetStrIncludePic = strIncludePic
	End Function
	
	
	Private Function GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)
		Dim strSql, IDOrder
		
		If IsValidID(iChannelID) = False Then
			iChannelID = 0
		Else
			iChannelID = ReplaceLabelBadChar(iChannelID)
		End If  
		If IsValidID(arrClassID) = False Then
			arrClassID = 0
		Else
			arrClassID = ReplaceLabelBadChar(arrClassID)
		End If 		
		
		strSql = strSql & " from PW_Article A left join PW_Class C on A.ClassID=C.ClassID where A.Deleted=" & PE_False & " "
		
		If InStr(iChannelID, ",") > 0 Then
			strSql = strSql & " and A.ChannelID in (" & FilterArrNull(iChannelID, ",") & ")"
		Else
			If PE_CLng(iChannelID) > 0 Then strSql = strSql & " and A.ChannelID=" & PE_CLng(iChannelID)
		End If	
	
		If arrClassID <> "0" Then
			If InStr(arrClassID, ",") = 0 And IncludeChild = True Then
				Dim trs
				Set trs = Conn.Execute("select arrChildID from PW_Class where ClassID=" & PE_CLng(arrClassID) & "")
				If trs.BOF And trs.EOF Then
					arrClassID = "0"
				Else
					If IsNull(trs(0)) Or Trim(trs(0)) = "" Then
						arrClassID = "0"
					Else
						arrClassID = trs(0)
					End If
				End If
				Set trs = Nothing
			End If
			
			If InStr(arrClassID, ",") > 0 Then
				strSql = strSql & " and A.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
			Else
				If PE_CLng(arrClassID) > 0 Then strSql = strSql & " and A.ClassID=" & PE_CLng(arrClassID)
			End If
		End If
		If IsHot = True Then
			strSql = strSql & " and A.Hits>=" & HitsOfHot
		End If
		If IsElite = True Then
			strSql = strSql & " and A.Elite=" & PE_True
		End If
		if  Keyword<>"" then
			strSql = strSql & " and A.Title like'%"& Keyword &"%' "
		End if	
		If IsPicUrl = True Then
			strSql = strSql & " and A.DefaultPicUrl<>'' "
		End If
	
		strSql = strSql & " order by A.OnTop " & PE_OrderType & ","
		Select Case PE_CLng(OrderType)
		Case 1, 2
		
		Case 3
			strSql = strSql & "A.UpdateTime desc,"
		Case 4
			strSql = strSql & "A.UpdateTime asc,"
		Case 5
			strSql = strSql & "A.Hits desc,"
		Case 6
			strSql = strSql & "A.Hits asc,"
		Case Else
	
		End Select
		If OrderType = 2 Then
			IDOrder = "asc"
		Else
			IDOrder = "desc"
		End If
		strSql = strSql & "A.ArticleID " & IDOrder
		GetSqlStr = strSql
	End Function



	Public Function GetCustomFromTemplate(strValue)   '得到自定义列表的版面设计的HTML代码
		Dim strCustom, strParameter
		strCustom = strValue
		regEx.Pattern = "【articlelist\((.*?)\)】([\s\S]*?)【\/articlelist】"
		Set Matches = regEx.Execute(strCustom)
		For Each Match In Matches
			strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
			strCustom = PE_Replace(strCustom, Match.Value, GetCustomFromLabel(strParameter, Match.SubMatches(1)))
		Next
		GetCustomFromTemplate = strCustom
	End Function



'【articlelist(ChannelID, arrClassID, IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen, IncludePic)】【Cols=2|<hr>】【Rows=2|<hr><hr>】 循环内容【/articlelist】
Private Function GetCustomFromLabel(strTemp, strList)
    Dim arrTemp
    Dim strArticlePic, strPicTemp, arrPicTemp
    Dim iChannelID, arrClassID, IncludeChild, ItemNum, IsHot, IsElite, DateNum, OrderType, UsePage, TitleLen, ContentLen
    Dim iCols, iColsHtml, iRows, iRowsHtml, iNumber
    Dim IncludePic
    If strTemp = "" Or strList = "" Then GetCustomFromLabel = "": Exit Function

    iCols = 1: iRows = 1: iColsHtml = "": iRowsHtml = ""
    regEx.Pattern = "【(cols|rows)=(\d{1,2})\s*(?:\||｜)(.+?)】"
    Set Matches = regEx.Execute(strList)
    For Each Match In Matches
        If LCase(Match.SubMatches(0)) = "cols" Then
            If Match.SubMatches(1) > 1 Then iCols = Match.SubMatches(1)
            iColsHtml = Match.SubMatches(2)
        ElseIf LCase(Match.SubMatches(0)) = "rows" Then
            If Match.SubMatches(1) > 1 Then iRows = Match.SubMatches(1)
            iRowsHtml = Match.SubMatches(2)
        End If
        strList = regEx.Replace(strList, "")
    Next
    
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 10 and UBound(arrTemp) <> 9  Then
        GetCustomFromLabel = "自定义列表标签：【articlelist(参数列表)】列表内容【/articlelist】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If


    Select Case Trim(arrTemp(0))
    Case "channelid"
        iChannelID = ChannelID
    Case Else
        iChannelID = arrTemp(0)
    End Select
    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(Trim(arrClassID), "|", ",")
    IncludeChild = PE_CBool(arrTemp(2))
    ItemNum = PE_CLng(arrTemp(3))
    IsHot = PE_CBool(arrTemp(4))
    IsElite = PE_CBool(arrTemp(5))
    OrderType = PE_CLng(arrTemp(6))
    UsePage = PE_CBool(arrTemp(7))
    TitleLen = PE_CLng(arrTemp(8))
    ContentLen = PE_CLng(arrTemp(9))
    If UBound(arrTemp) = 10  then
        IncludePic = PE_CBool(arrTemp(10))
    Else
        IncludePic = False	    
    End If
    FoundErr = False
    If (PE_Clng(iChannelID) <> 0 and Instr(iChannelID,",")=0) and (PE_Clng(iChannelID)<>PrevChannelID Or ChannelID = 0) Then
        Call GetChannel(PE_Clng(iChannelID))
        PrevChannelID = iChannelID		 
    End If
    If FoundErr = True Then
        GetCustomFromLabel = ErrMsg
        Exit Function
    End If

    Dim rsField, ArrField, iField
    Set rsField = Conn.Execute("select FieldName,LabelName,FieldType from PW_Field where ChannelID=" & iChannelID & "")
    If Not (rsField.BOF And rsField.EOF) Then
        ArrField = rsField.getrows(-1)
    End If
    Set rsField = Nothing

    Dim sqlCustom, rsCustom, iCount, strCustomList
    iCount = 0
    sqlCustom = ""
    strCustomList = ""
    
    sqlCustom = "select "
    If ItemNum > 0 Then
        sqlCustom = sqlCustom & "top " & ItemNum & " "
    End If
    If ContentLen > 0 Then
        sqlCustom = sqlCustom & "A.Content,"
    End If
    If IsArray(ArrField) Then
        For iField = 0 To UBound(ArrField, 2)
            sqlCustom = sqlCustom & "A." & ArrField(0, iField) & ","
        Next
    End If
    sqlCustom = sqlCustom & "A.ArticleID,A.ChannelID,A.ClassID,A.Title,A.LinkUrl,A.Keyword,A.Intro,A.DefaultPicUrl,A.IncludePic,A.TitleFontColor,A.TitleFontType"
    sqlCustom = sqlCustom & ",A.Author,A.CopyFrom,A.UpdateTime,A.Hits,A.OnTop,A.Elite,A.InfoPurview,C.ClassName"
    sqlCustom = sqlCustom & GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IncludePic)
    Set rsCustom = Server.CreateObject("ADODB.Recordset")
    rsCustom.Open sqlCustom, Conn, 1, 1
    If rsCustom.BOF And rsCustom.EOF Then
        totalPut = 0
        strCustomList = GetInfoList_StrNoItem(arrClassID, IsHot, IsElite)
        rsCustom.Close
        Set rsCustom = Nothing
        GetCustomFromLabel = strCustomList
        Exit Function
    End If

    If UsePage = True Then
        totalPut = rsCustom.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                iMod = 0
                rsCustom.Move (CurrentPage - 1) * MaxPerPage - iMod
            Else
                CurrentPage = 1
            End If
        End If
    End If
    PrevChannelID = 0
    Do While Not rsCustom.EOF
		If rsCustom("ChannelID") <> PrevChannelID Then
			Call GetChannel(rsCustom("ChannelID"))
			PrevChannelID = rsCustom("ChannelID")
		End If
        strTemp = strList
        If UsePage = True Then
            iNumber = (CurrentPage - 1) * MaxPerPage + iCount + 1
        Else
            iNumber = iCount + 1
        End If
         
        strTemp = PE_Replace(strTemp, "{$autoid}", iNumber)
        strTemp = PE_Replace(strTemp, "{$classid}", rsCustom("ClassID"))
        strTemp = PE_Replace(strTemp, "{$classname}", rsCustom("ClassName"))
        strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCustom("ClassID")))
		
        strTemp = PE_Replace(strTemp, "{$articleid}", rsCustom("ArticleID"))
        If InStr(strTemp, "{$articleurl}") > 0 Then strTemp = PE_Replace(strTemp, "{$articleurl}", GetArticleUrl(rsCustom("UpdateTime"), rsCustom("ArticleID"),rsCustom("InfoPurview")))
		
        strTemp = PE_Replace(strTemp, "{$author}", rsCustom("Author"))
        strTemp = PE_Replace(strTemp, "{$copyfrom}", rsCustom("CopyFrom"))
        strTemp = PE_Replace(strTemp, "{$hits}", rsCustom("Hits"))
		strTemp = PE_Replace(strTemp, "{$linkurl}", rsCustom("LinkUrl"))
		
		
		if InStr(strTemp, "{$property}") >0 then
			Dim PropertyStr
			PropertyStr = ""
			if rsCustom("OnTop") = True then
				PropertyStr = PropertyStr&"<font color=""green""> 顶</font>"
			end if
			if rsCustom("Elite") = True then
				PropertyStr = PropertyStr&"<font color=""blue""> 荐</font>"
			end if
			if rsCustom("Hits") > HitsOfHot then
				PropertyStr = PropertyStr&"<font color=""red""> 荐</font>"
			end if
			strTemp = PE_Replace(strTemp, "{$property}", PropertyStr)	
		end if
        If InStr(strTemp, "{$updatedate}") > 0 Then strTemp = PE_Replace(strTemp, "{$updatedate}", FormatDateTime(rsCustom("UpdateTime"), 2))
        strTemp = PE_Replace(strTemp, "{$updatetime}", rsCustom("updatetime"))
		If InStr(strHtml, "{$month}") > 0 Then strTemp = PE_Replace(strTemp, "{$month}", getMonth(rsCustom("UpdateTime")))	
		If InStr(strHtml, "{$day}") > 0 Then strTemp = PE_Replace(strTemp, "{$day}", getDay(rsCustom("UpdateTime")))	
		If InStr(strHtml, "{$year}") > 0 Then strTemp = PE_Replace(strTemp, "{$year}", getYear(rsCustom("UpdateTime")))
		If InStr(strHtml, "{$smalltitle}") > 0 then
			if rsCustom("IncludePic") > 0 then
				strTemp = PE_Replace(strTemp, "{$smalltitle}", GetInfoList_GetStrIncludePic(rsCustom("IncludePic")))
			else
				strTemp = PE_Replace(strTemp, "{$smalltitle}", "")
			end if
		end if
		
		strTemp = PE_Replace(strTemp, "{$title}", GetInfoList_GetStrTitle(rsCustom("Title"),TitleLen,rsCustom("TitleFontType"), rsCustom("TitleFontColor")))
        strTemp = PE_Replace(strTemp, "{$titleall}", rsCustom("Title"))
		
		if InStr(strHtml, "{$content}")>0 then
			If ContentLen > 0 Then
				strTemp = PE_Replace(strTemp, "{$content}", GetInfoList_GetStrContent(rsCustom("Content"),ContentLen))
			End If
		end if
		If ContentLen > 0 Then
			strTemp = PE_Replace(strTemp, "{$intro}", GetInfoList_GetStrContent(rsCustom("Intro"),ContentLen))
		else
			strTemp = PE_Replace(strTemp, "{$intro}", rsCustom("Intro"))
		end if
        strTemp = PE_Replace(strTemp, "{$articlepic}", GetContent(rsCustom("DefaultPicUrl")))
         
        '替换首页图片
        regEx.Pattern = "\{\$articlepic\((.*?)\)\}"
        Set Matches = regEx.Execute(strTemp)
        For Each Match In Matches
            arrPicTemp = Split(Match.SubMatches(0), ",")
            strArticlePic = GetDefaultPicUrl(Trim(rsCustom("DefaultPicUrl")), PE_CLng(arrPicTemp(0)), PE_CLng(arrPicTemp(1)))
            strTemp = Replace(strTemp, Match.Value, strArticlePic)
        Next
        
		If IsArray(ArrField) Then
            For iField = 0 To UBound(ArrField, 2)
                Select Case ArrField(2, iField)
                Case 8,9
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField)))))
                Case 4
                    If PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="" or IsNull(PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))) or PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="http://" Then
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "")	
                    Else 
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "<img class='fieldImg' src='" &PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))&"' border=0>")	
                    End If
                Case Else
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLEncode(rsCustom(Trim(ArrField(0, iField)))))				
                End Select 
           Next
        End If

        strCustomList = strCustomList & strTemp
        rsCustom.MoveNext
        iCount = iCount + 1
        If iCols > 1 And iCount Mod iCols = 0 Then strCustomList = strCustomList & iColsHtml
        If iRows > 1 And iCount Mod iCols * iRows = 0 Then strCustomList = strCustomList & iRowsHtml
        If UsePage = True And iCount >= MaxPerPage Then Exit Do
    Loop
    rsCustom.Close
    Set rsCustom = Nothing
    
    GetCustomFromLabel = strCustomList
End Function

'【relatearticle(iChannelID,arrClassID, ArticleNum, TitleLen, OrderType)】{$Title}{$url}{$Smalltitle}{$ClassID}{$UpdateTime}{$Hits}【/relatearticle】
Public Function GetCorrelativeFromTemplate(strValue)  
    Dim strCorrelative, strParameter
    strCorrelative = strValue
    regEx.Pattern = "【relatearticle\((.*?)\)】([\s\S]*?)【\/relatearticle】"
    Set Matches = regEx.Execute(strCorrelative)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCorrelative = PE_Replace(strCorrelative, Match.Value, GetCorrelative(strParameter, Match.SubMatches(1)))
    Next
    GetCorrelativeFromTemplate = strCorrelative
End Function

'=================================================
'函数名：GetCorrelative
'作  用：显示相关文章 之中参数(iChannelID,arrClassID, ArticleNum, TitleLen, OrderType)
'=================================================
private Function GetCorrelative(strTemp, strList)
    Dim arrTemp
	Dim iChannelID,arrClassID,ArticleNum,TitleLen,OrderType, MaxNum
    Dim rsCorrelative, sqlCorrelative, strCorrelative, iTemp
    Dim strKey, arrKey, i
	strCorrelative=""
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 4 Then
        GetCorrelative = "相关文章标签：【relatearticle(参数列表)】列表内容【/relatearticle】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
    iChannelID = Replace(iChannelID,"|",",")
    Select Case iChannelID
    Case "channelid"
        iChannelID = ChannelID
    Case else
        If IsValidID(iChannelID) = False Then
            iChannelID = 0
        End If  
    End Select	
    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(arrClassID,"|",",")	
	ArticleNum = PE_CLng(arrTemp(2))
    TitleLen = PE_CLng(arrTemp(3))
	OrderType = PE_CLng(arrTemp(4))
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    End If  
    If ArticleNum > 0 And ArticleNum <= 20 Then
        sqlCorrelative = "select top " & ArticleNum
    Else
        sqlCorrelative = "Select Top 5 "
    End If
    strKey = Mid(rsArticle("Keyword"), 2, Len(rsArticle("Keyword")) - 2)
    If InStr(strKey, "|") > 1 Then
        arrKey = Split(strKey, "|")
        MaxNum = UBound(arrKey)   
        strKey = "((A.Keyword like '%|" & Replace(Replace(arrKey(0), "［", ""), "］", "") & "|%')"
        For i = 1 To MaxNum
            strKey = strKey & " or (A.Keyword like '%|" & Replace(Replace(arrKey(i), "［", ""), "］", "") & "|%')"
        Next
        strKey = strKey & ")"
    Else
        strKey = "(A.Keyword like '%|" & strKey & "|%')"
    End If
    sqlCorrelative = sqlCorrelative & " A.ArticleID,A.ClassID,A.Title,A.UpdateTime,A.Hits,A.TitleFontColor,A.TitleFontType,A.IncludePic,A.InfoPurview,C.ClassName from PW_Article A left join PW_Class C on A.ClassID=C.ClassID where 1=1"
    If InStr(iChannelID, ",") > 0 Then
        sqlCorrelative = sqlCorrelative & " and A.ChannelID in (" & FilterArrNull(iChannelID, ",") & ")"
    Else
        If PE_CLng(iChannelID) > 0 Then sqlCorrelative = sqlCorrelative & " and A.ChannelID=" & PE_CLng(iChannelID)
    End If	
    If arrClassID <> "0" Then
        If InStr(arrClassID, ",") > 0 Then
            sqlCorrelative = sqlCorrelative & " and A.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
        Else
            If PE_CLng(arrClassID) > 0 Then sqlCorrelative = sqlCorrelative & " and A.ClassID=" & PE_CLng(arrClassID)
        End If
    End If
    sqlCorrelative = sqlCorrelative & " and A.Deleted=" & PE_False & ""
    sqlCorrelative = sqlCorrelative & " and " & strKey & " and A.ArticleID<>" & ArticleID & " Order by "
    Select Case PE_CLng(OrderType)
    Case 1
        sqlCorrelative = sqlCorrelative & "A.ArticleID desc"
    Case 2
        sqlCorrelative = sqlCorrelative & "A.ArticleID asc"
    Case 3
        sqlCorrelative = sqlCorrelative & "A.UpdateTime desc"
    Case 4
        sqlCorrelative = sqlCorrelative & "A.UpdateTime asc"
    Case Else
        sqlCorrelative = sqlCorrelative & "A.ArticleID desc"
    End Select
    Set rsCorrelative = Conn.Execute(sqlCorrelative)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsCorrelative.BOF And rsCorrelative.EOF Then
        strCorrelative = "没有相关信息！"
    Else
        Do While Not rsCorrelative.EOF
			strTemp = strList
			if rsInfoList("IncludePic") > 0 then
				strTemp = PE_Replace(strTemp, "{$smalltitle}", GetInfoList_GetStrIncludePic(rsCorrelative("IncludePic")))
			else
				strTemp = PE_Replace(strTemp, "{$smalltitle}", "")
			end if
			strTemp = PE_Replace(strTemp, "{$title}", GetInfoList_GetStrTitle(rsCorrelative("Title"),TitleLen,rsCorrelative("TitleFontType"), rsCorrelative("TitleFontColor")))
			strTemp = PE_Replace(strTemp, "{$titleall}", rsCorrelative("Title"))
			strTemp = PE_Replace(strTemp, "{$articleurl}", GetArticleUrl(rsCorrelative("UpdateTime"), rsCorrelative("ArticleID"),rsCorrelative("InfoPurview")))
			strTemp = PE_Replace(strTemp, "{$classid}", rsCorrelative("classid"))
			strTemp = PE_Replace(strTemp, "{$classname}", rsCorrelative("ClassName"))
			strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCorrelative("ClassID")))
			strTemp = PE_Replace(strTemp, "{$updatetime}", rsCorrelative("UpdateTime"))
			If InStr(strHtml, "{$month}") > 0 Then strTemp = PE_Replace(strTemp, "{$month}", getMonth(rsCorrelative("UpdateTime")))	
			If InStr(strHtml, "{$day}") > 0 Then strTemp = PE_Replace(strTemp, "{$day}", getDay(rsCorrelative("UpdateTime")))	
			If InStr(strHtml, "{$year}") > 0 Then strTemp = PE_Replace(strTemp, "{$year}", getYear(rsCorrelative("UpdateTime")))
        strCorrelative = strCorrelative & strTemp
        rsCorrelative.MoveNext
    Loop
	End if
    rsCorrelative.Close
    Set rsCorrelative = Nothing
    
    GetCorrelative = strCorrelative
End function
	
'=================================================
'函数名：GetPrevArticle
'作  用：显示上一篇文章
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'        LinkAddress 链接地址
'=================================================
Private Function GetPrevArticle(TitleLen)
    Dim rsPrev, sqlPrev, strPrev
    strPrev = PE_Replace("<li>上一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlPrev = "Select Top 1 ArticleID,Title,UpdateTime,Hits,InfoPurview from PW_Article Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsArticle("ClassID") & " and ArticleID<" & rsArticle("ArticleID") & " order by ArticleID DESC"
    Set rsPrev = Conn.Execute(sqlPrev)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsPrev.EOF Then
        strPrev = strPrev & "没有了"
    Else
        strPrev = strPrev & "<a href='" & GetArticleUrl(rsPrev("UpdateTime"), rsPrev("ArticleID"),rsPrev("InfoPurview")) &"'"
        strPrev = strPrev & " title='" & rsPrev("Title") &"'>" & GetSubStr(rsPrev("Title"), TitleLen, True) & "</a>"
    End If
    rsPrev.Close
    Set rsPrev = Nothing
    strPrev = strPrev & "</li>"
    GetPrevArticle = strPrev
End Function


'=================================================
'函数名：GetNextArticle
'作  用：显示下一篇文章
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetNextArticle(TitleLen)
    Dim rsNext, sqlNext, strNext
    strNext = Replace("<li>下一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlNext = "Select Top 1 ArticleID,Title,UpdateTime,InfoPurview from PW_Article Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsArticle("ClassID") & " and ArticleID>" & rsArticle("ArticleID") & " order by ArticleID ASC"
    Set rsNext = Conn.Execute(sqlNext)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsNext.EOF Then
        strNext = strNext & "没有了"
    Else
        strNext = strNext & "<a href='" & GetArticleUrl(rsNext("UpdateTime"), rsNext("ArticleID"),rsNext("InfoPurview")) &"'"
        strNext = strNext & " title='" & rsNext("Title") &"'>" & GetSubStr(rsNext("Title"), TitleLen, True) & "</a>"
    End If
    rsNext.Close
    Set rsNext = Nothing
    strNext = strNext & "</li>"
    GetNextArticle = strNext
End Function

'=================================================
'函数名：GetArticleContent
'作  用：显示文章具体的内容
Public Function GetArticleContent()
	Dim FoundErr, ErrMsg
    FoundErr = False
    ErrMsg = ""
	Dim ErrMsg_NoLogin,ErrMsg_PurviewCheckedErr
	ErrMsg_NoLogin = Replace(Replace("<br>&nbsp;&nbsp;&nbsp;&nbsp;你还没注册？或者没有登录？这{$itemunit}要求至少是本站的注册会员才能阅读！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你还没注册，请赶紧<a href='{$InstallDir}member/Reg.asp'><font color=red>点此注册</font></a>吧！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你已经注册但还没登录，请赶紧<a href='{$InstallDir}member/User_Login.asp'><font color=red>点此登录</font></a>吧！<br><br>", "{$itemunit}", ChannelItemUnit & ChannelShortName), "{$InstallDir}", strInstallDir)
	If rsArticle("InfoPurview")<>"0" then
        If UserLogined <> True Then
            FoundErr = True
            ErrMsg = ErrMsg & ErrMsg_NoLogin
        Else
			Call GetUser(UserID)
			ErrMsg_PurviewCheckedErr = "对不起，您没有查看此栏目内容的权限！"
			if FoundInArr(rsArticle("InfoPurview"),GroupID,",") = false then
				FoundErr = True
				ErrMsg = ErrMsg & ErrMsg_PurviewCheckedErr
			end if
		End IF
	end If
    If FoundErr = True Then
        GetArticleContent = ErrMsg
        Exit Function
    End If
    If rsArticle("LinkUrl") <> "" And rsArticle("LinkUrl") <> "http://" Then
        GetArticleContent = "<script language='javascript'>window.location.href='" & rsArticle("LinkUrl") & "';</script>"
	else
		GetArticleContent = ReplaceKeyLink(GetContent(rsArticle("Content")))
	end if
End Function


'=================================================
'函数名：GetDefaultPicUrl
Private Function GetDefaultPicUrl(ByVal DefaultPicUrl, ByVal DefaultPicWidth, ByVal DefaultPicHeight)
    Dim strUrl, FileType, strPicUrl
    If DefaultPicUrl = "" Or IsNull(DefaultPicUrl) = True Then
        strUrl = strUrl & "<img src='" & strInstallDir & "images/nopic.gif' "
        If DefaultPicWidth > 0 Then strUrl = strUrl & " width='" & DefaultPicWidth & "'"
        If DefaultPicHeight > 0 Then strUrl = strUrl & " height='" & DefaultPicHeight & "'"
        strUrl = strUrl & " border='0'>"
    Else
        FileType = LCase(Mid(DefaultPicUrl, InStrRev(DefaultPicUrl, ".") + 1))
        strPicUrl = GetContent(DefaultPicUrl)
        Select Case FileType
        Case "swf"
            strUrl = strUrl & "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' "
            If DefaultPicWidth > 0 Then strUrl = strUrl & " width='" & DefaultPicWidth & "'"
            If DefaultPicHeight > 0 Then strUrl = strUrl & " height='" & DefaultPicHeight & "'"
            strUrl = strUrl & "><param name='movie' value='" & strPicUrl & "'><param name='quality' value='high'><embed src='" & strPicUrl & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' "
            If DefaultPicWidth > 0 Then strUrl = strUrl & " width='" & DefaultPicWidth & "'"
            If DefaultPicHeight > 0 Then strUrl = strUrl & " height='" & DefaultPicHeight & "'"
            strUrl = strUrl & "></embed></object>"
        Case "gif", "jpg", "jpeg", "jpe", "bmp", "png"
            strUrl = strUrl & "<img src='"& strPicUrl & "' "
            If DefaultPicWidth > 0 Then strUrl = strUrl & " width='" & DefaultPicWidth & "'"
            If DefaultPicHeight > 0 Then strUrl = strUrl & " height='" & DefaultPicHeight & "'"
            strUrl = strUrl & " border='0'>"
        Case Else
            strUrl = strUrl & "<img src='" & strInstallDir & "images/nopic.gif' "
            If DefaultPicWidth > 0 Then strUrl = strUrl & " width='" & DefaultPicWidth & "'"
            If DefaultPicHeight > 0 Then strUrl = strUrl & " height='" & DefaultPicHeight & "'"
            strUrl = strUrl & " border='0'>"
        End Select
    End If
    GetDefaultPicUrl = strUrl
End Function


Public Sub GetHTML_Index()
	
    Call GetChannel(ChannelID)
	strHtml = GetTemplate(tempindex)
	
    ClassID = 0
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
	strHtml = PE_Replace(strHtml, "{$classtitle}", ChannelName)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    If UseCreateHTML > 0 Then
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
    Else
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$ShowPage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
    End If
End Sub


Public Sub GetHtml_Class()
	If list_template="" then
		list_template = templist
	End if
	strHtml = GetTemplate(list_template)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords_Class)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description_Class)
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
	strHtml = PE_Replace(strHtml, "{$classtitle}",ClassName)
	
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    Dim strPath
    strPath = ChannelUrl & "/"

    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
    strHtml = PE_Replace(strHtml, "{$classpicurl}", ClassPicUrl)
    strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	IF UseCreateHTML > 0 then
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
	else
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))	
	end IF
End Sub

Public Sub GetHtml_Article()
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    strHtml = PE_Replace(strHtml, "{$articleid}", ArticleID)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", GetKeywords(",", rsArticle("Keyword")))
    strHtml = PE_Replace(strHtml, "{$meta_description}", rsArticle("Intro"))
    Call ReplaceCommonLabel   '解析通用标签，包含自定义标签

    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    strHtml = PE_Replace(strHtml, "{$articleid}", ArticleID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", ArticleTitle & "_" & SiteName)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath & "&nbsp;" & strNavLink & "&nbsp;详细内容")
    If InStr(strHtml, "{$MY_") > 0 Then
        Dim rsField
        Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
        Do While Not rsField.EOF
            If rsField("FieldType") = 8 Or rsField("FieldType") = 9  Or rsField("FieldType") = 10 Then
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLDecode(rsArticle(Trim(rsField("FieldName")))))
            Else
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLEncode(rsArticle(Trim(rsField("FieldName")))))		
            End If	
            rsField.MoveNext
        Loop
        Set rsField = Nothing
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
	strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
    If InStr(strHtml, "{$author}") > 0 Then strHtml = PE_Replace(strHtml, "{$author}", rsArticle("Author"))
	If InStr(strHtml, "{$copyfrom}") > 0 Then strHtml = PE_Replace(strHtml, "{$copyfrom}", rsArticle("CopyFrom"))
	If InStr(strHtml, "{$hits}") > 0 Then strHtml = PE_Replace(strHtml, "{$hits}", GetArticleHits())
	If InStr(strHtml, "{$updatedate}") > 0 Then strHtml = PE_Replace(strHtml, "{$updatedate}", FormatDateTime(rsArticle("UpdateTime"), 2))
	strHtml = PE_Replace(strHtml, "{$updatetime}", rsArticle("updatetime"))

	If InStr(strHtml, "{$month}") > 0 Then strHtml = PE_Replace(strHtml, "{$month}", getMonth(rsArticle("UpdateTime")))	
	If InStr(strHtml, "{$day}") > 0 Then strHtml = PE_Replace(strHtml, "{$day}", getDay(rsArticle("UpdateTime")))	
	If InStr(strHtml, "{$year}") > 0 Then strHtml = PE_Replace(strHtml, "{$year}", getYear(rsArticle("UpdateTime")))
	
    If InStr(strHtml, "{$intro}") > 0 Then strHtml = PE_Replace(strHtml, "{$intro}", rsArticle("Intro"))
	If InStr(strHtml, "{$title}") > 0 Then strHtml = PE_Replace(strHtml, "{$title}", Trim(rsArticle("Title")))
    If InStr(strHtml, "{$content}") > 0 Then strHtml = PE_Replace(strHtml, "{$content}", GetArticleContent())
	
	strHtml = PE_Replace(strHtml, "{$defaultpicurl}", rsArticle("DefaultPicUrl"))
	
	DIM strParameter, strTemp
	'''替换上一条信息
	regEx.Pattern = "\{\$prevarticle\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetPrevArticle(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换下一条信息
	regEx.Pattern = "\{\$nextarticle\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetNextArticle(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换相关文章
	strHtml = GetCorrelativeFromTemplate(strHtml)
End Sub


Private Function GetArticleHits()
    If UseCreateHTML > 0 Then
        GetArticleHits = "<script language='javascript' src='" & ChannelUrl_ASPFile & "/GetHits.asp?ArticleID=" & ArticleID & "'></script>"
    Else
        GetArticleHits = rsArticle("Hits")
    End If
End Function

End Class

%>