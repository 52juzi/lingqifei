<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1--频道管理员，2--栏目总编，3--栏目管理员
Const PurviewLevel_Others = ""   '其他权限

Dim SkinID
SkinID = PE_CLng(GetValue("SkinID"))

Response.Write "<html><head><title>风格管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("风 格 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td><a href='Admin_Skin.asp'>风格管理首页</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Skin.asp?Action=Add'>添加风格</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf

Select Case Action
    Case "Add"
        Call Add
    Case "Modify"
        Call Modify
    Case "SaveAdd","SaveModify"
        Call SaveSkin
    Case "SetDefault"
        Call SetDefault
    Case "Del"
        Call DelSkin
    Case "Refresh"
        Call CreatSkinFile
        If FoundErr = False Then
            Call WriteSuccessMsg("刷新CSS风格文件成功！", ComeUrl)
        End If
		Call Refresh(ComeUrl,1)
    Case Else
        Call main
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim rsKind, sqlKind
    Response.Write "<br>"
    Response.Write "<SCRIPT language=javascript>" & vbCrLf
    Response.Write "    function CheckAll(thisform){" & vbCrLf
    Response.Write "        for (var i=0;i<thisform.elements.length;i++){" & vbCrLf
    Response.Write "            var e = thisform.elements[i];" & vbCrLf
    Response.Write "            if (e.Name != ""chkAll""&&e.disabled!=true&&e.zzz!=1)" & vbCrLf
    Response.Write "                e.checked = thisform.chkAll.checked;" & vbCrLf
    Response.Write "        }" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "</script>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "    <td height='22'>您现在的位置：网站风格管理 >> 所有风格管理 </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>" & vbCrLf
    Response.Write "  <tr>" & vbCrLf
    Response.Write "  <form name='myform' method='Post' action='Admin_Skin.asp' onsubmit='return ConfirmDel();'>" & vbCrLf
    Response.Write "    <td>" & vbCrLf
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>" & vbCrLf
    Response.Write "        <tr class='title' height='22'> " & vbCrLf
    Response.Write "          <td width='30' align='center'><strong>选中</strong></td>" & vbCrLf
    Response.Write "          <td width='30' align='center'><strong>ID</strong></td>" & vbCrLf
    Response.Write "          <td align='center'><strong>风格名称</strong></td>"& vbCrLf
    Response.Write "      	  <td width='60'><strong>系统默认</strong></td>"& vbCrLf
    Response.Write "          <td width='200' height='22' align='center'><strong> 操作</strong></td>"& vbCrLf
    Response.Write "        </tr>"& vbCrLf
	sqlKind = "select * from PW_Skin order by SkinID desc"
    Set rsKind = conn.execute(sqlKind)
    If rsKind.BOF And rsKind.EOF Then
		Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>没 有 任 何 风 格！<br><br></td></tr>"
    Else
        Do While Not rsKind.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">" & vbCrLf
            Response.Write "          <td align='center' height='22'>"
            Response.Write "           <input type=""checkbox"" value=" & rsKind("SkinID") & " name=""SkinID"""
            If rsKind("IsDefault") = True Then Response.Write "disabled"
            Response.Write "> " & vbCrLf
            Response.Write "          </td>" & vbCrLf
            Response.Write "      <td align='center'>" & rsKind("SkinID") & "</td>"& vbCrLf
            Response.Write "      <td align='center'>" & rsKind("SkinName") & "</td>"& vbCrLf
			Response.Write "      <td align='center'>"
			If rsKind("IsDefault") = True Then
				Response.Write "<FONT style='font-size:12px' color='#008000'><b>√</b></FONT>"
			End If
			Response.Write "</td>"& vbCrLf
            Response.Write "      <td align='center'>"
			If rsKind("IsDefault") = False Then
				Response.Write "&nbsp;<a href='Admin_Skin.asp?Action=SetDefault&SkinID=" & rsKind("SkinID") & "'>设为系统默认</a>"
			Else
				Response.Write "<font color='gray'>&nbsp;设为系统默认</font>"
			End If
            Response.Write "&nbsp;&nbsp;<a href='Admin_Skin.asp?Action=Modify&SkinID=" & rsKind("SkinID") & "'>修改风格</a>&nbsp;&nbsp;"
            If rsKind("IsDefault") = False Then
                Response.Write "<a href='Admin_Skin.asp?Action=Del&SkinID=" & rsKind("SkinID") & "' onClick=""return confirm('确定要删除此风格吗？');"">删除风格</a>"
            Else
                Response.Write "<font color='gray'>删除风格</font>"
            End If

            Response.Write "      </td>"& vbCrLf
            Response.Write "    </tr>"& vbCrLf
            rsKind.MoveNext
        Loop
    End If
    rsKind.Close
    Set rsKind = Nothing
    Response.Write "        <tr class=""tdbg"">" & vbCrLf
    Response.Write "          <td height='30' colspan='5'>"
	Response.Write "        	<input name=""Action"" type=""hidden""  value=""Del"">   " & vbCrLf
	Response.Write "        	<input name=""chkAll"" type=""checkbox"" id=""chkAll"" onclick=CheckAll(this.form) value=""checkbox"" >选中所有项目" & vbCrLf
	Response.Write "        	&nbsp;&nbsp;&nbsp;&nbsp;将选定的项目： " & vbCrLf
	Response.Write "        	<input type=""submit"" value=""批&nbsp;量&nbsp;删&nbsp;除 "" name=""Del"" onclick='return confirm(""确定要删除此风格吗？"");' >&nbsp;&nbsp;" & vbCrLf
    Response.Write "          </td>" & vbCrLf
	Response.Write "        </tr> " & vbCrLf
	Response.Write "        <tr class='tdbg'>"
	Response.Write "          <td height='40' align='center' colspan='5'><input type='submit' name='Submit' value='刷新风格CSS文件' onclick=""document.myform.Action.value='Refresh'""></td>"
	Response.Write "        </tr>"
    Response.Write "      </table>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </form>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
End Sub


Sub Add()

    Response.Write "<form name='myform' method='post' action='Admin_Skin.asp'>"
    Response.Write "  <table width='100%' border='0' cellspacing='1' cellpadding='2' class='border'>"
    Response.Write "    <tr align='center' class='title'>"
    Response.Write "      <td height='22' colspan='2'><strong>添加新风格</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='100'><strong>风格名称：</strong></td>"
    Response.Write "      <td> <input name='SkinName' type='text' id='SkinName' value='' size='50' maxlength='50'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='100'><strong>风格配色设置</strong><br>"
    Response.Write "        <br>"
    Response.Write "      修改风格设置必须具备一定网页设计知识<br><br>"
    Response.Write "      不能使用单引号或双引号，否则会容易造成程序错误</td>"
    Response.Write "      <td><textarea name='Skin_CSS' cols='80' rows='20' id='Skin_CSS'></textarea></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr align='center' class='tdbg'>"
    Response.Write "      <td height='50' colspan='2'><input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "        <input type='submit' name='Submit' value=' 添 加 '></td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub


Sub Modify()
    Dim rs, sql

    If SkinID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定SkinID</li>"
        Exit Sub
    End If
    
    sql = "select * from PW_Skin where SkinID=" & SkinID

    Set rs = Conn.Execute(sql)

    If rs.BOF And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的风格！</li>"
        rs.Close
        Set rs = Nothing
        Exit Sub
    End If

    Response.Write "<form name='myform' method='post' action='Admin_Skin.asp'>"
    Response.Write "  <table width='100%' border='0' cellspacing='1' cellpadding='2' class='border'>"
    Response.Write "    <tr align='center' class='title'>"
    Response.Write "      <td height='22' colspan='2'><strong>修改风格设置</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='100'><strong>风格名称：</strong></td>"
    Response.Write "      <td> <input name='SkinName' type='text' id='SkinName' value='" & rs("SkinName") & "' size='50' maxlength='50'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='100'><strong>风格配色设置</strong><br>"
    Response.Write "        <br>"
    Response.Write "      修改风格设置必须具备一定网页设计知识<br><br>"
    Response.Write "      不能使用单引号或双引号，否则会容易造成程序错误</td>"
    Response.Write "      <td><textarea name='Skin_CSS' cols='80' rows='20' id='Skin_CSS'>" & rs("Skin_CSS") & "</textarea></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr align='center' class='tdbg'>"
    Response.Write "      <td height='50' colspan='2'><input name='SkinID' type='hidden' id='SkinID' value='" & SkinID & "'><input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "        <input type='submit' name='Submit' value=' 保存修改结果 '></td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"

    rs.Close
    Set rs = Nothing
End Sub



Sub SaveSkin()
    Dim SkinName, Skin_CSS
    Dim rs, sql
    SkinName = GetForm("SkinName")
    Skin_CSS = GetForm("Skin_CSS")

    If SkinName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>风格名称不能为空！</li>"
    End If

    If Skin_CSS = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>风格内容不能为空！</li>"
    End If

    If FoundErr = True Then Exit Sub
	
    If Action = "SaveAdd" Then
		sql = "select top 1 * from PW_Skin"
		Set rs = Server.CreateObject("Adodb.RecordSet")
		rs.Open sql, Conn, 1, 3
		rs.addnew
		rs("IsDefault") = False
	ElseIF Action = "SaveModify" Then
		sql = "select top 1 * from PW_Skin where SkinID=" & SkinID
		Set rs = Server.CreateObject("Adodb.RecordSet")
		rs.Open sql, Conn, 1, 3
        If rs.BOF And rs.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的风格！</li>"
            rs.Close
            Set rs = Nothing
            Exit Sub
        End If
	End if
    rs("SkinName") = SkinName
    rs("Skin_CSS") = Skin_CSS
    rs.Update
    rs.Close
    Set rs = Nothing
	Call CreatSkinFile
    Response.Redirect "Admin_Skin.asp"
End Sub


Sub SetDefault()

    If SkinID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定SkinID!</li>"
        Exit Sub
    End If
        
    Conn.Execute ("update PW_Skin set IsDefault="& PE_False &"")
    Conn.Execute ("update PW_Skin set IsDefault="& PE_True &" where SkinID=" & SkinID & "")
    Call CreatSkinFile
    Call WriteSuccessMsg("操作成功！", ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub


Sub DelSkin()
    Dim sql
	If IsValidID(SkinID) = False Then
		SkinID = ""
	End If

    If SkinID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定SkinID</li>"
        Exit Sub
    End If

    If InStr(SkinID, ",") > 0 Then
        sql = "Delete * from PW_Skin where SkinID In (" & SkinID & ") and IsDefault=False"
    Else
        sql = "Delete * from PW_Skin where SkinID=" & PE_CLng(SkinID) &" and IsDefault=False"
    End If
	Conn.execute(sql)
    Call WriteSuccessMsg("成功删除选定的风格。", ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub


Sub CreatSkinFile()

    If ObjInstalled_FSO = False Then
        Exit Sub
    End If

    If Not fso.FolderExists(Server.MapPath(InstallDir)) Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先进行网站配置后再进行此项操作。</li>"
        Exit Sub
    End If

    If Not fso.FolderExists(Server.MapPath(InstallDir & "template")) Then
        fso.CreateFolder (Server.MapPath(InstallDir & "template"))
    End If

    Dim rsSkin, sqlSkin, hf, strSkin
    sqlSkin = "select * from PW_Skin where IsDefault=" & PE_True & ""
    Set rsSkin = Conn.Execute(sqlSkin)

    Do While Not rsSkin.EOF
        strSkin = Replace_CaseInsensitive(rsSkin("Skin_CSS"), "Images/", InstallDir &"template/"& TemplateDir &"/Images/")
        Call WriteToFile(InstallDir & "template/Style.css", strSkin)
        rsSkin.MoveNext
    Loop

    rsSkin.Close
    Set rsSkin = Nothing
End Sub




%>