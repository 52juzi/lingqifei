<!--#include file="Admin_createcommon.asp"-->
<%
Dim PW_Content

Set PW_Content = New Article
PW_Content.Init
tmpPageTitle = strPageTitle    '保存页面标题到临时变量中，以做为栏目及内容页循环生成时初始值
tmpNavPath = strNavPath
ArticleID = GetValue("ArticleID")
Select Case Action
Case "CreateArticle"
    Call CreateArticle
Case "CreateClass"
    Call CreateClass
Case "CreateIndex"
    Call CreateIndex
Case "CreateArticle2"
    If AutoCreateType > 0 Then
        IsAutoCreate = True
        Call CreateArticle
        If ClassID > 0 Then
            ClassID = ParentPath & "," & ClassID
            Call CreateClass
        End If
        '在生成首页前，要将栏目ID置为0
        ClassID = 0
        arrChildID = 0
        Call CreateIndex

        Call CreateSiteIndex     '生成网站首页
    End If
Case Else
    FoundErr = True
    ErrMsg = ErrMsg & "<li>参数错误！</li>"
End Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
ELse
	Call ShowProcess
End If


Response.Write "</body></html>"
Set PW_Content = Nothing
Call CloseConn


Sub CreateArticle()
    'On Error Resume Next
    ChannelID = PE_CLng(GetValue("ChannelID"))
    Dim sql, strFields, ArticlePath
    Dim strArticleContent
    sql = "select * from PW_Article where Deleted=" & PE_False & " and InfoPurview='0' and ChannelID=" & ChannelID
	If IsAutoCreate = False Then
		Response.Write "<b>正在生成" & ChannelShortName & "页面……请稍候！<font color='red'>在此过程中请勿刷新此页面！！！</font></b><br>"
		Response.Flush
	End If
    Select Case CreateType
    Case 1 '选定的文章
        If IsValidID(ArticleID) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请正确指定要生成的" & ChannelShortName & "ID</li>"
            Exit Sub
        End If
        If InStr(ArticleID, ",") > 0 Then
            sql = sql & " and ArticleID in (" & ArticleID & ")"
        Else
            sql = sql & " and ArticleID=" & ArticleID
        End If
        strUrlParameter = "&ArticleID=" & ArticleID
    Case 2 '选定的栏目
        ClassID = PE_CLng(GetValue("ClassID"))
        If ClassID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定要生成的栏目ID</li>"
            Exit Sub
        End If
        Call GetClass
        If FoundErr = True Then Exit Sub
        If InStr(arrChildID, ",") > 0 Then
            sql = sql & " and ClassID in (" & arrChildID & ")"
        Else
            sql = sql & " and ClassID=" & ClassID
        End If
    Case 3 '所有文章
        
    Case 4 '最新的文章
        Dim TopNew
        TopNew = PE_CLng(GetValue("TopNew"))
        If TopNew <= 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定有效的数目！"
            Exit Sub
        End If
        sql = "select top " & TopNew & " * from PW_Article where Deleted=" & PE_False & " And InfoPurview='0' and ChannelID=" & ChannelID & ""
        strUrlParameter = "&TopNew=" & TopNew
    Case 5 '指定更新时间
        Dim BeginDate, EndDate
        BeginDate = GetValue("BeginDate")
        EndDate = GetValue("EndDate")
        If Not (IsDate(BeginDate) And IsDate(EndDate)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入有效的日期！</li>"
            Exit Sub
        End If
        sql = sql & " and UpdateTime between #" & BeginDate & "# and #" & EndDate & "#"
        strUrlParameter = "&BeginDate=" & BeginDate & "&EndDate=" & EndDate
    Case 6 '指定ID范围
        Dim BeginID, EndID
        BeginID = GetValue("BeginID")
        EndID = GetValue("EndID")
        If Not (IsNumeric(BeginID) And IsNumeric(EndID)) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请输入数字！</li>"
            Exit Sub
        End If
        sql = sql & " and ArticleID between " & BeginID & " and " & EndID & ""
        strUrlParameter = "&BeginID=" & BeginID & "&EndID=" & EndID
    Case 9 '所有未生成的文章
        sql = "select top " & MaxPerPage_Create & " * from PW_Article where Deleted=" & PE_False & " and InfoPurview='0' and ChannelID=" & ChannelID & ""
		sql = sql & " and (CreateTime is null or CreateTime<=UpdateTime)"
    Case Else
        Response.Write "参数错误！"
        Exit Sub
    End Select
    If CreateType = 4 Then
        sql = sql & " order by UpdateTime desc,ClassID,ArticleID"
    Else
        sql = sql & " order by ClassID,ArticleID"
    End If
    Set rsArticle = Server.CreateObject("ADODB.Recordset")
    rsArticle.Open sql, Conn, 1, 1
    If rsArticle.Bof And rsArticle.EOF Then
        TotalCreate = 0
		iTotalPage = 0
        rsArticle.Close
        Set rsArticle = Nothing
        Exit Sub
    Else
        If CreateType = 9 Then
			TotalCreate = PE_Clng(Conn.Execute("select count(*) from PW_Article where Deleted=" & PE_False & " And InfoPurview='0' and ChannelID=" & ChannelID & " and (CreateTime is null or CreateTime<=UpdateTime)")(0))
		Else
			TotalCreate = rsArticle.RecordCount
		End If
		
    End If
    strFileName = ChannelUrl_ASPFile & "/detial.asp" '得到路径

    Call MoveRecord(rsArticle)
    Call ShowTotalCreate(ChannelItemUnit & ChannelShortName)
    Do While Not rsArticle.EOF
        FoundErr = False
        ArticleID = rsArticle("ArticleID")
        ClassID = rsArticle("ClassID")
        strNavPath = tmpNavPath
        If ChannelID <> PrevChannelID Then
            Call GetChannel(ChannelID)
            PrevChannelID = ChannelID
        End If
        Call GetClass
		If temparticle <> article_template And article_template<>"" then
			strTemplate = GetTemplate(article_template) '分类的内容页
		Else
			strTemplate = GetTemplate(temparticle)'频道的内容页
		end If
        strPageTitle = tmpPageTitle
        iCount = iCount + 1
        If rsArticle("InfoPurview") <> "0" Then
            Response.Write "<li><font color='red'>ID为 " & rsArticle("ArticleID") & " 的" & ChannelShortName & "因为设置了阅读权限，所以没有生成。</font></li>"
            Response.Flush
        Else
            CurrentPage = 1
            ArticlePath = ChannelUrl&"/"
            If CreateMultiFolder(ArticlePath) = False Then
                Response.Write "请检查服务器。系统不能创建生成文件所需要的文件夹。"
                Exit Sub
            End If
            tmpFileName = ArticlePath & GetItemFileName(rsArticle("UpdateTime"), ArticleID)
            '生成页面时判定转向连接
            If rsArticle("LinkUrl") <> "" And rsArticle("LinkUrl") <> "http://" Then
                Call WriteToFile(tmpFileName, PW_Content.GetLinkUrlContent(rsArticle("LinkUrl"), ArticleID))
                Response.Write "<li>成功生成第 <font color='red'><b>" & iCount & " </b></font> " & ChannelItemUnit & ChannelShortName & "。&nbsp;&nbsp;ID：" & ArticleID & " &nbsp;&nbsp;标题：" & rsArticle("Title") & " &nbsp;&nbsp;地址：<a href='" & tmpFileName & "' target='_blank'>" & tmpFileName & "</a></li>" & vbCrLf
                Response.Flush
            Else
                ArticleUrl = GetArticleUrl(rsArticle("UpdateTime"), ArticleID, rsArticle("InfoPurview"))
                ArticleTitle = Replace(Replace(Replace(Replace(rsArticle("Title") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
				strHtml = strTemplate
                Call PW_Content.GetHtml_Article
				Call GetModelFront_Html()
                '写入生成地址
                Call WriteToFile(tmpFileName, strHtml)
                Response.Write "<li>成功生成第 <font color='red'><b>" & iCount & " </b></font> " & ChannelItemUnit & ChannelShortName & "。&nbsp;&nbsp;ID：" & ArticleID & " &nbsp;&nbsp;标题：" & rsArticle("Title") & " &nbsp;&nbsp;地址：<a href='" & tmpFileName & "' target='_blank'>" & tmpFileName & "</a></li>" & vbCrLf
                Response.Flush
            End If
            '生成内容结束，更新内容的生成时间
            Conn.Execute ("update PW_Article set CreateTime=" & PE_Now & " where ArticleID=" & ArticleID)

        End If
        If Response.IsClientConnected = False Then Exit Do
        If iCount Mod MaxPerPage_Create = 0 Then Exit Do
        rsArticle.MoveNext
    Loop
    rsArticle.Close
    Set rsArticle = Nothing
End Sub
%>