<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1 检查
Const PurviewLevel_Others = "AD"   '其他权限

Dim ADID, ZoneID, ZoneTypeArray

ZoneTypeArray=Array("矩形横幅","对联广告","图片切换(限JPG格式)","FLASH轮换")


ZoneID = GetValue("ZoneID")

If Action = "" Then
    Action = "ZoneList"
End If

If IsValidID(ZoneID) = False Then
    ZoneID = ""
End If
strFileName = "Admin_Advertisement.asp?Action=" & Action
Response.Write "<html><head><title>广告管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("网 站 广 告 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>" & vbCrLf
Response.Write "      <a href='Admin_Advertisement.asp?Action=ZoneList'>广告版位管理</a>&nbsp;|&nbsp;"
Response.Write "      <a href='Admin_Advertisement.asp?Action=AddZone'>添加新版位</a>&nbsp;|&nbsp;"
Response.Write "      <a href='Admin_Advertisement.asp?Action=ADList'>网站广告管理</a>&nbsp;|&nbsp;"
Response.Write "      <a href='Admin_Advertisement.asp?Action=AddAD'>添加新广告</a>&nbsp;|&nbsp;"
Response.Write "      <a href='Admin_UploadFile.asp?UploadDir=UploadPic'>广告上传图片管理</a>&nbsp;"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf


Select Case Action
Case "AddZone"
    Call AddZone
Case "ModifyZone"
    Call ModifyZone
Case "SaveAddZone", "SaveModifyZone"
    Call SaveZone
Case "SetZoneActive", "CancelZoneActive", "DelZone"
    Call SetZoneProperty
Case "ZoneList"
    Call ZoneList
Case "ZoneJSCode"
    Call ZoneJSCode
Case "CreateJSZone"
    Call CreateJSZone
Case "PreviewZone"
    Call PreviewZone
Case "AddAD"
    Call AddAD
Case "ModifyAD"
    Call ModifyAD
Case "SaveAddAD", "SaveModifyAD"
    Call SaveAD
Case "SetADPassed", "CancelADPassed", "DelAD"
    Call SetADProperty
Case "PreviewAD"
    Call PreviewAD
Case "ADList"
    Call ADList
Case Else
    Call ZoneList
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If

Response.Write "</body></html>"
Call CloseConn



Sub ZoneList()
    Dim rsZone, sqlZone
    Call ShowJS_Main("版位")
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td height='22'>您现在的位置：网站广告管理&nbsp;&gt;&gt;&nbsp;版位管理&nbsp;&gt;&gt;&nbsp;所有版位</td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_Advertisement.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "    <td>"
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "        <tr class='title' height='22'>"
    Response.Write "          <td width='30' align='center'><strong>选中</strong></td>"
    Response.Write "          <td width='30' align='center'><strong>ID</strong></td>"
    Response.Write "          <td align='center'><strong>版位名称</strong></td>"
    Response.Write "          <td width='65' align='center'><strong>版位类型</strong></td>"
    Response.Write "          <td width='35' align='center'><strong>显示</strong></td>"
    Response.Write "          <td width='65' align='center'><strong>版位尺寸</strong></td>"
    Response.Write "          <td width='30' align='center'><strong>活动</strong></td>"
    Response.Write "          <td width='120' align='center'><strong>操作</strong></td>"
    Response.Write "          <td width='80' align='center'><strong>版位JS</strong></td>"
    Response.Write "        </tr>"

    sqlZone = "select * from PW_AdZone order by ZoneID desc"

    Set rsZone = Server.CreateObject("ADODB.Recordset")
    rsZone.Open sqlZone, Conn, 1, 1
    If rsZone.BOF And rsZone.EOF Then
        Response.Write "        <tr class='tdbg'><td colspan='20' align='center'><br>没有任何广告版位！<br><br></td></tr>"
    Else
        totalPut = rsZone.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                rsZone.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If

        Dim ZoneNum
        ZoneNum = 0
        Do While Not rsZone.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
            Response.Write "          <td align='center'><input name='ZoneID' type='checkbox' value='" & rsZone("ZoneID") & "'></td>"
            Response.Write "          <td align='center'>" & rsZone("ZoneID") & "</td>"
            Response.Write "          <td>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=ADList&ZoneID=" & rsZone("ZoneID") & "' title='" & rsZone("ZoneIntro") & "'>" & rsZone("ZoneName") & "</a>"
            Response.Write "          </td>"
            Response.Write "          <td align='center'>" & ZoneTypeArray(rsZone("ZoneType")) & "</td>"
            Response.Write "          <td align='center'>"
            If rsZone("ShowType") = 2 Then
                Response.Write "优先"
            ElseIf rsZone("ShowType") = 3 Then
                Response.Write "循环"
            Else
                Response.Write "随机"
            End If
            Response.Write "          </td>"
            Response.Write "          <td align='center'>" & rsZone("ZoneWidth") & " x " & rsZone("ZoneHeight") & "</td>"
            Response.Write "          <td align='center'>"
            If rsZone("Active") = True Then
                Response.Write "<b>√</b>"
            Else
                Response.Write "<font color=red><b>×</b></font>"
            End If
            Response.Write "          </td>"
            Response.Write "          <td align='center'>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=AddAD&ZoneID=" & rsZone("ZoneID") & "'>添加</a>&nbsp;"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=ModifyZone&ZoneID=" & rsZone("ZoneID") & "'>修改</a>&nbsp;"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=DelZone&ZoneID=" & rsZone("ZoneID") & "' onClick=""return confirm('确定要删除此版位吗？');"">删除</a>&nbsp;"
            Response.Write "<br>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=ClearZone&ZoneID=" & rsZone("ZoneID") & "' onClick=""return confirm('确定要清空此版位吗？清空后原来的属于此版位的广告将不再属于版位。');"">清空</a>&nbsp;"
            If rsZone("Active") = False Then
                Response.Write "            <a href='Admin_Advertisement.asp?Action=SetZoneActive&ZoneID=" & rsZone("ZoneID") & "'>活动</a>"
            Else
                Response.Write "            <a href='Admin_Advertisement.asp?Action=CancelZoneActive&ZoneID=" & rsZone("ZoneID") & "'>暂停</a>"
            End If
            Response.Write "          <td width='80' align='center'>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=CreateJSZone&ZoneID=" & rsZone("ZoneID") & "'>刷新</a>&nbsp;"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=PreviewZone&ZoneID=" & rsZone("ZoneID") & "'>预览</a>"
            Response.Write "<br>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=ZoneJSCode&ZoneID=" & rsZone("ZoneID") & "'>JS调用代码</a>"
            Response.Write "          </td>"
            Response.Write "          </td>"
            Response.Write "        </tr>"

            ZoneNum = ZoneNum + 1
            If ZoneNum >= MaxPerPage Then Exit Do
            rsZone.MoveNext
        Loop
    End If
    rsZone.Close
    Set rsZone = Nothing
    Response.Write "      </table>"
    Response.Write "      <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "        <tr>"
    Response.Write "          <td width='130' height='30'>"
    Response.Write "            <input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的版位"
    Response.Write "          </td>"
    Response.Write "          <td>"
    Response.Write "            <input type='submit' value='删除选定版位' name='submit' onClick=""document.myform.Action.value='DelZone'"">&nbsp;"
    Response.Write "            <input type='submit' value='设为活动版位' name='submit' onClick=""document.myform.Action.value='SetZoneActive'"">&nbsp;"
    Response.Write "            <input type='submit' value='暂停版位显示' name='submit' onClick=""document.myform.Action.value='CancelZoneActive'"">&nbsp;"
    Response.Write "            <input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "          </td>"
    Response.Write "        </tr>"
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </form>"
    Response.Write "  </tr>"
    Response.Write "</table>"

    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "个版位", True)
    End If
    Response.Write "<br>"
End Sub


Sub ShowJS_Zone()
    Response.Write "<script language=JavaScript>" & vbCrLf
    Response.Write "<!--" & vbCrLf
    Response.Write "function CheckForm()" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  if(myform.ZoneName.value==''){" & vbCrLf
    Response.Write "    alert('版位名称不能为空！');" & vbCrLf
    Response.Write "    myform.ZoneName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "    if(myform.ZoneWidth.value=='' || myform.ZoneWidth.value=='0' ){" & vbCrLf
    Response.Write "      alert('版位宽度不能为空且必须大于0！');" & vbCrLf
    Response.Write "      myform.ZoneWidth.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "    if(myform.ZoneHeight.value=='' || myform.ZoneHeight.value=='0' ){" & vbCrLf
    Response.Write "      alert('版位高度不能为空且必须大于0！');" & vbCrLf
    Response.Write "      myform.ZoneHeight.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function Change_Setting()" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  if(document.myform.ZoneType[0].checked == true) {" & vbCrLf
    Response.Write "    document.myform.ShowType[0].checked = true;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.ZoneType[1].checked == true) {" & vbCrLf
    Response.Write "    document.myform.ShowType[1].checked = true;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.ZoneType[2].checked == true) {" & vbCrLf
    Response.Write "    document.myform.ShowType[1].checked = true;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.ZoneType[3].checked == true) {" & vbCrLf
    Response.Write "    document.myform.ShowType[1].checked = true;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
   Response.Write "//-->" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub AddZone()
    Call ShowJS_Zone
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Advertisement.asp' target='_self'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添 加 版 位</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位名称：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='ZoneName' type='text' id='ZoneName' size='60' maxlength='60' value=''> <font color='red'>*</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位描述：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <textarea name='ZoneIntro' cols='50' rows='3' id='ZoneIntro'></textarea>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位类型：</strong><br>选择放置于此版位的广告类型。</td>"
    Response.Write "      <td width='600'>"
	Call ShowZoneType_List(0)  
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位尺寸：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <table>"
    Response.Write "          <tr>"
    Response.Write "            <td>"
    Response.Write "              宽度: "
    Response.Write "              <input name='ZoneWidth' size='5' maxlength='4' value='0'>&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "              高度:"
    Response.Write "              <input name='ZoneHeight' size='5' maxlength='4' value='0'>"
    Response.Write "              <font color='red'>*</font>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>显示方式：</strong><br>当版位中有多个广告时按照此设定进行显示（依据广告的权重）。</td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='ShowType' type='radio' value='1' checked> 按权重优先显示&nbsp;&nbsp;显示权重值最大的广告,对矩形横幅有效。<br>"
    Response.Write "        <input name='ShowType' type='radio' value='2'> 按顺序循环显示&nbsp;&nbsp;对循环播放的图片类广告有效。"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位状态：</strong><br>设为活动的版位才能在前台显示。</td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='Active' type='checkbox' id='Active' value='yes' checked> 活动版位"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAddZone'>"
    Response.Write "        <input  type='submit' name='Submit' value=' 添 加 '>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Advertisement.asp?Action=ZoneList'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub


Sub ModifyZone()
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的版位ID</li>"
        Exit Sub
    Else
        ZoneID = PE_CLng(ZoneID)
    End If
    Dim rsZone, sqlZone
    sqlZone = "select * from PW_AdZone where ZoneID=" & ZoneID
    Set rsZone = Conn.Execute(sqlZone)
    If rsZone.BOF And rsZone.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的版位</li>"
        rsZone.Close
        Set rsZone = Nothing
        Exit Sub
    End If
    Call ShowJS_Zone
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Advertisement.asp' target='_self'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修 改 版 位</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位名称：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='ZoneName' type='text' id='ZoneName' size='60' maxlength='60' value='" & rsZone("ZoneName") & "'> <font color='red'>*</font>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位描述：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <textarea name='ZoneIntro' cols='50' rows='3' id='ZoneIntro'>" & PE_ConvertBR(rsZone("ZoneIntro")) & "</textarea>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位类型：</strong><br>选择放置于此版位的广告类型。</td>"
    Response.Write "      <td width='600'>"
	Call ShowZoneType_List(rsZone("ZoneType"))  
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位尺寸：</strong></td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <table>"
    Response.Write "          <tr>"
    Response.Write "            <td>"
    Response.Write "              宽度: "
    Response.Write "              <input name='ZoneWidth' size='5' maxlength='4' value='" & rsZone("ZoneWidth") & "'>&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "              高度:"
    Response.Write "              <input name='ZoneHeight' size='5' maxlength='4' value='" & rsZone("ZoneHeight") & "'>"
    Response.Write "              <font color='red'>*</font>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>显示方式：</strong><br>当版位中有多个广告时按照此设定进行显示（依据广告的权重）。</td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='ShowType' type='radio' value='1' " & IsRadioChecked(rsZone("ShowType"), 1) & "> 按权重优先显示&nbsp;&nbsp;显示权重值最大的广告。<br>"
    Response.Write "        <input name='ShowType' type='radio' value='2' " & IsRadioChecked(rsZone("ShowType"), 2)
    Response.Write "> 按顺序循环显示&nbsp;&nbsp;此方式仅对循环播放的图片类广告有效。"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200'><strong>版位状态：</strong><br>设为活动的版位才能在前台显示。</td>"
    Response.Write "      <td width='600'>"
    Response.Write "        <input name='Active' type='checkbox' id='Active' value='yes' " & IsRadioChecked(rsZone("Active"), True) & "> 活动版位"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='ZoneID' type='hidden' id='ZoneID' value='" & rsZone("ZoneID") & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModifyZone'>"
    Response.Write "        <input  type='submit' name='Submit' value=' 修 改 '>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Advertisement.asp?Action=ZoneList'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
    rsZone.Close
    Set rsZone = Nothing
End Sub


Sub SaveZone()
Dim ZoneID,ZoneName,ZoneIntro, ZoneType,ZoneWidth, ZoneHeight, ShowType, Active
    Dim rsZone, sqlZone
    ZoneID = PE_CLng(GetForm("ZoneID"))
    ZoneName = GetForm("ZoneName")
    ZoneIntro = GetForm("ZoneIntro")
    ZoneType = PE_CLng(GetForm("ZoneType"))
    ZoneWidth = PE_CLng(GetForm("ZoneWidth"))
    ZoneHeight = PE_CLng(GetForm("ZoneHeight"))
    ShowType = PE_CLng(GetForm("ShowType"))
    Active = PE_CBool(GetForm("Active"))
    If ZoneName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>版位名称不能为空！</li>"
    End If
    If ZoneWidth <=0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>版位宽度不能小于零！</li>"
    End If
    If ZoneHeight <=0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>版位高度不能小于零！</li>"
    End If
    If FoundErr = True Then Exit Sub

    ZoneName = PE_HTMLEncode(ZoneName)
    ZoneIntro = PE_HTMLEncode(ZoneIntro)
    Set rsZone = Server.CreateObject("adodb.recordset")
    If Action = "SaveAddZone" Then
        sqlZone = "select top 1 * from PW_AdZone"
        rsZone.Open sqlZone, Conn, 1, 3
        rsZone.addnew
        rsZone("UpdateTime") = Now()
    ElseIf Action = "SaveModifyZone" Then
        If ZoneID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定版位ID的值</li>"
            Exit Sub
        End If
        sqlZone = "select * from PW_AdZone where ZoneID=" & ZoneID
        rsZone.Open sqlZone, Conn, 1, 3
        If rsZone.BOF And rsZone.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的版位！</li>"
            rsZone.Close
            Set rsZone = Nothing
            Exit Sub
        End If
    End If
    rsZone("ZoneName") = ZoneName
    rsZone("ZoneIntro") = ZoneIntro
    rsZone("ZoneType") = ZoneType
    rsZone("ZoneWidth") = ZoneWidth
    rsZone("ZoneHeight") = ZoneHeight
    rsZone("ShowType") = ShowType
    rsZone("Active") = Active
    rsZone.Update
    rsZone.Close
    Set rsZone = Nothing
    Call CreateJSZoneID(ZoneID)
    Call CloseConn
    Response.Redirect "Admin_Advertisement.asp?Action=ZoneList&ChannelID=" & ChannelID
End Sub


Sub SetZoneProperty()
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定版位ID</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If

    Dim sqlProperty, rsProperty
    Dim MoveChannelID
    If InStr(ZoneID, ",") > 0 Then
        sqlProperty = "select * from PW_AdZone where ZoneID in (" & ZoneID & ")"
    Else
        sqlProperty = "select * from PW_AdZone where ZoneID=" & ZoneID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
        Select Case Action
        Case "SetZoneActive"
            rsProperty("Active") = True
        Case "CancelZoneActive"
            rsProperty("Active") = False
        Case "DelZone"
            Call DelZoneID_AD(rsProperty("IncludeADID"), rsProperty("ZoneID"))
            Dim ZoneJSFile
            ZoneJSFile = GetZoneJSName(rsProperty("ZoneID"))
            If fso.FileExists(Server.MapPath(ZoneJSFile)) Then
                fso.DeleteFile Server.MapPath(ZoneJSFile)
            End If
            rsProperty.Delete
        End Select
        rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    If Action = "SetZoneActive" Or Action = "CancelZoneActive" Then
        Call CreateJSZoneID(ZoneID)
    End If
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub ClearZone()
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ZoneID = PE_CLng(ZoneID)
    End If

    Dim rs, IncludeADID
    Set rs = Conn.Execute("select IncludeADID from PW_AdZone where ZoneID=" & ZoneID)
    If rs.BOF And rs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>版位不存在，或者已经被删除</li>"
    Else
        IncludeADID = rs(0)
    End If
    rs.Close
    Set rs = Nothing
    If FoundErr = True Then Exit Sub

    Conn.Execute ("update PW_AdZone set IncludeADID='' where ZoneID=" & ZoneID)
    Call DelZoneID_AD(IncludeADID, ZoneID)

    Call CreateJSZoneID(ZoneID)
    Call WriteSuccessMsg("已经成功清空本版位下的广告。", ComeUrl)
End Sub


Sub CreateJSZone()
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定版位ID</li>"
        Exit Sub
    End If
    Call CreateJSZoneID(ZoneID)
    Call WriteSuccessMsg("刷新版位JS成功！", ComeUrl)
End Sub


Sub ZoneJSCode()
    Dim ID, sqlJs, rsJs
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数丢失！</li>"
        Exit Sub
    Else
        ZoneID = PE_CLng(ZoneID)
    End If
    sqlJs = "select ZoneID,ZoneName,UpdateTime from PW_AdZone where ZoneID=" & ZoneID
    Set rsJs = Conn.Execute(sqlJs)
    If rsJs.BOF And rsJs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的版位！</li>"
        rsJs.Close
        Set rsJs = Nothing
        Exit Sub
    End If

    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='2' align='center'><strong>版位JS调用代码----" & rsJs("ZoneName") & "</strong></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg2'>"
    Response.Write "    <td height='25' align='center'>调用方法：将下面的代码插入到网页中预定的广告位置</td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='100' align='center'>"
    Response.Write "      <textarea name='ZoneJSCode' cols='100' rows='5' id='ZoneJSCode'><script language=""javascript"" src=""" & GetZoneJSNameCode(rsJs("ZoneID")) & """></script></textarea>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='25' align='center'><a href='" & ComeUrl & "'>返回</a></td>"
    Response.Write "  </tr>"
    Response.Write "</table>"

    rsJs.Close
    Set rsJs = Nothing
End Sub

Sub PreviewZone()
    Dim ID, sqlJs, rsJs
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数丢失！</li>"
        Exit Sub
    Else
        ZoneID = PE_CLng(ZoneID)
    End If
    sqlJs = "select ZoneID,IncludeADID,ZoneName,UpdateTime from PW_AdZone where ZoneID=" & ZoneID
    Set rsJs = Conn.Execute(sqlJs)
    If rsJs.BOF And rsJs.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的版位！</li>"
        rsJs.Close
        Set rsJs = Nothing
        Exit Sub
    End If

    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='2' align='center'><strong>预览版位JS效果----" & rsJs("ZoneName") & "</strong></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg2'>"
    Response.Write "    <td height='25' align='center'>"
    Response.Write "      <a href='javascript:this.location.reload();'>刷新页面</a>&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "      <a href='" & ComeUrl & "'>返回上页</a>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "  <tr valign='top'>"
    If IsNull(rsJs("IncludeADID")) Or rsJs("IncludeADID") = "" Then
        Response.Write "    <td height='200' align='center'><br><br><br><br>版位中暂时还未添加广告，请添加后再进行预览！</td>"
    Else
        Response.Write "    <td height='800'><script language=""javascript"" src=""" & GetZoneJSName(rsJs("ZoneID")) & """></script></td>"
    End If
    Response.Write "  </tr>"
    Response.Write "</table>"

    rsJs.Close
    Set rsJs = Nothing
End Sub


Sub DelZoneID_AD(arrADID, iZoneID)
    If iZoneID = "" Or IsNull(iZoneID) Then
        Exit Sub
    Else
        iZoneID = PE_CLng(iZoneID)
    End If
    If IsValidID(arrADID) = True Then
        Dim sqlAD, rsAD, Adfile
        arrADID = ReplaceBadChar(arrADID)
        sqlAD = "select * from PW_Advertisement where ADID in (" & arrADID & ") and ZoneID='"& iZoneID &"'"
        Set rsAD = Server.CreateObject("Adodb.RecordSet")
        rsAD.Open sqlAD, Conn, 1, 3
        Do While Not rsAD.EOF
				Adfile=rsAD("ImgUrl")
				Adfile=GetPhotoUrl(Adfile)
				If fso.FileExists(Server.MapPath(Adfile)) Then
					fso.DeleteFile Server.MapPath(Adfile)
				End If
			rsAD.delete
            rsAD.Update
            rsAD.MoveNext
        Loop
        rsAD.Close
        Set rsAD = Nothing
    End If
End Sub




Sub ShowZoneType_List(iZoneTypeNum)
	Dim I
	for I=0 to ubound(ZoneTypeArray)
		Response.write "<input type='radio' name='ZoneType' value='"& I &"' onclick='Change_Setting();' " & IsRadioChecked(iZoneTypeNum, I) & " />"&ZoneTypeArray(I)&"&nbsp;&nbsp;"
	next
End Sub

Function GetZoneJSName(iZoneID)
        GetZoneJSName = InStallDir & ADDir & "/" & iZoneID & ".js"
End Function

Function GetZoneJSNameCode(iZoneID)
    GetZoneJSNameCode = "{$installdir}{$addir}/" & iZoneID & ".js"
End Function


Sub CreateJSZoneID(ZoneID)
    If ObjInstalled_FSO = False Then
        Exit Sub
    End If
	Dim arrZoneID, j, rsZone, sqlZone, ZoneJS_Path, ZoneJS_Name, strADJS
    Dim ZoneType, IncludeADID, ShowType, ZoneWidth, ZoneHeight, Active
	Dim PicStr,LinkStr,TextStr
    If IsValidID(ZoneID) = False Then
        Exit Sub
    Else
        ZoneID = Replace(ZoneID, " ", "")
    End If
	
    arrZoneID = Split(ZoneID, ",")
    For j = 0 To UBound(arrZoneID)
        sqlZone = "select * from PW_AdZone where ZoneID=" & arrZoneID(j)
        Set rsZone = Conn.Execute(sqlZone)
        If Not rsZone.BOF And Not rsZone.EOF Then
            ZoneJS_Path = InstallDir & ADDir &"/"
            ZoneJS_Name = ZoneJS_Path & rsZone("ZoneID") &".js"
            ZoneType = PE_CLng(rsZone("ZoneType"))
            IncludeADID = rsZone("IncludeADID")
            ShowType = rsZone("ShowType")
            ZoneWidth = rsZone("ZoneWidth")
            ZoneHeight = rsZone("ZoneHeight")
            Active = rsZone("Active")
            strADJS = ""
            If IsValidID(IncludeADID) = True And Active = True Then
                Dim rsAD, sqlAD,ADNum,N
				set rsAD=server.CreateObject("Adodb.recordset")
				
                sqlAD = "select * from PW_Advertisement where Passed=" & PE_True & " and ADID in (" & IncludeADID & ")"
				sqlAD = sqlAD &" order by Priority desc, ADID desc"
                rsAD.open sqlAD,conn,1,1
                If Not (rsAD.BOF And rsAD.EOF) Then
					N=0
					ADNum=rsAd.recordcount
					Select Case ZoneType
						Case 0     '矩形横幅
							Select case ShowType
								Case 1
									Select Case	rsAD("ADType")
										Case 1     '图片
											strADJS = strADJS & "document.write(""<table width='"& ZoneWidth &"' height='"& ZoneHeight &"' border='0' cellspacing='0' cellpadding='0'><tr><td>"
											strADJS = strADJS & "<a href='"& rsAd("LinkUrl") &"'"
											if  rsAd("LinkTarget")=1 then strADJS = strADJS & " target='_blank'"
											strADJS = strADJS& "><img src='"& rsAd("ImgUrl") &"'"
											if rsAd("ImgWidth")>0 then strADJS = strADJS & " width='"& rsAd("ImgWidth") &"'"
											if rsAd("ImgHeight")>0 then strADJS = strADJS & " height='"& rsAd("ImgHeight") &"'"
											strADJS = strADJS& " border='0' ></td></tr></table>"")"
										case 2    '' flash
											strADJS = strADJS & "document.write(""<table width='"& ZoneWidth &"' height='"& ZoneHeight &"' border='0' cellspacing='0' cellpadding='0'><tr><td>"
											strADJS = strADJS & "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'"
											If rsAd("ImgWidth") > 0 Then strADJS = strADJS & " width='" & rsAd("ImgWidth") & "'"
											If rsAd("ImgHeight") > 0 Then strADJS = strADJS & " height='" & rsAd("ImgHeight") & "'"
											strADJS = strADJS & "><param name='movie' value='" & rsAd("ImgUrl") & "'>"
											strADJS = strADJS & "<param name='quality' value='autohigh'>"
											strADJS = strADJS & "<param name='wmode' value='Opaque'>"											
											strADJS = strADJS & "<embed src='" & rsAD("ImgUrl") & "' quality='autohigh'"
											strADJS = strADJS & " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash'"
											If rsAd("ImgWidth") > 0 Then strADJS = strADJS & " width='" & rsAd("ImgWidth") & "'"
											If rsAd("ImgHeight") > 0 Then strADJS = strADJS & " height='" & rsAd("ImgHeight") & "'"
											strADJS = strADJS & "></embed></object></td></tr></table>"")"
									End Select
								Case 2
									DIM I,defaultPic
									I=0
									defaultPic = rsAD("ImgUrl")
									strADJS = strADJS & "var bannerAD=new Array(); "& vbCrLf
									strADJS = strADJS & "var bannerADlink=new Array(); "& vbCrLf
									strADJS = strADJS & "var adNum=0; "& vbCrLf
									do while not rsAD.eof
											strADJS = strADJS & "bannerAD["& I &"]="""& rsAD("ImgUrl") &""""&vbcrlf
											strADJS = strADJS & "bannerADlink["& I &"]="""& rsAD("LinkUrl") &""""&vbcrlf
										rsAD.movenext
										I=I+1
									loop
									strADJS = strADJS & "var preloadedimages=new Array(); " & vbCrLf
									strADJS = strADJS & "for (i=1;i<bannerAD.length;i++){ " & vbCrLf
									strADJS = strADJS & "preloadedimages[i]=new Image(); " & vbCrLf
									strADJS = strADJS & "preloadedimages[i].src=bannerAD[i]; " & vbCrLf
									strADJS = strADJS & "}" & vbCrLf
									strADJS = strADJS & "" & vbCrLf
									strADJS = strADJS & "function setTransition(){ " & vbCrLf
									strADJS = strADJS & "if (document.all){ " & vbCrLf
									strADJS = strADJS & "bannerADrotator.filters.revealTrans.Transition=Math.floor(Math.random()*23); " & vbCrLf
									strADJS = strADJS & "bannerADrotator.filters.revealTrans.apply(); " & vbCrLf
									strADJS = strADJS & "} " & vbCrLf
									strADJS = strADJS & "}" & vbCrLf
									strADJS = strADJS & "" & vbCrLf
									strADJS = strADJS & "function playTransition(){ " & vbCrLf
									strADJS = strADJS & "if (document.all) " & vbCrLf
									strADJS = strADJS & "bannerADrotator.filters.revealTrans.play() " & vbCrLf
									strADJS = strADJS & "}" & vbCrLf
									strADJS = strADJS & "" & vbCrLf
									strADJS = strADJS & "function nextAd(){ " & vbCrLf
									strADJS = strADJS & "if(adNum<bannerAD.length-1)adNum++ ; " & vbCrLf
									strADJS = strADJS & "else adNum=0; " & vbCrLf
									strADJS = strADJS & "setTransition(); " & vbCrLf
									strADJS = strADJS & "document.images.bannerADrotator.src=bannerAD[adNum]; " & vbCrLf
									strADJS = strADJS & "playTransition(); " & vbCrLf
									strADJS = strADJS & "theTimer=setTimeout(""nextAd()"", 5000); " & vbCrLf
									strADJS = strADJS & "}" & vbCrLf
									strADJS = strADJS & "" & vbCrLf
									strADJS = strADJS & "function jump2url(){ " & vbCrLf
									strADJS = strADJS & "jumpUrl=bannerADlink[adNum]; " & vbCrLf
									strADJS = strADJS & "jumpTarget='_blank'; " & vbCrLf
									strADJS = strADJS & "if (jumpUrl != ''){ " & vbCrLf
									strADJS = strADJS & "if (jumpTarget != '')window.open(jumpUrl,jumpTarget); " & vbCrLf
									strADJS = strADJS & "else location.href=jumpUrl; " & vbCrLf
									strADJS = strADJS & "} " & vbCrLf
									strADJS = strADJS & "} " & vbCrLf
									strADJS = strADJS & "function displayStatusMsg() { " & vbCrLf
									strADJS = strADJS & "status=bannerADlink[adNum]; " & vbCrLf
									strADJS = strADJS & "document.returnValue = true; " & vbCrLf
									strADJS = strADJS & "}" & vbCrLf
									strADJS = strADJS & "" & vbCrLf
									strADJS = strADJS & "document.write(""<img style='FILTER: revealTrans(duration=2,transition=20)' width='"& ZoneWidth &"' height='"& ZoneHeight &"' src='"& defaultPic &"' border=0  onclick='jump2url()' style='cursor:hand' name=bannerADrotator>"");" & vbCrLf
									strADJS = strADJS & "nextAd()" & vbCrLf
							End Select
						Case 1     '对联广告
							strADJS = strADJS & "var lastScrollY=0;     " & vbCrLf
							strADJS = strADJS & "function heartBeat(){    " & vbCrLf
							strADJS = strADJS & "var diffY;     " & vbCrLf
							strADJS = strADJS & "if (document.documentElement && document.documentElement.scrollTop)     " & vbCrLf
							strADJS = strADJS & "    diffY = document.documentElement.scrollTop;     " & vbCrLf
							strADJS = strADJS & "else if (document.body)     " & vbCrLf
							strADJS = strADJS & "    diffY = document.body.scrollTop     " & vbCrLf
							strADJS = strADJS & "else     " & vbCrLf
							strADJS = strADJS & "    {/*Netscape stuff*/}     " & vbCrLf
							strADJS = strADJS & "percent=.1*(diffY-lastScrollY);      " & vbCrLf
							strADJS = strADJS & "if(percent>0)percent=Math.ceil(percent);      " & vbCrLf
							strADJS = strADJS & "else percent=Math.floor(percent);      " & vbCrLf
							strADJS = strADJS & "document.getElementById(""adleft"").style.top=parseInt(document.getElementById(""adleft"").style.top)+percent+""px"";     " & vbCrLf
							strADJS = strADJS & "document.getElementById(""adright"").style.top=parseInt(document.getElementById(""adright"").style.top)+percent+""px"";     " & vbCrLf
							strADJS = strADJS & "lastScrollY=lastScrollY+percent;      " & vbCrLf
							strADJS = strADJS & "}" & vbCrLf
							strADJS = strADJS & "function adhide(names){document.getElementById(names).style.display='none';}" & vbCrLf
							strADJS = strADJS & "function screencl(names){if(screen.width<=800){adhide(names);}}    " & vbCrLf
							strADJS = strADJS & "lcode=""<div id='adleft' style='left:10px;position: absolute;z-index:1;top:100px;'>"
							Dim leftAdContent
							leftAdContent=""
							Select case rsAD("ADType")
								Case 1    '' 图片
									leftAdContent = leftAdContent & "<a href='"& rsAd("LinkUrl") &"'"
									if  rsAd("LinkTarget")=1 then leftAdContent = leftAdContent & " target='_blank'"
									leftAdContent = leftAdContent & "><img src='"& rsAd("ImgUrl") &"'"
									if rsAd("ImgWidth")>0 then leftAdContent = leftAdContent & " width='"& rsAd("ImgWidth") &"'"
									if rsAd("ImgHeight")>0 then leftAdContent = leftAdContent & " height='"& rsAd("ImgHeight") &"'"
									leftAdContent = leftAdContent & " border='0' ></a>"
								case 2    '' flash
									leftAdContent = leftAdContent &"<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'"
									If rsAd("ImgWidth") > 0 Then leftAdContent = leftAdContent & " width='" & rsAd("ImgWidth") & "'"
									If rsAd("ImgHeight") > 0 Then leftAdContent = leftAdContent & " height='" & rsAd("ImgHeight") & "'"
									leftAdContent = leftAdContent & "><param name='movie' value='" & rsAd("ImgUrl") & "'>"
									leftAdContent = leftAdContent & "<param name='quality' value='autohigh'><param name='wmode' value='Opaque'>"
									leftAdContent = leftAdContent & "<embed src='" & rsAd("ImgUrl") & "' quality='autohigh'"
									leftAdContent = leftAdContent & " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash'"
									If rsAd("ImgWidth") > 0 Then leftAdContent = leftAdContent & " width='" & rsAd("ImgWidth") & "'"
									If rsAd("ImgHeight") > 0 Then leftAdContent = leftAdContent & " height='" & rsAd("ImgHeight") & "'"
									leftAdContent = leftAdContent & "></embed></object>"
							end select
							strADJS = strADJS & leftAdContent &"<br><div title='关闭' onclick=adhide('adleft') style='cursor:pointer; text-align:right'>[X]</div></div>"";"&vbcrlf
							strADJS = strADJS & "document.write(lcode);" & vbCrLf
							strADJS = strADJS & "screencl('adleft');    " & vbCrLf
							strADJS = strADJS & "rcode=""<div id='adright' style='right:10px;position: absolute;z-index:1;top:100px;'>"
							Dim rightAdContent
							rightAdContent=""
							if ADNum>=2 then
								rsAd.movenext
								Select case rsAD("ADType")
								Case 1    '' 图片
									rightAdContent = rightAdContent & "<a href='"& rsAd("LinkUrl") &"'"
									if  rsAd("LinkTarget")=1 then rightAdContent = rightAdContent & " target='_blank'"
									rightAdContent = rightAdContent & "><img src='"& rsAd("ImgUrl") &"'"
									if rsAd("ImgWidth")>0 then rightAdContent = rightAdContent & " width='"& rsAd("ImgWidth") &"'"
									if rsAd("ImgHeight")>0 then rightAdContent = rightAdContent & " height='"& rsAd("ImgHeight") &"'"
									rightAdContent = rightAdContent & " border='0' ></a>"
								case 2    '' flash
									rightAdContent = rightAdContent &"<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'"
									If rsAd("ImgWidth") > 0 Then rightAdContent = rightAdContent & " width='" & rsAd("ImgWidth") & "'"
									If rsAd("ImgHeight") > 0 Then rightAdContent = rightAdContent & " height='" & rsAd("ImgHeight") & "'"
									rightAdContent = rightAdContent & "><param name='movie' value='" & rsAd("ImgUrl") & "'>"
									rightAdContent = rightAdContent & "<param name='quality' value='autohigh'><param name='wmode' value='Opaque'>"
									rightAdContent = rightAdContent & "<embed src='" & rsAd("ImgUrl") & "' quality='autohigh'"
									rightAdContent = rightAdContent & " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash'"
									If rsAd("ImgWidth") > 0 Then rightAdContent = rightAdContent & " width='" & rsAd("ImgWidth") & "'"
									If rsAd("ImgHeight") > 0 Then rightAdContent = rightAdContent & " height='" & rsAd("ImgHeight") & "'"
									rightAdContent = rightAdContent & "></embed></object>"
								end select
							else
								rightAdContent = leftAdContent
							end if
								strADJS = strADJS & rightAdContent & "<br><div title='关闭' onclick=adhide('adright') style='cursor:pointer; text-align:right'>[X]</div></div>"";"&vbcrlf
								strADJS = strADJS & "document.write(rcode);    " & vbCrLf
								strADJS = strADJS & "screencl('adright');    " & vbCrLf
							strADJS = strADJS & "window.setInterval(""heartBeat()"",1);    " & vbCrLf
						Case 2     '图片切换
							strADJS = strADJS & "<!--" & vbCrLf
							strADJS = strADJS & "var swf_width = "& ZoneWidth &";" & vbCrLf
							strADJS = strADJS & "var swf_height = "& ZoneHeight-20 &";" & vbCrLf
							strADJS = strADJS & "var config = '0xFFFFFF|0x2|0099FF|15|0xFFFFFF|0x0099FF|0x000000|8|3|1|_blank';" & vbCrLf
							PicStr=""
							LinkStr=""
							TextStr=""
							do while not rsAD.eof
								PicStr = PicStr & rsAD("ImgUrl")&"|"
								LinkStr = LinkStr & rsAd("LinkUrl")&"|"
								TextStr=TextStr & GetSubStr(rsAd("ADName"),40,True)&"|"
								rsAD.movenext
								N=N+1
								if N>=9 then exit do
							loop
							PicStr=left(PicStr,len(PicStr)-1)
							LinkStr=left(LinkStr,len(LinkStr)-1)
							TextStr=left(TextStr,len(TextStr)-1)
							strADJS = strADJS & "var files = '"& PicStr &"';" & vbCrLf
							strADJS = strADJS & "var links = '"& LinkStr &"';" & vbCrLf
							strADJS = strADJS & "var texts = '"& TextStr &"';" & vbCrLf
							strADJS = strADJS & "document.write('<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"" width=""' + swf_width + '"" height=""' + swf_height + '"">');" & vbCrLf
							strADJS = strADJS & "document.write('<param name=""movie"" value="""& InstallDir &"Flash/advFocus.swf"">'); document.write('<param name=""quality"" value=""high"">'); document.write('<param name=""menu"" value=""false"">');" & vbCrLf
							strADJS = strADJS & "document.write('<param name=""wmode"" value=""opaque"">'); document.write('<param name=""FlashVars"" value=""bcastr_config=' + config + '&bcastr_file=' + files + '&bcastr_link=' + links + '&bcastr_title=' + texts + '"">');" & vbCrLf
							strADJS = strADJS & "document.write('<embed src="""& InstallDir &"Flash/advFocus.swf"" wmode=""opaque"" FlashVars=""bcastr_config=' + config + '&bcastr_file=' + files + '&bcastr_link=' + links + '&bcastr_title=' + texts + '"" menu=""false"" quality=""high"" width=""' + swf_width + '"" height=""' + swf_height + '"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer""></embed>');" & vbCrLf
							strADJS = strADJS & "document.write('</object>');" & vbCrLf
							strADJS = strADJS &  "//-->" & vbCrLf
						Case 3     'FLASH切换
							strADJS = strADJS & "<!--" & vbCrLf
							strADJS = strADJS &  "var focus_width="& ZoneWidth &" //宽度" & vbCrLf
							strADJS = strADJS &  "var focus_height="& ZoneHeight-20 &" //高度" & vbCrLf
							strADJS = strADJS &  "var text_height=20 //标题高度" & vbCrLf
							strADJS = strADJS &  "var swf_height = focus_height+text_height" & vbCrLf
							strADJS = strADJS &  "" & vbCrLf
							PicStr=""
							LinkStr=""
							TextStr=""
							do while not rsAD.eof
								PicStr = PicStr & rsAD("ImgUrl")&"|"
								LinkStr = LinkStr & rsAd("LinkUrl")&"|"
								TextStr=TextStr & GetSubStr(rsAd("ADName"),40,True)&"|"
								rsAD.movenext
								N=N+1
								if N>=5 then exit do
							loop
							PicStr=left(PicStr,len(PicStr)-1)
							LinkStr=left(LinkStr,len(LinkStr)-1)
							TextStr=left(TextStr,len(TextStr)-1)
							strADJS = strADJS &  "var pics= '"& PicStr &"'//图片地址" & vbCrLf
							strADJS = strADJS &  "var links='"& LinkStr &"'//相对于图片的链接地址" & vbCrLf
							strADJS = strADJS &  "var texts='"& TextStr &"'//标题文字" & vbCrLf
							strADJS = strADJS &  "document.write('<object classid=""clsid:d27cdb6e-ae6d-11cf-96b8-444553540000"" codebase=""http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"" width=""'+ focus_width +'"" height=""'+ swf_height +'"">');" & vbCrLf
							strADJS = strADJS &  "document.write('<param name=""allowScriptAccess"" value=""sameDomain""><param name=""movie"" value="""& InstallDir &"Flash/focus.swf""><param name=""quality"" value=""high""><param name=""bgcolor"" value=""#F0F0F0"">');" & vbCrLf
							strADJS = strADJS &  "document.write('<param name=""menu"" value=""false""><param name=wmode value=""opaque"">');" & vbCrLf
							strADJS = strADJS &  "document.write('<param name=""FlashVars"" value=""pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'"">');" & vbCrLf
							strADJS = strADJS &  "document.write('<embed src=""pixviewer.swf"" wmode=""opaque"" FlashVars=""pics='+pics+'&links='+links+'&texts='+texts+'&borderwidth='+focus_width+'&borderheight='+focus_height+'&textheight='+text_height+'"" menu=""false"" bgcolor=""#F0F0F0"" quality=""high"" width=""'+ focus_width +'"" height=""'+ swf_height +'"" allowScriptAccess=""sameDomain"" type=""application/x-shockwave-flash"" pluginspage=""http://www.macromedia.com/go/getflashplayer""  />');" & vbCrLf
							strADJS = strADJS &  "document.write('</object>');" & vbCrLf
							strADJS = strADJS &  "//-->" & vbCrLf
					End Select
                End If
                rsAD.Close
                Set rsAD = Nothing
            End If
            If Not fso.FolderExists(Server.MapPath(ZoneJS_Path)) Then
                fso.CreateFolder Server.MapPath(ZoneJS_Path)
            End If
            Call WriteToFile(ZoneJS_Name, strADJS)
        End If
        rsZone.Close
        Set rsZone = Nothing
    Next
End Sub


'''''''''''''''''''' 广告部分  ''''''''''''''''''''''''''''''''''''''''''''''''''

Sub ShowJS_AD()
    Response.Write "<script language=JavaScript>" & vbCrLf
    Response.Write "<!--" & vbCrLf
    Response.Write "function CheckForm()" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  if(myform.ADName.value==''){" & vbCrLf
    Response.Write "    alert('广告名称不能为空！');" & vbCrLf
    Response.Write "    myform.ADName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[0].checked == true && myform.ImgUrl.value==''){" & vbCrLf
    Response.Write "    alert('图片地址不能为空！');" & vbCrLf
    Response.Write "    myform.ImgUrl.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[0].checked == true && checkRate(myform.ImgWidth.value)==false){" & vbCrLf
    Response.Write "    alert('图片宽度必须大于0！');" & vbCrLf
    Response.Write "    myform.ImgWidth.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[0].checked == true && checkRate(myform.ImgHeight.value)==false){" & vbCrLf
    Response.Write "    alert('图片高度必须大于0！');" & vbCrLf
    Response.Write "    myform.ImgHeight.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[1].checked == true && myform.FlashUrl.value==''){" & vbCrLf
    Response.Write "    alert('动画地址不能为空！');" & vbCrLf
    Response.Write "    myform.FlashUrl.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[1].checked == true && checkRate(myform.FlashWidth.value)==false){" & vbCrLf
    Response.Write "    alert('FLASH宽度必须大于0！');" & vbCrLf
    Response.Write "    myform.FlashWidth.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.ADType[1].checked == true && checkRate(myform.FlashHeight.value)==false){" & vbCrLf
    Response.Write "    alert('FLASH高度必须大于0！');" & vbCrLf
    Response.Write "    myform.FlashHeight.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(myform.Priority.value==''){" & vbCrLf
    Response.Write "    alert('广告权重不能为空！');" & vbCrLf
    Response.Write "    myform.Priority.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function ADTypeChecked(i)" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  document.myform.ADType[i].checked = true;" & vbCrLf
    Response.Write "  Change_ADType();" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "" & vbCrLf
	Response.Write "function checkRate(input)   " & vbCrLf
	Response.Write "{   " & vbCrLf
	Response.Write "     var re = /^[1-9]+[0-9]*]*$/;   " & vbCrLf
	Response.Write "  " & vbCrLf
	Response.Write "     if (!re.test(input))   " & vbCrLf
	Response.Write "    {   " & vbCrLf
	Response.Write "        return false;   " & vbCrLf
	Response.Write "     }   " & vbCrLf
	Response.Write "}" & vbCrLf
    Response.Write "function Change_ADType()" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "  for (var j=0;j<document.myform.ADType.length;j++){" & vbCrLf
    Response.Write "    var ot = eval('document.all.ADContent_' + (j + 1) + '');" & vbCrLf
    Response.Write "    if(document.myform.ADType[j].checked==true){" & vbCrLf
    Response.Write "      ot.style.display = '';" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "    else{" & vbCrLf
    Response.Write "      ot.style.display = 'none';" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "//-->" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub ADList()
    Dim rsAD, sqlAD
    Call ShowJS_Main("广告")
    strFileName = "Admin_Advertisement.asp?Action=ADList&ZoneID="&PE_Clng(ZoneID)
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td height='22'>" & GetADManagePath() & "</td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_Advertisement.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "    <td>"
    Response.Write "      <table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "        <tr class='title' height='22'>"
    Response.Write "          <td width='30' align='center'><strong>选中</strong></td>"
    Response.Write "          <td width='30' align='center'><strong>ID</strong></td>"
    Response.Write "          <td width='65' align='center'><strong>广告预览</strong></td>"
    Response.Write "          <td align='center'><strong>广告名称</strong></td>"
    Response.Write "          <td width='60' align='center'><strong>广告类型</strong></td>"
    Response.Write "          <td width='40' align='center'><strong>权重</strong></td>"
    Response.Write "          <td width='40' align='center'><strong>已审核</strong></td>"
    Response.Write "          <td width='150' align='center'><strong>操作</strong></td>"
    Response.Write "        </tr>"
    sqlAD = "select * from PW_Advertisement where 1=1"
    If ZoneID <> "" Then
        Dim tZone
        Set tZone = Conn.Execute("select IncludeADID from PW_AdZone where ZoneID=" & PE_CLng(ZoneID))
        If Not (tZone.BOF And tZone.EOF) Then
            If Not IsNull(tZone(0)) And tZone(0) <> "" Then
                sqlAD = sqlAD & " and ADID in (" & tZone(0) & ") "
            Else
                sqlAD = sqlAD & " and 1=0 "
            End If
        End If
        Set tZone = Nothing
    End If
    sqlAD = sqlAD & " order by ADID desc"
    Set rsAD = Server.CreateObject("ADODB.Recordset")
    rsAD.Open sqlAD, Conn, 1, 1
    If rsAD.BOF And rsAD.EOF Then
        Response.Write "        <tr class='tdbg'><td colspan='20' align='center'><br>没有任何广告！<br><br></td></tr>"
    Else
        totalPut = rsAD.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                rsAD.Move (CurrentPage - 1) * MaxPerPage
            Else
                CurrentPage = 1
            End If
        End If

        Dim ADNum
        ADNum = 0
        Do While Not rsAD.EOF
            Response.Write "        <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
            Response.Write "          <td align='center'><input name='ADID' type='checkbox' value='" & rsAD("ADID") & "'></td>"
            Response.Write "          <td align='center'>" & rsAD("ADID") & "</td>"
            Response.Write "          <td align='center'><a href='Admin_Advertisement.asp?Action=PreviewAD&ADID=" & rsAD("ADID") & "'>预览</a></td>"
            Response.Write "          <td><a href='Admin_Advertisement.asp?Action=ModifyAD&ADID=" & rsAD("ADID") & "'>" & rsAD("ADName") & "</a></td>"
            Response.Write "          <td align='center'>" & GetADType(rsAD("ADType")) & "</td>"
            Response.Write "          <td align='center'>" & rsAD("Priority") & "</td>"
            Response.Write "          <td width='40' align='center'>"
            If rsAD("Passed") = True Then
                Response.Write "<b>√</b>"
            Else
                Response.Write "<font color=red><b>×</b></font>"
            End If
            Response.Write "          </td>"
            Response.Write "          <td width='150' align='center'>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=ModifyAD&ADID=" & rsAD("ADID") & "'>修改</a>"
            Response.Write "            <a href='Admin_Advertisement.asp?Action=DelAD&ADID=" & rsAD("ADID") & "' onClick=""return confirm('确定要删除此广告吗？');"">删除</a>"
            If rsAD("Passed") = False Then
                Response.Write "            <a href='Admin_Advertisement.asp?Action=SetADPassed&ADID=" & rsAD("ADID") & "'>通过审核</a>"
            Else
                Response.Write "            <a href='Admin_Advertisement.asp?Action=CancelADPassed&ADID=" & rsAD("ADID") & "'>取消通过</a>"
            End If
            Response.Write "          </td>"
            Response.Write "        </tr>"

            ADNum = ADNum + 1
            If ADNum >= MaxPerPage Then Exit Do
            rsAD.MoveNext
        Loop
    End If
    rsAD.Close
    Set rsAD = Nothing
    Response.Write "      </table>"
    Response.Write "      <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "        <tr>"
    Response.Write "          <td width='130' height='30'>"
    Response.Write "            <input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中所有的广告"
    Response.Write "          </td>"
    Response.Write "          <td>"
    Response.Write "            <input type='submit' value='删除选定广告' name='submit' onClick=""document.myform.Action.value='DelAD'"">&nbsp;"
    Response.Write "            <input type='submit' value='审核通过选定广告' name='submit' onClick=""document.myform.Action.value='SetADPassed'"">&nbsp;"
    Response.Write "            <input type='submit' value='取消审核选定广告' name='submit' onClick=""document.myform.Action.value='CancelADPassed'"">&nbsp;"
    Response.Write "            <input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "          </td>"
    Response.Write "        </tr>"
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </form>"
    Response.Write "  </tr>"
    Response.Write "</table>"

    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "个广告", True)
    End If
End Sub


Sub AddAD()
    Call ShowJS_AD
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Advertisement.asp' target='_self'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>添 加 广 告</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr align='center'>"
    Response.Write "      <td class='tdbg' valign='top' width='255'>"
    Response.Write "        <table width='100%' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "          <tr>"
    Response.Write "            <td align='center'><b>所属版位</b></td>"
    Response.Write "          </tr>"
    Response.Write "          <tr>"
    Response.Write "            <td>"
    Response.Write "              <select name='ZoneID' size='2' multiple style='height:360px;width:250px;'>"
    Response.Write GetZone_Option(ZoneID)
    Response.Write "              </select>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "      <td valign='top'>"
    Response.Write "        <table width='100%' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告名称：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='ADName' type='text' id='ADName' size='58' maxlength='255' value=''>"
    Response.Write "              <font color='red'>*</font>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告类型：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              " & GetADType_Option(1)
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告内容：</strong></td>"
    Response.Write "            <td height='265' valign='top'>"
    Response.Write "              <table id='ADContent_1' width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#ffffff' style='display:'>"
    Response.Write "                <tr align='center' class='tdbg2'>"
    Response.Write "                  <td colspan='2'><strong>广告内容设置--图片</strong></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='ImgUrl' type='text' id='ImgUrl' size='58' maxlength='255' value=''>"
    Response.Write "                    <font color='red'>*</font>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片上传：</td>"
    Response.Write "                  <td> <iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=AdPic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片尺寸：</td>"
    Response.Write "                  <td>"
    Response.Write "                    宽：<input name='ImgWidth' type='text' id='ImgWidth' size='6' maxlength='5' value=''>"
    Response.Write "                    像素&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "                    高：<input name='ImgHeight' type='text' id='ImgHeight' size='6' maxlength='5' value=''>"
    Response.Write "                    像素"
    Response.Write "                  <font color='red'>*</font></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkUrl' type='text' id='LinkUrl' value='http://' size='58' maxlength='255'>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接提示：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkAlt' type='text' id='LinkAlt' value='' size='58' maxlength='255'>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接目标：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkTarget' type='radio' id='LinkTarget' value='1' checked>新窗口"
    Response.Write "                    <input name='LinkTarget' type='radio' id='LinkTarget' value='0'>原窗口"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>广告简介：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <textarea name='ADIntro' cols='48' rows='4' id='ADIntro'></textarea>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "              </table>"
    Response.Write "              <table id='ADContent_2' width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#ffffff' style='display:none'>"
    Response.Write "                <tr align='center' class='tdbg2'>"
    Response.Write "                  <td colspan='2'><strong>广告内容设置--动画</strong></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='FlashUrl' type='text' id='FlashUrl' size='58' maxlength='255' value=''>"
    Response.Write "                    <font color='red'>*</font>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画上传：</td>"
    Response.Write "                  <td> <iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=AdPic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画尺寸：</td>"
    Response.Write "                  <td>"
    Response.Write "                    宽：<input name='FlashWidth' type='text' id='FlashWidth' size='6' maxlength='5' value=''>"
    Response.Write "                    像素&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "                    高：<input name='FlashHeight' type='text' id='FlashHeight' size='6' maxlength='5' value=''>"
    Response.Write "                    像素"
    Response.Write "                  <font color='red'>*</font></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>背景透明：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input type='radio' name='FlashWmode' value='0' checked> 不透明&nbsp;&nbsp;"
    Response.Write "                    <input type='radio' name='FlashWmode' value='1'> 透明&nbsp;&nbsp;"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "              </table>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告权重：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='Priority' type='text' id='Priority' size='4' maxlength='3' value='1'> <font color='red'>*</font> 此项为版位广告随机显示时的优先权，权重越大显示机会越大。"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>审核状态：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='Passed' type='checkbox' id='Passed' value='yes' checked> 通过审核"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1'>"
    Response.Write "    <tr>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveAddAD'>"
    Response.Write "        <input type='submit' name='Submit' value=' 添 加 '>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
End Sub


Sub ModifyAD()
    Dim ADID, rsAD, sqlAD
    ADID = GetUrl("ADID")
    If ADID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的广告ID</li>"
        Exit Sub
    Else
        ADID = PE_CLng(ADID)
    End If
    sqlAD = "select * from PW_Advertisement where ADID=" & ADID
    Set rsAD = Conn.Execute(sqlAD)
    If rsAD.BOF And rsAD.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的广告</li>"
        rsAD.Close
        Set rsAD = Nothing
        Exit Sub
    End If
    Dim ADType
    Dim ImgUrl, ImgWidth, ImgHeight, LinkUrl, LinkAlt, LinkTarget, ADIntro
    Dim FlashUrl, FlashWidth, FlashHeight, FlashWmode
    Dim ADText, ADCode, strDisabled

    ADType = rsAD("ADType")
    Select Case ADType
    Case 1
        ImgUrl = rsAD("ImgUrl")
        ImgWidth = rsAD("ImgWidth")
        ImgHeight = rsAD("ImgHeight")
        LinkUrl = rsAD("LinkUrl")
        LinkAlt = rsAD("LinkAlt")
        LinkTarget = rsAD("LinkTarget")
        ADIntro = rsAD("ADIntro")
    Case 2
        FlashUrl = rsAD("ImgUrl")
        FlashWidth = rsAD("ImgWidth")
        FlashHeight = rsAD("ImgHeight")
        FlashWmode = rsAD("FlashWmode")
    Case 3
        ADText = rsAD("ADIntro")
    Case 4
        ADCode = rsAD("ADIntro")
    End Select
    If ADType <> 1 Then strDisabled = " disabled"

    Call ShowJS_AD
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Advertisement.asp' target='_self'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "    <tr class='title'>"
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修 改 广 告</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr align='center'>"
    Response.Write "      <td class='tdbg' valign='top' width='255'>"
    Response.Write "        <table width='100%' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "          <tr>"
    Response.Write "            <td align='center'><b>所属版位</b></td>"
    Response.Write "          </tr>"
    Response.Write "          <tr>"
    Response.Write "            <td>"
    Response.Write "              <select name='ZoneID' size='2' multiple style='height:360px;width:250px;'>"
    Response.Write GetZone_Option(rsAD("ZoneID"))
    Response.Write "              </select>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "      <td valign='top'>"
    Response.Write "        <table width='100%' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告名称：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='ADName' type='text' id='ADName' size='58' maxlength='255' value='" & rsAD("ADName") & "'>"
    Response.Write "              <font color='red'>*</font>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告类型：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              " & GetADType_Option(rsAD("ADType"))
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告内容：</strong></td>"
    Response.Write "            <td height='265' valign='top'>"
    Response.Write "              <table id='ADContent_1' width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#ffffff' style='display:" & StyleDisplay(rsAD("ADType"), 1) & "'>"
    Response.Write "                <tr align='center' class='tdbg2'>"
    Response.Write "                  <td colspan='2'><strong>广告内容设置--图片</strong></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='ImgUrl' type='text' id='ImgUrl' size='58' maxlength='255' value='" & ImgUrl & "'>"
    Response.Write "                    <font color='red'>*</font>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片上传：</td>"
    Response.Write "                  <td> <iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=AdPic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>图片尺寸：</td>"
    Response.Write "                  <td>"
    Response.Write "                    宽：<input name='ImgWidth' type='text' id='ImgWidth' size='6' maxlength='5' value='" & ImgWidth & "'>"
    Response.Write "                    像素&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "                    高：<input name='ImgHeight' type='text' id='ImgHeight' size='6' maxlength='5' value='" & ImgHeight & "'>"
    Response.Write "                    像素"
    Response.Write "                  <font color='red'>*</font></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkUrl' type='text' id='LinkUrl' value='" & LinkUrl & "' size='58' maxlength='255'>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接提示：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkAlt' type='text' id='LinkAlt' value='" & LinkAlt & "' size='58' maxlength='255'>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>链接目标：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='LinkTarget' type='radio' id='LinkTarget' value='1' " & IsRadioChecked(LinkTarget, 1) & ">新窗口"
    Response.Write "                    <input name='LinkTarget' type='radio' id='LinkTarget' value='0' " & IsRadioChecked(LinkTarget, 0) & ">原窗口"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>广告简介：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <textarea name='ADIntro' cols='48' rows='4' id='ADIntro'>" & PE_ConvertBR(ADIntro) & "</textarea>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "              </table>"
    Response.Write "              <table id='ADContent_2' width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#ffffff' style='display:" & StyleDisplay(rsAD("ADType"), 2) & "'>"
    Response.Write "                <tr align='center' class='tdbg2'>"
    Response.Write "                  <td colspan='2'><strong>广告内容设置--动画</strong></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画地址：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input name='FlashUrl' type='text' id='FlashUrl' size='58' maxlength='255' value='" & FlashUrl & "'>"
    Response.Write "                    <font color='red'>*</font>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画上传：</td>"
    Response.Write "                  <td> <iframe style='top:2px' ID='uploadPhoto' src='Upload.asp?dialogtype=AdPic' frameborder=0 scrolling=no width='360' height='25'></iframe>"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>动画尺寸：</td>"
    Response.Write "                  <td>"
    Response.Write "                    宽：<input name='FlashWidth' type='text' id='FlashWidth' size='6' maxlength='5' value='" & FlashWidth & "'>"
    Response.Write "                    像素&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "                    高：<input name='FlashHeight' type='text' id='FlashHeight' size='6' maxlength='5' value='" & FlashHeight & "'>"
    Response.Write "                    像素"
    Response.Write "                  <font color='red'>*</font></td>"
    Response.Write "                </tr>"
    Response.Write "                <tr class='tdbg'>"
    Response.Write "                  <td width='80' align='right'>背景透明：</td>"
    Response.Write "                  <td>"
    Response.Write "                    <input type='radio' name='FlashWmode' value='0' " & IsRadioChecked(FlashWmode, 0) & "> 不透明&nbsp;&nbsp;"
    Response.Write "                    <input type='radio' name='FlashWmode' value='1' " & IsRadioChecked(FlashWmode, 1) & "> 透明&nbsp;&nbsp;"
    Response.Write "                  </td>"
    Response.Write "                </tr>"
    Response.Write "              </table>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>广告权重：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='Priority' type='text' id='Priority' size='4' maxlength='3' value='" & rsAD("Priority") & "'> <font color='red'>*</font> 此项为版位广告随机显示时的优先权，权重越大显示机会越大。"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "          <tr class='tdbg'>"
    Response.Write "            <td width='70' align='right'><strong>审核状态：</strong></td>"
    Response.Write "            <td>"
    Response.Write "              <input name='Passed' type='checkbox' id='Passed' value='yes' " & IsRadioChecked(rsAD("Passed"), True) & "> 通过审核"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1'>"
    Response.Write "    <tr>"
    Response.Write "      <td height='40' colspan='2' align='center'>"
    Response.Write "        <input name='ADID' type='hidden' id='ADID' value='" & rsAD("ADID") & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModifyAD'>"
    Response.Write "        <input type='submit' name='Submit' value=' 修 改 '>&nbsp;&nbsp;"
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Advertisement.asp?Action=ADList'"" style='cursor:hand;'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"
    rsAD.Close
    Set rsAD = Nothing
End Sub


Sub SaveAD()
    Dim ADID, ADType, ADName, ZoneID, Priority, Passed
    Dim ImgUrl, ImgWidth, ImgHeight, FlashWmode, LinkUrl, LinkAlt, LinkTarget, ADIntro
    Dim rsAD, sqlAD, OldZoneID

    ADID = PE_CLng(GetForm("ADID"))
    ADType = PE_CLng(GetForm("ADType"))
    ADName = GetForm("ADName")
    ZoneID = GetForm("ZoneID")
    Priority = PE_CLng(GetForm("Priority"))
    Passed = GetForm("Passed")

    ImgUrl = ""
    ImgWidth = 0
    ImgHeight = 0
    FlashWmode = 0
    LinkUrl = ""
    LinkAlt = ""
    LinkTarget = 1
    ADIntro = ""

    Select Case ADType
    Case 1
        ImgUrl = GetForm("ImgUrl")
        ImgWidth = PE_CLng(GetForm("ImgWidth"))
        ImgHeight = PE_CLng(GetForm("ImgHeight"))
        LinkUrl = GetForm("LinkUrl")
        If LinkUrl = "http://" Then LinkUrl = ""
        LinkAlt = GetForm("LinkAlt")
        LinkTarget = PE_CLng(GetForm("LinkTarget"))
        ADIntro = GetForm("ADIntro")
    Case 2
        ImgUrl = GetForm("FlashUrl")
        ImgWidth = PE_CLng(GetForm("FlashWidth"))
        ImgHeight = PE_CLng(GetForm("FlashHeight"))
        FlashWmode = PE_CLng(GetForm("FlashWmode"))
    End Select

    If ADName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>广告名称不能为空！</li>"
    End If
    If ZoneID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择所属版位！</li>"
    End If
    If Priority = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>广告权重必须填写！</li>"
    End If
    Select Case ADType
    Case 1
        If ImgUrl = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>图片地址不能为空！</li>"
        End If
    Case 2
        If ImgUrl = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>动画地址不能为空！</li>"
        End If
    End Select
	If ImgWidth <=0 Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>广告宽度必须大于0！</li>"
	End If
	If ImgHeight <=0 Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>广告高度必须大于0！</li>"
	End If
    If FoundErr = True Then
        Exit Sub
    End If
    ADName = PE_HTMLEncode(ADName)
    ImgUrl = PE_HTMLEncode(ImgUrl)
    LinkUrl = PE_HTMLEncode(LinkUrl)
    LinkAlt = PE_HTMLEncode(LinkAlt)
    ADIntro = PE_HTMLEncode(ADIntro)
    If IsValidID(ZoneID) = False Then
        ZoneID = ""
    Else
        ZoneID = ReplaceBadChar(ZoneID)
    End If
    Passed = CBool(Passed = "yes")
    Set rsAD = Server.CreateObject("adodb.recordset")
    If Action = "SaveAddAD" Then
        sqlAD = "select top 1 * from PW_Advertisement"
        rsAD.Open sqlAD, Conn, 1, 3
        rsAD.addnew
    ElseIf Action = "SaveModifyAD" Then
        If ADID = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定广告ID的值</li>"
            Exit Sub
        End If
        sqlAD = "select * from PW_Advertisement where ADID=" & ADID
        rsAD.Open sqlAD, Conn, 1, 3
        If rsAD.BOF And rsAD.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的广告！</li>"
            rsAD.Close
            Set rsAD = Nothing
            Exit Sub
        End If
        OldZoneID = rsAD("ZoneID")
    End If

    rsAD("ADType") = ADType
    rsAD("ADName") = ADName
    rsAD("ZoneID") = ZoneID
    rsAD("ImgUrl") = ImgUrl
    rsAD("ImgWidth") = ImgWidth
    rsAD("ImgHeight") = ImgHeight
    rsAD("FlashWmode") = FlashWmode
    rsAD("LinkUrl") = LinkUrl
    rsAD("LinkAlt") = LinkAlt
    rsAD("LinkTarget") = LinkTarget
    rsAD("ADIntro") = ADIntro
    rsAD("Priority") = Priority
    rsAD("Passed") = Passed
    rsAD.Update
	if Action = "SaveAddAD" Then
		ADID=rsAD("ADID")
        Call AddADID_Zone(ZoneID, ADID)
	end if
    rsAD.Close
    Set rsAD = Nothing

    If ZoneID <> OldZoneID Then
        Call DelADID_Zone(OldZoneID, ADID)
        Call AddADID_Zone(ZoneID, ADID)
        Call CreateJSZoneID(OldZoneID)
    End If
    Call CreateJSZoneID(ZoneID)
    Call CloseConn
    Response.Redirect "Admin_Advertisement.asp?Action=ADList"
End Sub


Sub SetADProperty()
    Dim ADID, sqlProperty, rsProperty
    Dim MoveChannelID
    ADID = GetValue("ADID")
    If IsValidID(ADID) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定广告ID</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If

    If InStr(ADID, ",") > 0 Then
        sqlProperty = "select * from PW_Advertisement where ADID in (" & ADID & ")"
    Else
        sqlProperty = "select * from PW_Advertisement where ADID=" & ADID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
        Select Case Action
        Case "SetADPassed"
            rsProperty("Passed") = True
        Case "CancelADPassed"
            rsProperty("Passed") = False
        Case "DelAD"
            Call DelADID_Zone(rsProperty("ZoneID"), rsProperty("ADID"))
			Dim Ad_file
			Ad_file=rsProperty("ImgUrl")
            If fso.FileExists(Server.MapPath(Ad_file)) Then
                fso.DeleteFile Server.MapPath(Ad_file)
            End If
            rsProperty.Delete
        End Select
        rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing

    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub PreviewAD()
    Dim ADID, sqlAD, rsAD
    ADID = GetUrl("ADID")
    If ADID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数丢失！</li>"
        Exit Sub
    Else
        ADID = PE_CLng(ADID)
    End If
    sqlAD = "select * from PW_Advertisement where ADID=" & ADID
    Set rsAD = Conn.Execute(sqlAD)
    If rsAD.BOF And rsAD.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的广告！</li>"
        rsAD.Close
        Set rsAD = Nothing
        Exit Sub
    End If

    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='2' align='center'><strong>预览广告----" & rsAD("ADName") & "</strong></td>"
    Response.Write "  </tr>"
    Response.Write "  <tr class='tdbg2'>"
    Response.Write "    <td height='25' align='center'>"
    Response.Write "      <a href='javascript:this.location.reload();'>刷新页面</a>&nbsp;&nbsp;&nbsp;&nbsp;"
    Response.Write "      <a href='" & ComeUrl & "'>返回上页</a>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "  <tr valign='top'>"
    Response.Write "    <td height='300'>" & GetADContent(rsAD("ADID")) & "</td>"
    Response.Write "  </tr>"
    Response.Write "</table>"

    rsAD.Close
    Set rsAD = Nothing
End Sub


'******************************************************************************************
'以下为广告管理中用到的函数，获取相关的管理路径、参数设置等信息。
'******************************************************************************************
Sub AddADID_Zone(arrZoneID, iADID)
    If iADID = "" Or IsNull(iADID) Then
        Exit Sub
    Else
        iADID = PE_CLng(iADID)
    End If
    If IsValidID(arrZoneID) = True Then
        Dim sqlZone, rsZone
        arrZoneID = ReplaceBadChar(arrZoneID)
        sqlZone = "select IncludeADID from PW_AdZone where ZoneID in (" & arrZoneID & ")"
        Set rsZone = Server.CreateObject("Adodb.RecordSet")
        rsZone.Open sqlZone, Conn, 1, 3
        Do While Not rsZone.EOF
            rsZone(0) = AppendStr(rsZone(0), CStr(iADID), ",")
            rsZone.Update
            rsZone.MoveNext
        Loop
        rsZone.Close
        Set rsZone = Nothing
    End If
End Sub


Sub DelADID_Zone(arrZoneID, iADID)
    If iADID = "" Or IsNull(iADID) Then
        Exit Sub
    Else
        iADID = PE_CLng(iADID)
    End If
    If IsValidID(arrZoneID) = True Then
        Dim sqlZone, rsZone
        sqlZone = "select IncludeADID from PW_AdZone where ZoneID in (" & arrZoneID & ")"
        Set rsZone = Server.CreateObject("Adodb.RecordSet")
        rsZone.Open sqlZone, Conn, 1, 3
        Do While Not rsZone.EOF
            rsZone(0) = RemoveStr(rsZone(0), CStr(iADID), ",")
            rsZone.Update
            rsZone.MoveNext
        Loop
        rsZone.Close
        Set rsZone = Nothing
    End If
End Sub


Function GetZone_Option(arrZoneID)
    Dim strTemp, rsZone, sqlZone
    sqlZone = "select ZoneID,ZoneName,ZoneWidth,ZoneHeight from PW_AdZone order by ZoneID desc"
    Set rsZone = Conn.Execute(sqlZone)
    Do While Not rsZone.EOF
        strTemp = strTemp & "<option value='" & rsZone("ZoneID") & "'"
        If FoundInArr(arrZoneID, rsZone("ZoneID"), ",") = True Then strTemp = strTemp & " selected"
        strTemp = strTemp & ">" & rsZone("ZoneName") & "【"& rsZone("ZoneWidth") &"*"& rsZone("ZoneHeight") &"】</option>"
        rsZone.MoveNext
    Loop
    rsZone.Close
    Set rsZone = Nothing
    GetZone_Option = strTemp
End Function


Function GetADManagePath()
    Dim strPath, sqlPath, rsPath
    strPath = "您现在的位置：网站广告管理&nbsp;&gt;&gt;&nbsp;"
    If ZoneID <> "" Then
        Dim nZone
        Set nZone = Conn.Execute("select ZoneName from PW_AdZone where ZoneID=" & PE_CLng(ZoneID))
        If Not nZone.BOF And Not nZone.EOF Then
            strPath = strPath & nZone("ZoneName") & "&nbsp;&gt;&gt;&nbsp;"
        End If
    End If
    strPath = strPath & "所有广告"
    GetADManagePath = strPath
End Function


Function GetADType(iADType)
    If iADType = 1 Then
        GetADType = "图片"
    ElseIf iADType = 2 Then
        GetADType = "动画"
    End If
End Function


Function GetADType_Option(iADType)
    Dim strADType
    strADType = strADType & "<input type='radio' name='ADType' value='1' onclick='Change_ADType();'"
    If iADType = 1 Then strADType = strADType & " checked"
    strADType = strADType & "> 图片&nbsp;&nbsp;"
    strADType = strADType & "<input type='radio' name='ADType' value='2' onclick='Change_ADType();'"
    If iADType = 2 Then strADType = strADType & " checked"
    strADType = strADType & "> 动画&nbsp;&nbsp;"
    GetADType_Option = strADType
End Function


Function GetADContent(ADID)
    If IsNull(ADID) Or ADID = "" Then
        Exit Function
    Else
        ADID = PE_CLng(ADID)
    End If

    Dim rsAD, sqlAD, strAD, strTemp
    sqlAD = "select * from PW_Advertisement where ADID=" & ADID
    Set rsAD = Server.CreateObject("ADODB.Recordset")
    rsAD.Open sqlAD, Conn, 1, 1
    If Not (rsAD.BOF And rsAD.EOF) Then
        Select Case rsAD("ADType")
        Case 1
            strTemp = strTemp & "<img src='" & rsAD("ImgUrl") & "'"
            If rsAD("ImgWidth") > 0 Then strTemp = strTemp & " width='" & rsAD("ImgWidth") & "'"
            If rsAD("ImgHeight") > 0 Then strTemp = strTemp & " height='" & rsAD("ImgHeight") & "'"
            strTemp = strTemp & " border='0'>"
            If IsNull(rsAD("LinkUrl")) Or rsAD("LinkUrl") = "" Then
                strAD = strTemp
            Else
                strAD = "<a href='" & rsAD("LinkUrl") & ""
                If rsAD("LinkTarget") = 0 Then
                    strAD = strAD & " target='_self'"
                Else
                    strAD = strAD & " target='_blank'"
                End If
                If rsAD("LinkAlt") <> "" Then strAD = strAD & " title='" & rsAD("LinkAlt") & "'"
                strAD = strAD & ">" & strTemp & "</a>"
            End If
        Case 2
            strAD = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'"
            If rsAD("ImgWidth") > 0 Then strAD = strAD & " width='" & rsAD("ImgWidth") & "'"
            If rsAD("ImgHeight") > 0 Then strAD = strAD & " height='" & rsAD("ImgHeight") & "'"
            strAD = strAD & "><param name='movie' value='" & rsAD("ImgUrl") & "'>"
            If rsAD("FlashWmode") = 1 Then strAD = strAD & "<param name='wmode' value='transparent'>"
            strAD = strAD & "<param name='quality' value='autohigh'>"
            strAD = strAD & "<embed src='" & rsAD("ImgUrl") & "' quality='autohigh'"
            strAD = strAD & " pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash'"
            If rsAD("FlashWmode") = 1 Then strAD = strAD & " wmode='transparent'"
            If rsAD("ImgWidth") > 0 Then strAD = strAD & " width='" & rsAD("ImgWidth") & "'"
            If rsAD("ImgHeight") > 0 Then strAD = strAD & " height='" & rsAD("ImgHeight") & "'"
            strAD = strAD & "></embed></object>"
        End Select
    End If
    rsAD.Close
    Set rsAD = Nothing
    GetADContent = strAD
End Function

%>