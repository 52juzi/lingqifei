<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<!--#include file="../PW_Editor/fckeditor.asp"-->
<!--#include file="../Include/PW_XmlHttp.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 1   '0--不检查，1 检查
Const PurviewLevel_Others = ""   '其他权限

Dim ManageType, AddType
Dim ClassID, OnTop, IsElite, IsHot
Dim tClass, ClassName, RootID, ParentID, Depth, ParentPath, Child, arrChildID

Dim SoftID


ManageType = GetValue("ManageType")
OnTop = GetValue("OnTop")
IsElite = GetValue("IsElite")
IsHot = GetValue("IsHot")
ClassID = PE_CLng(GetValue("ClassID"))
SoftID = GetValue("SoftID")
AddType = GetValue("AddType")

If Action = "" Then
    Action = "Manage"
End If


If IsValidID(SoftID) = False Then
    SoftID = ""
End If
If AddType = "" Then
    AddType = 1
Else
    AddType = PE_CLng(AddType)
End If
FileName = "Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=" & Action & "&ManageType=" & ManageType
strFileName = FileName & "&ClassID=" & ClassID & "&Field=" & strField & "&keyword=" & Keyword

Response.Write "<html><head><title>" & ChannelShortName & "管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf

Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
Dim strTitle
strTitle = ChannelName & "管理----"
Select Case Action
Case "Add"
    strTitle = strTitle & "添加" & ChannelShortName
Case "Modify"
    strTitle = strTitle & "修改" & ChannelShortName
Case "SaveAdd", "SaveModify"
    strTitle = strTitle & "保存" & ChannelShortName
Case "Show"
    strTitle = strTitle & "预览" & ChannelShortName
Case "Manage"
    Select Case ManageType
    Case "Recyclebin"
        strTitle = strTitle & ChannelShortName & "回收站管理"
    Case Else
        strTitle = strTitle & ChannelShortName & "管理首页"
    End Select
End Select

Call ShowPageTitle(strTitle)

Response.Write "  <tr class='tdbg'>"
Response.Write "    <td width='70' height='30' ><strong>管理导航：</strong></td><td colspan='5'>"
if Purview_View=True then
	Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Status=9'>" & ChannelShortName & "管理首页</a>"
end if
if Purview_Add=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Add&AddType=1&ClassID=" & ClassID & "'>添加" & ChannelShortName & "</a>"
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Add&AddType=3&ClassID=" & ClassID & "'>添加" & ChannelShortName & "（镜像）</a>"
end if
if Purview_Del = True then
    Response.Write "&nbsp;|&nbsp;<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&ManageType=Recyclebin'>" & ChannelShortName & "回收站管理</a>"
end if
if FoundInArr(FieldShow, "ClassID", ",") and Purview_Class=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "分类管理</a>"
end if
Response.Write "</td></tr>" & vbCrLf
If Action = "Manage" Then
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<form name='form3' method='Post' action='" & strFileName & "'><tr class='tdbg'>"
		Response.Write "  <td width='70' height='30' ><strong>" & ChannelShortName & "选项：</strong></td>"
		Response.Write "<td>"
		Response.Write "<input name='OnTop' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(OnTop, "True") & "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsElite' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsElite, "True") & "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "<input name='IsHot' type='checkbox' onclick='submit()' value=""True"" " & IsRadioChecked(IsHot, "True") & "> 热门" & ChannelShortName
		Response.Write "</td></tr></form>" & vbCrLf
    End If
End If
Response.Write "</table>" & vbCrLf

strFileName = strFileName & "&OnTop=" & OnTop & "&IsElite=" & IsElite & "&IsHot=" & IsHot



Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveSoft
Case "SetOnTop", "CancelOnTop", "SetElite", "CancelElite"
    Call SetProperty
Case "Show"
    Call Show
Case "Del"
    Call Del
Case "ConfirmDel"
    Call ConfirmDel
Case "ClearRecyclebin"
    Call ClearRecyclebin
Case "Restore"
    Call Restore
Case "RestoreAll"
    Call RestoreAll
Case "Manage"
    Call main
End Select


If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn



Sub main()
    Dim rsSoftList, sql, Querysql
	If ClassID > 0 Then
        Set tClass = Conn.Execute("select ClassName,RootID,ParentID,Depth,ParentPath,Child,arrChildID from PW_Class where ClassID=" & ClassID)
        If tClass.BOF And tClass.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目</li>"
        Else
            ClassName = tClass(0)
            RootID = tClass(1)
            ParentID = tClass(2)
            Depth = tClass(3)
            ParentPath = tClass(4)
            Child = tClass(5)
            arrChildID = tClass(6)
        End If
        Set tClass = Nothing
    End If
    If FoundErr = True Then Exit Sub
	
    Call ShowJS_Main(ChannelShortName)
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
		Response.Write "  <tr class='title'>"
		Response.Write "    <td height='22'>" & GetRootClass() & "</td>"
		Response.Write "  </tr>" & GetChild_Root() & ""
		Response.Write "</table>"
	end if
	Response.write "<br>"
    Select Case ManageType
    Case "Recyclebin"
        Call ShowContentManagePath(ChannelShortName & "回收站管理")
    Case Else
        Call ShowContentManagePath(ChannelShortName & "管理")
    End Select
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "    <form name='myform' method='Post' action='Admin_Soft.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "     <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "          <tr class='title' height='22'> "
    Response.Write "            <td height='22' width='30' align='center'><strong>选中</strong></td>"
    Response.Write "            <td width='25' align='center'><strong>ID</strong></td>"
    Response.Write "            <td align='center' ><strong>" & ChannelShortName & "名称</strong></td>"
	if FoundInArr(FieldShow, "Hits", ",") then
		Response.Write "            <td width='40' align='center' ><strong>下载数</strong></td>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "            <td width='80' align='center' ><strong>" & ChannelShortName & "属性</strong></td>"
	end if
	If ManageType = "Recyclebin" Then
		Response.Write "            <td width='100' align='center' ><strong>回收站操作</strong></td>"
    Else
        Response.Write "            <td width='180' align='center' ><strong>常规管理操作</strong></td>"
    End If
    Response.Write "          </tr>"
	If ClassID = -1 Or (ClassID > 0 And Child = 0) Then
		sql = "select top " & MaxPerPage & " S.ClassID,S.SoftID,S.SoftName,S.SoftVersion,S.Keyword,S.Author,S.UpdateTime,"
		sql = sql & "S.SoftPicUrl,S.DownloadUrl,S.SoftSize,S.Hits,S.OnTop,S.Elite"
		sql = sql & "  from PW_Soft S "
	Else
		sql = "select top " & MaxPerPage & " S.ClassID,S.SoftID,C.ClassName,S.SoftName,S.SoftVersion,S.Keyword,S.Author,S.UpdateTime,"
		sql = sql & "S.SoftPicUrl,S.DownloadUrl,S.SoftSize,S.Hits,S.OnTop,S.Elite"
		sql = sql & " from PW_Soft S left join PW_Class C on S.ClassID=C.ClassID "
	End If
    Querysql = " where S.ChannelID=" & ChannelID
    If ManageType = "Recyclebin" Then
        Querysql = Querysql & " and S.Deleted=" & PE_True & ""
    Else
        Querysql = Querysql & " and S.Deleted=" & PE_False & ""
    End If
	If OnTop = "True" Then
		Querysql = Querysql & " and S.OnTop=" & PE_True & ""
	End If
	If IsElite = "True" Then
		Querysql = Querysql & " and S.Elite=" & PE_True & ""
	End If
	If IsHot = "True" Then
		Querysql = Querysql & " and S.Hits>=" & HitsOfHot & ""
	End If
    If ClassID <> 0 Then
        If Child > 0 Then
            Querysql = Querysql & " and S.ClassID in (" & arrChildID & ")"
        Else
            Querysql = Querysql & " and S.ClassID=" & ClassID
        End If
    End If
    If Keyword <> "" Then
    	Querysql = Querysql & " and S.SoftName like '%" & Keyword & "%' "
    End If
	totalPut = PE_CLng(Conn.Execute("select Count(*) from PW_Soft S " & Querysql)(0))
    If CurrentPage < 1 Then
        CurrentPage = 1
    End If
    If (CurrentPage - 1) * MaxPerPage > totalPut Then
        If (totalPut Mod MaxPerPage) = 0 Then
            CurrentPage = totalPut \ MaxPerPage
        Else
            CurrentPage = totalPut \ MaxPerPage + 1
        End If
    End If
    If CurrentPage > 1 Then
    	Querysql = Querysql & " and S.SoftID < (select min(SoftID) from (select top " & ((CurrentPage - 1) * MaxPerPage) & " S.SoftID from PW_Soft S " & Querysql & " order by S.SoftID desc) as QuerySoft)"
    End If
	sql = sql & Querysql & " order by S.SoftID desc"
    Set rsSoftList = Server.CreateObject("ADODB.Recordset")
    rsSoftList.Open sql, Conn, 1, 1
    If rsSoftList.BOF And rsSoftList.EOF Then
        totalPut = 0
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>"

        If ClassID > 0 Then
            Response.Write "此栏目及其子栏目中没有任何"
        Else
            Response.Write "没有任何"
        End If
		Response.Write ChannelShortName & "！"
        Response.Write "<br><br></td></tr>"
    Else
        Dim SoftNum
        SoftNum = 0
        Do While Not rsSoftList.EOF
            Response.Write "      <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
			Response.Write "        <td width='30' align='center'><input name='SoftID' type='checkbox' onclick='CheckItem(this)' id='SoftID' value='" & rsSoftList("SoftID") & "'></td>"
			Response.Write "        <td width='25' align='center'>" & rsSoftList("SoftID") & "</td>"
            Response.Write "        <td>"
			If rsSoftList("ClassID") <> ClassID And ClassID <> -1 Then
				Response.Write "<a href='" & FileName & "&ClassID=" & rsSoftList("ClassID") & "'>["
				If rsSoftList("ClassName") <> "" Then
					Response.Write rsSoftList("ClassName")
				Else
					Response.Write "<font color='gray'>不属于任何栏目</font>"
				End If
				Response.Write "]</a>&nbsp;"
			End If
            Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Show&SoftID=" & rsSoftList("SoftID") & "'"
			
			Response.Write " title='名&nbsp;&nbsp;&nbsp;&nbsp;称：" & rsSoftList("SoftName") & vbCrLf
			if FoundInArr(FieldShow, "SoftVersion", ",") then
				Response.write "版&nbsp;&nbsp;&nbsp;&nbsp;本：" & rsSoftList("SoftVersion") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Author", ",") then
				Response.write "作&nbsp;&nbsp;&nbsp;&nbsp;者：" & rsSoftList("Author") & vbCrLf
			end if
			if FoundInArr(FieldShow, "UpdateTime", ",") then
				Response.write "更新时间：" & rsSoftList("UpdateTime") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.write "下载次数：" & rsSoftList("Hits") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Keyword", ",") then
				Response.write "关 键 字：" & Mid(rsSoftList("Keyword"), 2, Len(rsSoftList("Keyword")) - 2)
			end if
            Response.Write "'>" & rsSoftList("SoftName")
            If rsSoftList("SoftVersion") <> "" Then
                Response.Write "&nbsp;&nbsp;" & rsSoftList("SoftVersion")
            End If
            Response.Write "</td>"
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.Write "            <td width='40' align='center' >" & rsSoftList("Hits") & "</td>"
			end if
			if FoundInArr(FieldShow, "Property", ",") then
				Response.Write "      <td align='center'>"
				If rsSoftList("OnTop") = True Then
					Response.Write "<font color=blue>顶</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsSoftList("Hits") >= HitsOfHot Then
					Response.Write "<font color=red>热</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If rsSoftList("Elite") = True Then
					Response.Write "<font color=green>荐</font> "
				Else
					Response.Write "&nbsp;&nbsp;&nbsp;"
				End If
				If Trim(rsSoftList("SoftPicUrl")) <> "" Then
					Response.Write "<font color=blue>图</font>"
				Else
					Response.Write "&nbsp;&nbsp;"
				End If
				Response.Write "    </td>"
			end if
            Select Case ManageType
            Case "Recyclebin"
				if Purview_Del then
					Response.Write "<td width='100' align='center'>"
					Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=ConfirmDel&SoftID=" & rsSoftList("SoftID") & "' onclick=""return confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！');"">彻底删除</a> "
					Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Restore&SoftID=" & rsSoftList("SoftID") & "'>还原</a>"
					Response.Write "</td>"
				end if
            Case Else
                Response.Write "    <td align='center'>&nbsp;"
                if Purview_Modify then
                    Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Modify&SoftID=" & rsSoftList("SoftID") & "'>修改</a>&nbsp;"
                End If
                if Purview_Del then
                    Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Del&SoftID=" & rsSoftList("SoftID") & "' onclick=""return confirm('确定要删除此" & ChannelShortName & "吗？删除后你还可以从回收站中还原。');"">删除</a>&nbsp;"
                End If
				if FoundInArr(FieldShow, "Property", ",") then
                    If rsSoftList("OnTop") = False Then
                        Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=SetOnTop&SoftID=" & rsSoftList("SoftID") & "'>固顶</a>&nbsp;"
                    Else
                        Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=CancelOnTop&SoftID=" & rsSoftList("SoftID") & "'>解固</a>&nbsp;"
                    End If
                    If rsSoftList("Elite") = False Then
                        Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=SetElite&SoftID=" & rsSoftList("SoftID") & "'>设为推荐</a>"
                    Else
                        Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=CancelElite&SoftID=" & rsSoftList("SoftID") & "'>取消推荐</a>"
                    End If
                End If
                Response.Write "</td>"
            End Select
            Response.Write "</tr>"

            SoftNum = SoftNum + 1
            If SoftNum >= MaxPerPage Then Exit Do
            rsSoftList.MoveNext
        Loop
    End If
    rsSoftList.Close
    Set rsSoftList = Nothing
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td width='200' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页显示的所有" & ChannelShortName & "</td><td>"
    Select Case ManageType
    Case "Recyclebin"
        if Purview_Del then
            Response.Write "<input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='ConfirmDel'"" value=' 彻底删除 '>&nbsp;"
            Response.Write "<input name='Submit2' type='submit' id='Submit2' onClick=""document.myform.Action.value='ClearRecyclebin'"" value='清空回收站'>&nbsp;&nbsp;&nbsp;&nbsp;"
            Response.Write "<input name='Submit3' type='submit' id='Submit3' onClick=""document.myform.Action.value='Restore'"" value='还原选定的" & ChannelShortName & "'>&nbsp;"
            Response.Write "<input name='Submit4' type='submit' id='Submit4' onClick=""document.myform.Action.value='RestoreAll'"" value='还原所有" & ChannelShortName & "'>"
        End If
    Case Else
        if Purview_Del then
            Response.Write "<input name='submit1' type='submit' value=' 批量删除 ' onClick=""document.myform.Action.value='Del'""> "
        End If
    End Select
    
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "  </td></tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName & "", True)
    End If
    Response.Write "<form method='Get' name='SearchForm' action='" & FileName & "'>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='border'>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "   <td width='80' align='right'><strong>" & ChannelShortName & "搜索：</strong></td>"
    Response.Write "   <td>"
    Response.Write "<select name='Field' size='1'>"
    Response.Write "<option value='SoftName' selected>" & ChannelShortName & "名称</option>"
    Response.Write "</select>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<select name='ClassID'><option value=''>所有栏目</option>" & GetClass_Option(1,ClassID) & "</select>"
    end if
    Response.Write "<input type='text' name='keyword'  size='20' value='关键字' maxlength='50' onFocus='this.select();'>"
    Response.Write "<input type='submit' name='Submit'  value='搜索'>"
    Response.Write "<input name='ManageType' type='hidden' id='ManageType' value='" & ManageType & "'>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "</td></tr></table></form>"
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<br><b>说明：</b><br>&nbsp;&nbsp;&nbsp;&nbsp;" & ChannelShortName & "属性中的各项含义：<font color=blue>顶</font>----固顶" & ChannelShortName & "，<font color=red>热</font>----热门" & ChannelShortName & "，<font color=green>荐</font>----推荐" & ChannelShortName & "，<font color=blue>图</font>----有缩略图<br>"
	end if
End Sub
			
			
Sub ShowJS_Soft()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function SelectSoft(){" & vbCrLf
    Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?ChannelID=" & ChannelID & "&dialogtype=Softpic', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
    Response.Write "  if(arr!=null){" & vbCrLf
    Response.Write "    var ss=arr.split('|');" & vbCrLf
    Response.Write "    document.myform.SoftPicUrl.value=ss[0];" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function SelectFile(){" & vbCrLf
    Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?ChannelID=" & ChannelID & "&dialogtype=Soft', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
    Response.Write "  if(arr!=null){" & vbCrLf
    Response.Write "    var ss=arr.split('|');" & vbCrLf
    Response.Write "    strSoftUrl=ss[0];" & vbCrLf
    Response.Write "    var url='下载地址'+(document.myform.DownloadUrl.length+1)+'|'+strSoftUrl;" & vbCrLf
    Response.Write "    document.myform.DownloadUrl.options[document.myform.DownloadUrl.length]=new Option(url,url);" & vbCrLf
    Response.Write "    document.myform.SoftSize.value=ss[1];" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
	if FoundInArr(FieldShow, "DownloadUrl", ",") then
		Response.Write "function AddUrl(){" & vbCrLf
		Response.Write "  var thisurl='下载地址'+(document.myform.DownloadUrl.length+1)+'|http://'; " & vbCrLf
		Response.Write "  var url=prompt('请输入下载地址名称和链接，中间用“|”隔开：',thisurl);" & vbCrLf
		Response.Write "  if(url!=null&&url!=''){document.myform.DownloadUrl.options[document.myform.DownloadUrl.length]=new Option(url,url);}" & vbCrLf
		Response.Write "}" & vbCrLf
		Response.Write "function ModifyUrl(){" & vbCrLf
		Response.Write "  if(document.myform.DownloadUrl.length==0) return false;" & vbCrLf
		Response.Write "  var thisurl=document.myform.DownloadUrl.value; " & vbCrLf
		Response.Write "  if (thisurl=='') {alert('请先选择一个下载地址，再点修改按钮！');return false;}" & vbCrLf
		Response.Write "  var url=prompt('请输入下载地址名称和链接，中间用“|”隔开：',thisurl);" & vbCrLf
		Response.Write "  if(url!=thisurl&&url!=null&&url!=''){document.myform.DownloadUrl.options[document.myform.DownloadUrl.selectedIndex]=new Option(url,url);}" & vbCrLf
		Response.Write "}" & vbCrLf
		Response.Write "function DelUrl(){" & vbCrLf
		Response.Write "  if(document.myform.DownloadUrl.length==0) return false;" & vbCrLf
		Response.Write "  var thisurl=document.myform.DownloadUrl.value; " & vbCrLf
		Response.Write "  if (thisurl=='') {alert('请先选择一个下载地址，再点删除按钮！');return false;}" & vbCrLf
		Response.Write "  document.myform.DownloadUrl.options[document.myform.DownloadUrl.selectedIndex]=null;" & vbCrLf
		Response.Write "}" & vbCrLf
	end if
    Response.Write "function CheckForm()" & vbCrLf
    Response.Write "{" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "  if (document.myform.SoftName.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "名称不能为空！');" & vbCrLf
		Response.Write "    document.myform.SoftName.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "  if (document.myform.Keyword.value==''){" & vbCrLf
		Response.Write "    alert('关键字不能为空！');" & vbCrLf
		Response.Write "    document.myform.Keyword.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Detial", ",") then
		Response.Write "  var oEditor  =FCKeditorAPI.GetInstance(""SoftDetial"");" & vbCrLf
		Response.Write "  var Content =oEditor.GetXHTML();" & vbCrLf
		Response.Write "  if (Content==null || Content==''){" & vbCrLf
		Response.Write "     alert('" & ChannelShortName & "内容不能为空！');" & vbCrLf
		Response.Write "     oEditor.Focus();" & vbCrLf
		Response.Write "     return(false);" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "  var obj=document.myform.ClassID;" & vbCrLf
		Response.Write "  var iCount=0;" & vbCrLf
		Response.Write "  for(var i=0;i<obj.length;i++){" & vbCrLf
		Response.Write "    if(obj.options[i].selected==true){" & vbCrLf
		Response.Write "      iCount=iCount+1;" & vbCrLf
		Response.Write "    }" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  if (iCount==0){" & vbCrLf
		Response.Write "    alert('请选择所属栏目！');" & vbCrLf
		Response.Write "    document.myform.ClassID.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	End if
	if FoundInArr(FieldShow, "DownloadUrl", ",") then
		Response.Write "  if(document.myform.DownloadUrl.length==0){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "下载地址不能为空！');" & vbCrLf
		Response.Write "    document.myform.DownloadUrl.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "    for(var i=0;i<document.myform.DownloadUrl.length;i++){" & vbCrLf
		Response.Write "      if (document.myform.DownloadUrls.value==''){ document.myform.DownloadUrls.value=document.myform.DownloadUrl.options[i].value;}" & vbCrLf
		Response.Write "      else {document.myform.DownloadUrls.value+='$$$'+document.myform.DownloadUrl.options[i].value;}" & vbCrLf
		Response.Write "    }" & vbCrLf
	end if
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("ShowOnForm") = True  And rsField("EnableNull") = False Then
			Response.Write "  if (document.myform."&rsField("FieldName")&".value==''){" & vbCrLf
			Response.Write "    alert('"&rsField("Title")&"不能为空！');" & vbCrLf
			Response.Write "    document.myform."&rsField("FieldName")&".focus();" & vbCrLf
			Response.Write "    return false;" & vbCrLf
			Response.Write "  }" & vbCrLf
        End If
        rsField.MoveNext
    Loop
	
    Response.Write "  return true;  " & vbCrLf
    Response.Write "}" & vbCrLf
   Response.Write "</script>" & vbCrLf
End Sub


Sub Add()
    Call ShowJS_Soft

    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;添加" & ChannelShortName & "</td></tr></table>" & vbCrLf
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Soft.asp' target='_self'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>" & vbCrLf
    Response.Write "  <tr align='center'>" & vbCrLf
    Response.Write "    <td class='tdbg' height='200' valign='top'>" & vbCrLf
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='SoftName' type='text' value='' autocomplete='off' size='50' maxlength='255'> <font color='#FF0000'>*</font><input type='button' name='checksame' value='检查是否存在相同的" & ChannelShortName & "名' onclick=""showModalDialog('Admin_CheckSameTitle.asp?ModuleType=" & ModuleType & "&Title='+document.myform.SoftName.value,'checksame','dialogWidth:350px; dialogHeight:250px; help: no; scroll: no; status: no');"">"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Trim(Session("Keyword")) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>作者/开发商：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & Trim(Session("Author")) & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & Trim(Session("CopyFrom")) & "' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "SoftPicUrl", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>缩略图：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='SoftPicUrl' type='text' id='SoftPicUrl' size='80' maxlength='200'>"
		Response.Write "              <input type='button' name='Button2' value='从已上传图片中选择' onclick='SelectSoft()'>"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'></td>" & vbCrLf
		Response.Write "            <td><table><tr><td>上传" & ChannelShortName & "图片：</td><td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Softpic' frameborder=0 scrolling=no width='450' height='25'></iframe></td></tr></table></td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	Call ShowTabs_MyField_Add()
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>" &vbcrlf
		Response.Write "            <td><textarea name='SoftIntro' cols='80' rows='4'></textarea></td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	End if
	if FoundInArr(FieldShow, "Detial", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "详细：</td>"
		Response.Write "            <td>"
		Call ShowFckEditor("Soft","","SoftDetial","100%","300")
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "DownloadUrl", ",") then
		Response.Write "          <tr class='tdbg' id='UrlType2'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>上传" & ChannelShortName & "：</td>"
		Response.Write "            <td>"

		Response.Write "              <iframe style='top:2px' ID='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Soft' frameborder=0 scrolling=no width='450' height='25'></iframe>"
		Response.Write "            </td>"
		Response.Write "          </tr>"	
		Response.Write "          <tr class='tdbg' id='UrlType2'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "地址：</td>"
		Response.Write "            <td>"
		Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
		Response.Write "                <tr>"
		Response.Write "                  <td width='450'>"
		Response.Write "                    <input type='hidden' name='DownloadUrls' value=''>"
		Response.Write "                    <select name='DownloadUrl' style='width:500px;height:100px' size='2' ondblclick='return ModifyUrl();'></select>"
		Response.Write "                  </td>"
		Response.Write "                  <td>"
		Response.Write "                    <input type='button' name='Softselect' value='从已上传" & ChannelShortName & "中选择' onclick='SelectFile()'><br><br>"
		Response.Write "                    <input type='button' name='addurl' value='添加外部地址' onclick='AddUrl();'><br>"
		Response.Write "                    <input type='button' name='modifyurl' value='修改当前地址' onclick='return ModifyUrl();'><br>"
		Response.Write "                    <input type='button' name='delurl' value='删除当前地址' onclick='DelUrl();'>"
		Response.Write "                  </td>"
		Response.Write "                </tr>"
		Response.Write "              <tr><td  colspan='3'>系统提供的上传功能只适合上传比较小的" & ChannelShortName & "（如word帮助文档）。如果" & ChannelShortName & "比较大（" & MaxFileSize \ 1024 & "M以上），请先使用FTP上传，而不要使用系统提供的上传功能，以免上传出错或过度占用服务器的CPU资源。FTP上传后请将地址复制到下面的地址框中。</td></tr>"		
		Response.Write "              </table>"
		Response.Write "            </td>"
		Response.Write "          </tr>"
	
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "大小：</td>"
		Response.Write "            <td><input name='SoftSize' type='text' id='SoftSize' size='10' maxlength='10'> K</strong></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftVersion", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "版本：</td>"
		Response.Write "            <td><input name='SoftVersion' type='text' size='15' maxlength='100'></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftType", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "类别：</td>"
		Response.Write "            <td><select name='SoftType' id='SoftType'>" & GetSoftType(0) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftLanguage", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "语言：</td>"
		Response.Write "            <td><select name='SoftLanguage' id='SoftLanguage'>" & GetSoftLanguage(0) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "CopyrightType", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>授权形式：</td>"
		Response.Write "            <td><select name='CopyrightType' id='CopyrightType'>" & GetCopyrightType(0) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "OperatingSystem", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "平台：</td>"
		Response.Write "            <td>"
		Response.Write "              <input name='OperatingSystem' type='text' value='Win9x/NT/2000/XP/' size='80' maxlength='200'> <br>" & GetOperatingSystemList
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "DemoUrl", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "演示地址：</td>"
		Response.Write "            <td><input name='DemoUrl' type='text' value='http://' size='80' maxlength='200'></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'> 固顶" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Hot' type='checkbox' id='Hot' value='yes' onclick=""javascript:document.myform.Hits.value='" & HitsOfHot & "'""> 热门" & ChannelShortName 
		Response.Write "              &nbsp;&nbsp;&nbsp;<input name='Elite' type='checkbox' id='Elite' value='yes'> 推荐" & ChannelShortName 
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Response.write ShowInfoPurview("0")
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>下载数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='0' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf

    Response.Write "  <p align='center'>" &vbcrlf
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "   <input name='AddType' type='hidden' id='AddType' value='" & AddType & "'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 保存修改结果 ' onClick=""document.myform.Action.value='SaveAdd';document.myform.target='_self';"" style='cursor:hand;'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>" &vbcrlf
    Response.Write "</form>" &vbcrlf
End Sub


Sub Modify()
    Dim rsSoft, tmpAuthor, tmpCopyFrom
    Dim AddType, imageDownloadUrl
    
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的" & ChannelShortName & "ID</li>"
        Exit Sub
    Else
        SoftID = PE_CLng(SoftID)
    End If
    Set rsSoft = Conn.Execute("select * from PW_Soft where SoftID=" & SoftID & "")
    If rsSoft.BOF And rsSoft.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "</li>"
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Sub
    End If
    ClassID = rsSoft("ClassID")
    If FoundErr = True Then
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Sub
    End If
    imageDownloadUrl = Trim(rsSoft("DownloadUrl"))
    tmpAuthor = rsSoft("Author")
    tmpCopyFrom = rsSoft("CopyFrom")

    Call ShowJS_Soft


    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;修改" & ChannelShortName & "</td></tr></table>" & vbCrLf
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Soft.asp' target='_self'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>" & vbCrLf
    Response.Write "  <tr align='center'>" & vbCrLf
    Response.Write "    <td class='tdbg' height='200' valign='top'>" & vbCrLf
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='SoftName' type='text' value='" & rsSoft("SoftName") & "' autocomplete='off' size='50' maxlength='255'> <font color='#FF0000'>*</font><input type='button' name='checksame' value='检查是否存在相同的" & ChannelShortName & "名' onclick=""showModalDialog('Admin_CheckSameTitle.asp?ModuleType=" & ModuleType & "&Title='+document.myform.SoftName.value,'checksame','dialogWidth:350px; dialogHeight:250px; help: no; scroll: no; status: no');"">"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Mid(rsSoft("Keyword"), 2, Len(rsSoft("Keyword")) - 2) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>作者/开发商：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Author' type='text' id='Author' value='" & tmpAuthor & "' size='50' maxlength='100'>" & GetAuthorList(AdminName)
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='CopyFrom' type='text' id='CopyFrom' value='" & tmpCopyFrom & "' size='50' maxlength='100'>" & GetCopyFromList()
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	if FoundInArr(FieldShow, "SoftPicUrl", ",") then
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'>缩略图：</td>" & vbCrLf
		Response.Write "            <td>" & vbCrLf
		Response.Write "              <input name='SoftPicUrl'  value='" & GetPhotoUrl(rsSoft("SoftPicUrl")) & "' type='text' id='SoftPicUrl' size='80' maxlength='200'>"
		Response.Write "              <input type='button' name='Button2' value='从已上传图片中选择' onclick='SelectSoft()'>"
		Response.Write "            </td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
		Response.Write "          <tr class='tdbg'>" & vbCrLf
		Response.Write "            <td width='120' align='right' class='tdbg5'></td>" & vbCrLf
		Response.Write "            <td><table><tr><td>上传" & ChannelShortName & "图片：</td><td><iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Softpic' frameborder=0 scrolling=no width='450' height='25'></iframe></td></tr></table></td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
	end if
	Call ShowTabs_MyField_Modify(rsSoft)
	if FoundInArr(FieldShow, "Intro", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>" &vbcrlf
		Response.Write "            <td><textarea name='SoftIntro' cols='80' rows='4'>"& rsSoft("SoftIntro") &"</textarea></td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	End if
	if FoundInArr(FieldShow, "Detial", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "            <td>"
		Call ShowFckEditor("Soft",GetPhotoUrl(rsSoft("SoftDetial")),"SoftDetial","100%","200")
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "DownloadUrl", ",") then
			Response.Write "          <tr class='tdbg' id='UrlType2'>"
			Response.Write "            <td width='120' align='right' class='tdbg5'>上传" & ChannelShortName & "：</td>"
			Response.Write "            <td>"
	
			Response.Write "              <iframe style='top:2px' ID='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Soft' frameborder=0 scrolling=no width='450' height='25'></iframe>"
			Response.Write "            </td>"
			Response.Write "          </tr>"	
			Response.Write "          <tr class='tdbg' id='UrlType2'>"
			Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "地址：</td>"
			Response.Write "            <td>"
			Response.Write "              <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
			Response.Write "                <tr>"
			Response.Write "                  <td width='450'>"
			Response.Write "                    <input type='hidden' name='DownloadUrls' value=''>"
        Response.Write "                    <select name='DownloadUrl' style='width:400;height:100' size='2' ondblclick='return ModifyUrl();'>"
        Dim DownloadUrls, arrDownloadUrls, iTemp
        DownloadUrls = GetPhotoUrl(rsSoft("DownloadUrl"))
		if DownloadUrls<>"" then
			If InStr(DownloadUrls, "$$$") > 1 Then
				arrDownloadUrls = Split(DownloadUrls, "$$$")
				For iTemp = 0 To UBound(arrDownloadUrls)
					Response.Write "<option value='" & arrDownloadUrls(iTemp) & "'>" & arrDownloadUrls(iTemp) & "</option>"
				Next
			Else
				Response.Write "<option value='" & DownloadUrls & "'>" & DownloadUrls & "</option>"
			End If
		End if
        Response.Write "                    </select>"
			Response.Write "                  </td>"
			Response.Write "                  <td>"
			Response.Write "                    <input type='button' name='Softselect' value='从已上传" & ChannelShortName & "中选择' onclick='SelectFile()'><br><br>"
			Response.Write "                    <input type='button' name='addurl' value='添加外部地址' onclick='AddUrl();'><br>"
			Response.Write "                    <input type='button' name='modifyurl' value='修改当前地址' onclick='return ModifyUrl();'><br>"
			Response.Write "                    <input type='button' name='delurl' value='删除当前地址' onclick='DelUrl();'>"
			Response.Write "                  </td>"
			Response.Write "                </tr>"
			Response.Write "              <tr><td  colspan='3'>系统提供的上传功能只适合上传比较小的" & ChannelShortName & "（如ASP源代码压缩包）。如果" & ChannelShortName & "比较大（" & MaxFileSize \ 1024 & "M以上），请先使用FTP上传，而不要使用系统提供的上传功能，以免上传出错或过度占用服务器的CPU资源。FTP上传后请将地址复制到下面的地址框中。</td></tr>"		
			Response.Write "              </table>"
			Response.Write "            </td>"
			Response.Write "          </tr>"
	
	
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "大小：</td>"
		Response.Write "            <td><input name='SoftSize' type='text' id='SoftSize' size='10' value='" & rsSoft("SoftSize") & "' maxlength='10'> K</strong></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftVersion", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "版本：</td>"
		Response.Write "            <td><input name='SoftVersion' type='text' size='15' maxlength='100' value='" & rsSoft("SoftVersion") & "'></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftType", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "类别：</td>"
		Response.Write "            <td><select name='SoftType' id='SoftType'>" & GetSoftType(rsSoft("SoftType")) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "SoftLanguage", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "语言：</td>"
		Response.Write "            <td><select name='SoftLanguage' id='SoftLanguage'>" & GetSoftLanguage(rsSoft("SoftLanguage")) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "CopyrightType", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>授权形式：</td>"
		Response.Write "            <td><select name='CopyrightType' id='CopyrightType'>" & GetCopyrightType(rsSoft("CopyrightType")) & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "OperatingSystem", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "平台：</td>"
		Response.Write "            <td>"
		Response.Write "              <input name='OperatingSystem' type='text' value='" & rsSoft("OperatingSystem") & "' size='80' maxlength='200'> <br>" & GetOperatingSystemList
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "DemoUrl", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "演示地址：</td>"
		Response.Write "            <td><input name='DemoUrl' type='text' value='" & rsSoft("DemoUrl") & "' size='80' maxlength='200'></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='OnTop' type='checkbox' id='OnTop' value='yes'"
		If rsSoft("OnTop") = True Then Response.Write " checked"
		Response.Write "> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Hot' type='checkbox' id='Hot' value='yes' disabled> 热门" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "              <input name='Elite' type='checkbox' id='Elite' value='yes'"
		If rsSoft("Elite") = True Then Response.Write " checked"
		Response.Write "> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	Response.write ShowInfoPurview(rsSoft("InfoPurview"))
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>下载数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='" & rsSoft("Hits") & "' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & rsSoft("UpdateTime") & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf

    Response.Write "  <p align='center'>" &vbcrlf
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "   <input name='AddType' type='hidden' id='AddType' value='" & AddType & "'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='SoftID' type='hidden' id='SoftID' value='" & SoftID & "'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 修 改 ' onClick=""document.myform.Action.value='SaveModify';document.myform.target='_self';"" style='cursor:hand;'>&nbsp; "
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>" &vbcrlf
    Response.Write "</form>" &vbcrlf
End Sub





Sub SaveSoft()
    Dim rsSoft, sql, trs, i
    Dim SoftID, ClassID, SoftName, Author, CopyFrom,SoftIntro,SoftDetial,InfoPurview
    Dim DownloadUrls, UpdateTime
    Dim AddType

    
    SoftID = GetForm("SoftID")
    ClassID = GetForm("ClassID")
    SoftName = GetForm("SoftName")
	SoftIntro = GetForm("SoftIntro")
    Keyword = GetForm("Keyword")
    Author = PE_HTMLEncode(GetForm("Author"))
    CopyFrom = PE_HTMLEncode(GetForm("CopyFrom"))

    DownloadUrls = PE_HTMLEncode(GetForm("DownloadUrls"))
	
    UpdateTime = PE_CDate(GetForm("UpdateTime"))
	InfoPurview = GetForm("GroupID")
	if FoundInArr(FieldShow, "Title", ",") then
		If SoftName = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "标题不能为空</li>"
		End If
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		If ClassID = "" Or IsNull(ClassID) Or Not IsNumeric(ClassID) Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>未指定所属栏目！</li>"
		Else
			ClassID = PE_CLng(ClassID)
			Select Case ClassID
			Case -1
                ClassName = "不指定任何栏目"
                Depth = -1
                ParentPath = ""
			Case Else
				Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
				If tClass.BOF And tClass.EOF Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
				Else
					ClassName = tClass("ClassName")
					Depth = tClass("Depth")
					ParentPath = tClass("ParentPath")
					ParentID = tClass("ParentID")
					Child = tClass("Child")
				End If
            Set tClass = Nothing
			End Select
		end if
	else
		ClassID = -1
		Depth = -1
		ParentPath = ""
	end if
	
	if FoundInArr(FieldShow, "Keyword", ",") then
		Keyword = ReplaceBadChar(Keyword)
		If Keyword = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入" & ChannelShortName & "关键字</li>"
		End If
	end if

    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("EnableNull") = False and rsField("ShowOnForm") = True Then
            If GetForm(rsField("FieldName")) = "" Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>请输入" & rsField("Title") & "！</li>"
            End If
        End If
        rsField.MoveNext
    Loop
    
    If FoundErr = True Then
        Exit Sub
    End If
    SoftName = PE_HTMLEncode(SoftName)
    Keyword = Replace("|" & Keyword & "|","||","|")
    If Author = "" Then Author ="佚名"
    If CopyFrom = "" Then CopyFrom = "本站原创"
    Set rsSoft = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
            SoftID = GetNewID("PW_Soft", "SoftID")
            sql = "select top 1 * from PW_Soft"
            rsSoft.Open sql, Conn, 1, 3
            rsSoft.addnew
            rsSoft("SoftID") = SoftID
            rsSoft("ChannelID") = ChannelID
    ElseIf Action = "SaveModify" Then
        If SoftID = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定" & ChannelShortName & "ID的值</li>"
        Else
            SoftID = PE_CLng(SoftID)
            sql = "select * from PW_Soft where SoftID=" & SoftID
            rsSoft.Open sql, Conn, 1, 3
            If rsSoft.BOF And rsSoft.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到此" & ChannelShortName & "，可能已经被其他人删除。</li>"
            End If
        End If
    End If
    '将绝对地址转化为相对地址
	SoftDetial = GetForm("SoftDetial")
	SoftDetial = SetPhotoUrl(SoftDetial) '替换地址为{PW_Photourl}
    SoftDetial = ReplaceRemoteUrl(SoftDetial)
    If FoundErr = True Then
        Exit Sub
    End If
    rsSoft("ClassID") = ClassID
    rsSoft("SoftName") = SoftName
    rsSoft("Keyword") = Keyword

    rsSoft("SoftVersion") = GetForm("SoftVersion")
    rsSoft("SoftType") = PE_HTMLEncode(GetForm("SoftType"))
    rsSoft("SoftLanguage") = PE_HTMLEncode(GetForm("SoftLanguage"))
    rsSoft("CopyrightType") = PE_HTMLEncode(GetForm("CopyrightType"))
    rsSoft("OperatingSystem") = PE_HTMLEncode(GetForm("OperatingSystem"))
    rsSoft("Author") = Author
    rsSoft("CopyFrom") = CopyFrom

    rsSoft("DemoUrl") = PE_HTMLEncode(GetForm("DemoUrl"))
    rsSoft("SoftPicUrl") = PE_HTMLEncode(SetPhotoUrl(GetForm("SoftPicUrl")))
    rsSoft("SoftIntro") = SoftIntro
	rsSoft("SoftDetial") = SoftDetial
    rsSoft("Hits") = PE_CLng(GetForm("Hits"))
    rsSoft("UpdateTime") = UpdateTime
    rsSoft("Deleted") = False
    rsSoft("OnTop") = PE_CBool(GetForm("OnTop"))
    rsSoft("Elite") = PE_CBool(GetForm("Elite"))
    rsSoft("SoftSize") = PE_CLng(GetForm("SoftSize"))
    rsSoft("DownloadUrl") = SetPhotoUrl(DownloadUrls)
    rsSoft("InfoPurview") = InfoPurview

    If Not (rsField.BOF And rsField.EOF) Then
        rsField.MoveFirst
        Do While Not rsField.EOF
			If FoundInArr(FieldShow, rsField("FieldName"), ",") then
				If GetForm(rsField("FieldName")) <> "" or rsField("EnableNull")=True  Then
					rsSoft(Trim(rsField("FieldName"))) = GetForm(rsField("FieldName"))
				End If
			End if
            rsField.MoveNext
        Loop
    End If
	rsField.Close
    Set rsField = Nothing
    rsSoft.Update
    rsSoft.Close
    Set rsSoft = Nothing

    Response.Write "<br><br>"
    Response.Write "<table class='border' align=center width='500' border='0' cellpadding='2' cellspacing='1'>" &vbcrlf
    Response.Write "  <tr align=center> " &vbcrlf
    Response.Write "    <td  height='22' align='center' class='title' colspan='2'> "
    If Action = "SaveAdd" Then
        Response.Write "<b>添加" & ChannelShortName & "成功</b>"
    Else
        Response.Write "<b>修改" & ChannelShortName & "成功</b>"
    End If
    Response.Write "    </td>" &vbcrlf
    Response.Write "  </tr>" &vbcrlf
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "  <tr class='tdbg'>" &vbcrlf
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>所属栏目：</td>" &vbcrlf
		Response.Write "    <td width='400'>" & ShowClassPath() & "</td>" &vbcrlf
		Response.Write "  </tr>" &vbcrlf
	end if
    Response.Write "  <tr class='tdbg'>" &vbcrlf
    Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "名称：</td>" &vbcrlf
    Response.Write "    <td width='400'>" & SoftName & "</td>" &vbcrlf
    Response.Write "  </tr>" &vbcrlf
	if FoundInArr(FieldShow, "SoftVersion", ",") then
		Response.Write "  <tr class='tdbg'>" &vbcrlf
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "版本：</td>" &vbcrlf
		Response.Write "    <td width='400'>" & GetForm("SoftVersion") & "</td>" &vbcrlf
		Response.Write "  </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Author", ",") then
		Response.Write "  <tr class='tdbg'>" &vbcrlf
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "作者：</td>" &vbcrlf
		Response.Write "    <td width='400'>" & Author & "</td>" &vbcrlf
		Response.Write "  </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "  <tr class='tdbg'>" &vbcrlf
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>关 键 字：</td>" &vbcrlf
		Response.Write "    <td width='400'>" & Mid(Keyword, 2, Len(Keyword) - 2) & "</td>" &vbcrlf
		Response.Write "  </tr>" &vbcrlf
	end if
    Response.Write "  <tr class='tdbg'>" &vbcrlf
    Response.Write "    <td height='40' colspan='2' align='center'>" &vbcrlf
    Response.Write "【<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Modify&SoftID=" & SoftID & "'>修改此" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Add&AddType=" & AddType & "&ClassID=" & ClassID & "'>继续添加" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&Action=Manage&ClassID=" & ClassID & "'>" & ChannelShortName & "管理</a>】&nbsp;"
    Response.Write "    </td>" &vbcrlf
    Response.Write "  </tr>" &vbcrlf
    Response.Write "</table>" & vbCrLf
    Session("Keyword") = Trim(Request("Keyword"))
    Session("Author") = Author
    Session("CopyFrom") = CopyFrom
	If UseCreateHTML > 0 And InfoPurview="0" And ObjInstalled_FSO = True And AutoCreateType = 1 Then
		Response.Write "<br><iframe id='CreateSoft' width='100%' height='210' frameborder='0' src='Admin_CreateSoft.asp?ChannelID=" & ChannelID & "&Action=CreateSoft2&ClassID=" & ClassID & "&SoftID=" & SoftID & "&ShowBack=No'></iframe>"
    End If
End Sub



Sub SetProperty()
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    End If
    
    Dim sqlProperty, rsProperty
    If InStr(SoftID, ",") > 0 Then
        sqlProperty = "select * from PW_Soft where SoftID in (" & SoftID & ")"
    Else
        sqlProperty = "select * from PW_Soft where SoftID=" & SoftID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
		Select Case Action
		Case "SetOnTop"
			rsProperty("OnTop") = True
		Case "CancelOnTop"
			rsProperty("OnTop") = False
		Case "SetElite"
			rsProperty("Elite") = True
		Case "CancelElite"
			rsProperty("Elite") = False
		End Select
            rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Soft.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Soft.asp?ChannelID=" & ChannelID,1)
End Sub

'******************************************************************************************
'以下为删除、清空、还原等操作使用的函数，各模块实现过程类似，修改时注意同时修改各模块内容。
'******************************************************************************************

Sub Del()
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select S.SoftID,S.SoftName,S.UpdateTime,S.Deleted,S.ClassID from PW_Soft S "
    If InStr(SoftID, ",") > 0 Then
        sqlDel = sqlDel & " where S.SoftID in (" & SoftID & ") order by S.SoftID"
    Else
        sqlDel = sqlDel & " where S.SoftID=" & SoftID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
		rsDel("Deleted") = True
		rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Soft.asp?ChannelID=" & ChannelID)
	Call Refresh("Admin_Soft.asp?ChannelID=" & ChannelID,1)
End Sub



Sub ConfirmDel()
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select SoftPicUrl,DownloadUrl from PW_Soft where SoftID in (" & SoftID & ")"
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        Call DelFiles(GetUploadFiles(GetPhotoUrl(rsDel("DownloadUrl")),GetPhotoUrl(rsDel("SoftPicUrl"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Conn.Execute ("delete from PW_Soft where SoftID in (" & SoftID & ")")
    Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & SoftID & ")")
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub ClearRecyclebin()
    
    Dim sqlDel, rsDel
    SoftID = ""
    sqlDel = "select SoftID,SoftPicUrl,DownloadUrl from PW_Soft where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        If SoftID = "" Then
            SoftID = rsDel(0)
        Else
            SoftID = SoftID & "," & rsDel(0)
        End If
        	Call DelFiles(GetUploadFiles(GetPhotoUrl(rsDel("DownloadUrl")),GetPhotoUrl(rsDel("SoftPicUrl"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    If SoftID <> "" Then
        Conn.Execute ("delete from PW_Soft where Deleted=" & PE_True & " and ChannelID=" & ChannelID & "")
        Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & SoftID & ")")
    End If
    Call CloseConn
    Response.Redirect ComeUrl
End Sub

Sub Restore()
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    If InStr(SoftID, ",") > 0 Then
        sqlDel = "select * from PW_Soft where SoftID in (" & SoftID & ")"
    Else
        sqlDel = "select * from PW_Soft where SoftID=" & SoftID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Soft.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Soft.asp?ChannelID=" & ChannelID,1)
End Sub

Sub RestoreAll()
    Dim sqlDel, rsDel
    sqlDel = "select * from PW_Soft where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Soft.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Soft.asp?ChannelID=" & ChannelID,1)
End Sub

Sub Show()
    If SoftID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定" & ChannelShortName & "ID！</li>"
        Exit Sub
    End If
    Dim rsSoft
    Set rsSoft = Conn.Execute("select * from PW_Soft where SoftID=" & SoftID & "")
    If rsSoft.BOF And rsSoft.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "！</li>"
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Sub
    End If
    ClassID = rsSoft("ClassID")
	Select Case ClassID
        Case 0
            FoundErr = True
            ErrMsg = ErrMsg & "<li>指定的栏目不允许此操作！</li>"
		Case -1
			ClassName = "不指定任何栏目"
		Case Else
            Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
            If tClass.BOF And tClass.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
            Else
                ClassName = tClass("ClassName")
                Depth = tClass("Depth")
                ParentPath = tClass("ParentPath")
                ParentID = tClass("ParentID")
                Child = tClass("Child")
            End If
            Set tClass = Nothing
	end select
    If FoundErr = True Then
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Sub
    End If

    Response.Write "<br>您现在的位置：&nbsp;<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "管理</a>&nbsp;&gt;&gt;&nbsp;"
    If ParentID > 0 Then
        Dim sqlPath, rsPath
        sqlPath = "select ClassID,ClassName from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
        Set rsPath = Conn.Execute(sqlPath)
        Do While Not rsPath.EOF
            Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&ClassID=" & rsPath(0) & "'>" & rsPath(1) & "</a>&nbsp;&gt;&gt;&nbsp;"
            rsPath.MoveNext
        Loop
        rsPath.Close
        Set rsPath = Nothing
    End If
    Response.Write "<a href='Admin_Soft.asp?ChannelID=" & ChannelID & "&ClassID=" & ClassID & "'>" & ClassName & "</a>&nbsp;&gt;&gt;&nbsp;查看" & ChannelShortName & "信息："
    Response.Write rsSoft("SoftName") & "<br><br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg' height='200' valign='top'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
    Response.Write "<tr class='tdbg'>"
    Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
    Response.Write "  <td><strong>" & PE_HTMLEncode(rsSoft("SoftName")) & "&nbsp;&nbsp;"
	if FoundInArr(FieldShow, "SoftVersion", ",") then 
		Response.write PE_HTMLEncode(rsSoft("SoftVersion"))
	end if
    Response.Write "</strong></td></tr>"
    Response.Write "<tr class='tdbg'>"
    Response.Write "  <td width='100' align='right' class='tdbg5'>文件大小：</td>"
    Response.Write "  <td width='300'>" & rsSoft("SoftSize") & " K" & "</td>"
    Response.Write "</tr>"
	if FoundInArr(FieldShow, "Author", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>开 发 商：</td>"
		Response.Write "  <td width='300'>" & rsSoft("Author") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "CopyFrom", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "来源：</td>"
		Response.Write "  <td width='300'>" & rsSoft("CopyFrom") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "OperatingSystem", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "平台：</td>"
		Response.Write "  <td width='300'>" & rsSoft("OperatingSystem") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "SoftType", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "类别：</td>"
		Response.Write "  <td width='300'>" & rsSoft("SoftType") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "SoftLanguage", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "语言：</td>"
		Response.Write "  <td width='300'>" & rsSoft("SoftLanguage") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "CopyrightType", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>授权形式：</td>"
		Response.Write "  <td width='300'>" & rsSoft("CopyrightType") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "DemoUrl", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>演示地址：</td>"
		Response.Write "  <td width='300'><a href='" & rsSoft("DemoUrl") & "' target='_blank'>" & rsSoft("DemoUrl") & "</a></td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>添加时间：</td>"
		Response.Write "  <td width='300'>" & rsSoft("UpdateTime") & "</td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "Hits", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>下载次数：</td>"
		Response.Write "  <td>" & rsSoft("Hits")
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "DownloadUrl", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>下载地址：</td>"
		Response.Write "  <td>"
		Call ShowDownloadUrls(GetPhotoUrl(rsSoft("DownloadUrl")))
		Response.Write "  </td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "SoftPicUrl", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "缩略图：</td>"
		Response.Write "  <td align=left valign='middle'>&nbsp;"
		If rsSoft("SoftPicUrl") = "" Then
			Response.Write "<img src='" & InstallDir & "images/nopic.gif'>"
		Else
			Response.Write "<img src='" & GetPhotoUrl(rsSoft("SoftPicUrl")) & "' width='150'>"
		End If
		Response.Write "  </td>"
		Response.Write "</tr>"
	end if
	if FoundInArr(FieldShow, "Intro", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "  <td height='100'>" & GetPhotoUrl(rsSoft("SoftIntro")) & "</td>"
		Response.Write "</tr>"
	end if
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf
    Response.Write "<form name='formA' method='get' action='Admin_Soft.asp'><p align='center'>"
    Response.Write "<input type='hidden' name='ChannelID' value='" & ChannelID & "'>"
    Response.Write "<input type='hidden' name='SoftID' value='" & SoftID & "'>"
    Response.Write "<input type='hidden' name='Action' value=''>" & vbCrLf
    Response.Write "<input type='submit' name='submit' value=' 返 回 ' onclick=""window.location.href='Admin_Soft.asp'"">&nbsp;&nbsp;"
    If rsSoft("Deleted") = False Then
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value=' 删 除 ' onclick=""document.formA.Action.value='Del'"">&nbsp;&nbsp;"
		end if
		if FoundInArr(FieldShow, "Property", ",") then
            If rsSoft("OnTop") = False Then
                Response.Write "<input type='submit' name='submit' value='设为固顶' onclick=""document.formA.Action.value='SetOnTop'"">&nbsp;&nbsp;"
            Else
                Response.Write "<input type='submit' name='submit' value='取消固顶' onclick=""document.formA.Action.value='CancelOnTop'"">&nbsp;&nbsp;"
            End If
            If rsSoft("Elite") = False Then
                Response.Write "<input type='submit' name='submit' value='设为推荐' onclick=""document.formA.Action.value='SetElite'"">"
            Else
                Response.Write "<input type='submit' name='submit' value='取消推荐' onclick=""document.formA.Action.value='CancelElite'"">"
            End If
		end if
    Else
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value='彻底删除' onclick=""if(confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！')==true){document.formA.Action.value='ConfirmDel';}"">&nbsp;&nbsp;"
            Response.Write "<input type='submit' name='submit' value=' 还 原 ' onclick=""document.formA.Action.value='Restore'"">"
		end if
    End If
    Response.Write "</p></form>"

    rsSoft.Close
    Set rsSoft = Nothing
End Sub


Function GetSoftLanguage(SoftLanguage)
	Dim arrSoftLanguage
	arrSoftLanguage=Array("英文","简体中文","繁体中文","简繁中文","多国语言","其他语言")
    If IsArray(arrSoftLanguage) = False Then Exit Function
    
    Dim strTemp, i
    For i = 0 To UBound(arrSoftLanguage)
        If Trim(arrSoftLanguage(i)) <> "" Then
            strTemp = strTemp & "<option value='" & arrSoftLanguage(i) & "'"
            If Trim(SoftLanguage) = arrSoftLanguage(i) Then strTemp = strTemp & " selected"
            strTemp = strTemp & ">" & arrSoftLanguage(i) & "</option>"
        End If
    Next
    GetSoftLanguage = strTemp
End Function

Function GetSoftType(SoftType)
	Dim arrSoftType
	arrSoftType=Array("国产软件","国外软件","汉化补丁","程序源码","电影下载","Flash动画","其他")
    If IsArray(arrSoftType) = False Then Exit Function
    
    Dim strTemp, i
    For i = 0 To UBound(arrSoftType)
        If Trim(arrSoftType(i)) <> "" Then
            strTemp = strTemp & "<option value='" & arrSoftType(i) & "'"
            If Trim(SoftType) = arrSoftType(i) Then strTemp = strTemp & " selected"
            strTemp = strTemp & ">" & arrSoftType(i) & "</option>"
        End If
    Next
    GetSoftType = strTemp
End Function

Function GetCopyrightType(CopyrightType)
	Dim arrCopyrightType
	arrCopyrightType=Array("免费版","共享版","试用版","演示版","注册版","破解版","零售版")
    If IsArray(arrCopyrightType) = False Then Exit Function
    
    Dim strTemp, i
    For i = 0 To UBound(arrCopyrightType)
        If Trim(arrCopyrightType(i)) <> "" Then
            strTemp = strTemp & "<option value='" & arrCopyrightType(i) & "'"
            If Trim(CopyrightType) = arrCopyrightType(i) Then strTemp = strTemp & " selected"
            strTemp = strTemp & ">" & arrCopyrightType(i) & "</option>"
        End If
    Next
    GetCopyrightType = strTemp
End Function


Function GetOperatingSystemList()
    Dim strOperatingSystemList, i
    Dim arrOperatingSystem
	arrOperatingSystem=Array("Linux","DOS","9x","95","98","Me","NT","2000","XP",".NET","2003")
    strOperatingSystemList = "<script language = 'JavaScript'>" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "function ToSystem(addTitle){" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "    var str=document.myform.OperatingSystem.value;" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "    if (document.myform.OperatingSystem.value=="""") {" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "        document.myform.OperatingSystem.value=document.myform.OperatingSystem.value+addTitle;" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "    }else{" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "        if (str.substr(str.length-1,1)==""/""){" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "            document.myform.OperatingSystem.value=document.myform.OperatingSystem.value+addTitle;" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "        }else{" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "            document.myform.OperatingSystem.value=document.myform.OperatingSystem.value+""/""+addTitle;" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "        }" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "    }" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "    document.myform.OperatingSystem.focus();" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "}" & vbCrLf
    strOperatingSystemList = strOperatingSystemList & "</script>" & vbCrLf

    strOperatingSystemList = strOperatingSystemList & "<font color='#808080'>平台选择："
    If IsArray(arrOperatingSystem) Then
        For i = 0 To UBound(arrOperatingSystem)
            If Trim(arrOperatingSystem(i)) <> "" Then
                strOperatingSystemList = strOperatingSystemList & "<a href=""javascript:ToSystem('" & arrOperatingSystem(i) & "')"">" & arrOperatingSystem(i) & "</a>" & "/"
            End If
        Next
    End If
    strOperatingSystemList = strOperatingSystemList & "</font>"
    GetOperatingSystemList = strOperatingSystemList
End Function


Function GetUploadFiles(DownloadUrls, SoftPicUrl)
    Dim arrDownloadUrls, arrUrls, iTemp, strUrls
    strUrls = ""
    strUrls = strUrls & SoftPicUrl
    arrDownloadUrls = Split(DownloadUrls, "$$$")
    For iTemp = 0 To UBound(arrDownloadUrls)
        arrUrls = Split(arrDownloadUrls(iTemp), "|")
        If UBound(arrUrls) = 1 Then
			strUrls = strUrls & "|" & arrUrls(1)
        End If
    Next
    GetUploadFiles = strUrls
End Function


Sub ShowDownloadUrls(DownloadUrls)
    Dim arrDownloadUrls, arrUrls, iTemp
    DownloadUrls = Replace(DownloadUrls,  "&nbsp;", " ")
    DownloadUrls = Replace(DownloadUrls,  "@@@", "")
    arrDownloadUrls = Split(DownloadUrls, "$$$")
    For iTemp = 0 To UBound(arrDownloadUrls)
        arrUrls = Split(arrDownloadUrls(iTemp), "|")
        If UBound(arrUrls) >= 1 Then
        	Response.Write arrUrls(0) & "：<a href='" & arrUrls(1) &"'>" & arrUrls(1) & "</a><br>"
		else
			Response.Write DownloadUrls
        End If
    Next
End Sub

%>