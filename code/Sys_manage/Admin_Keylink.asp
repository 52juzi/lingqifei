<!--#include file="Admin_Common.asp"-->
<%

Const NeedCheckComeUrl = True   '是否需要检查外部访问

Const PurviewLevel_Channel = 0   '0--不检查,1检查
Const PurviewLevel_Others = ""   '其他权限
Response.Write "<html><head><title>站内链接管理</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'><link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "<SCRIPT language=javascript>" & vbCrLf
Response.Write "function unselectall(){" & vbCrLf
Response.Write "    if(document.myform.chkAll.checked){" & vbCrLf
Response.Write " document.myform.chkAll.checked = document.myform.chkAll.checked&0;" & vbCrLf
Response.Write "    }" & vbCrLf
Response.Write "}" & vbCrLf

Response.Write "function CheckAll(form){" & vbCrLf
Response.Write "  for (var i=0;i<form.elements.length;i++){" & vbCrLf
Response.Write "    var e = form.elements[i];" & vbCrLf
Response.Write "    if (e.Name != 'chkAll'&&e.disabled!=true)" & vbCrLf
Response.Write "       e.checked = form.chkAll.checked;" & vbCrLf
Response.Write "    }" & vbCrLf
Response.Write "}" & vbCrLf
Response.Write "function CheckKeyLink(){" & vbCrLf
Response.Write "  if(document.myform.Linkword.value==''){" & vbCrLf
Response.Write "      alert('链接文字不能为空！');" & vbCrLf
Response.Write "   document.myform.Linkword.focus();" & vbCrLf
Response.Write "      return false;" & vbCrLf
Response.Write "    }" & vbCrLf
Response.Write "  if(document.myform.Linkaddress.value=='' || document.myform.Linkaddress.value=='http://'){" & vbCrLf
Response.Write "      alert('链接地址不能为空！');" & vbCrLf
Response.Write "   document.myform.Linkaddress.focus();" & vbCrLf
Response.Write "      return false;" & vbCrLf
Response.Write "    }" & vbCrLf
Response.Write "}" & vbCrLf
Response.Write "</script>" & vbCrLf
Response.Write "</head>" & vbCrLf

Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
call ShowPageTitle("站 内 链 接 管 理")
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td>"
Response.Write "    <a href='Admin_Keylink.asp'>链接管理首页</a>&nbsp;|&nbsp;<a href='Admin_Keylink.asp?Action=Add'>新增链接</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "  </table>" & vbCrLf


Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd"
    Call SaveAdd
Case "SaveModify"
    Call SaveModify
Case "Del"
    Call Del
Case "Show", "Dis"
	Call SetShow
Case Else
    Call Main
end Select

If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub Main()
	Dim rsKeylink, sqlKeylink,iCount
	Response.Write "<br><table width='100%' border='0' align='center' cellpadding='0' cellspacing='0'>"
	Response.Write "  <tr><td height='22'>您现在的位置：网站管理&nbsp;&gt;&gt;&nbsp;站内链接管理</td></tr>"
	Response.Write "</table>"
	Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
	Response.Write "  <form name='myform' method='Post' action='Admin_Keylink.asp' onsubmit=""return confirm('确定要删除选中的站内链接吗？');"">"
	Response.Write "     <td>"
	Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
	Response.Write "  <tr align='center' class='title' height='22'>"
	Response.Write "    <td width='30'><strong>选中</strong></td>"
	Response.Write "    <td width='40'><strong>序号</strong></td>"
	Response.Write "    <td width='200'><strong>链接名称</strong></td>"
	Response.Write "    <td height='22'><strong>链接地址</strong></td>"
	Response.Write "    <td width='40'><strong>状态</strong></td>"
	Response.Write "    <td width='100'><strong>操 作</strong></td>"
	Response.Write "  </tr>"
    Set rsKeylink = Server.CreateObject("Adodb.RecordSet")
	sqlKeylink = "select * from PW_KeyLink order by ID Desc"
	rsKeylink.Open sqlKeylink, Conn, 1, 1
    If rsKeylink.BOF And rsKeylink.EOF Then
        Response.Write "  <tr class='tdbg'><td colspan='8' align='center'><br>没有任何站内链接！<br><br></td></tr>"
	else
		totalPut = rsKeylink.RecordCount
		If CurrentPage < 1 Then
			CurrentPage = 1
		End If
			If (CurrentPage - 1) * MaxPerPage > totalPut Then
				If (totalPut Mod MaxPerPage) = 0 Then
					CurrentPage = totalPut \ MaxPerPage
				Else
					CurrentPage = totalPut \ MaxPerPage + 1
				End If
			End If
			If CurrentPage > 1 Then
				If (CurrentPage - 1) * MaxPerPage < totalPut Then
					rsKeylink.Move (CurrentPage - 1) * MaxPerPage
				Else
					CurrentPage = 1
				End If
			End If
			Do While Not rsKeylink.EOF
				Response.Write "  <tr align='center' class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
				Response.Write "    <td><input name='ID' type='checkbox' id='ID' value='" & rsKeylink("ID") & "'"
				Response.Write " onclick='unselectall()'></td>"
				Response.Write "    <td>" & rsKeylink("ID") & "</td>"
				Response.Write "    <td>" & rsKeylink("Linkword") & "</td>"
				Response.Write "    <td>" & rsKeylink("Linkaddress") & "</td>"
				Response.Write "    <td align='center'>"& vbCrLf
				If rsKeylink("isUse")=1 Then
					Response.write "<font color='Green'><a href='Admin_Keylink.asp?Action=Dis&ID=" & rsKeylink("ID") & "'><font color='blue'>启用</font></a></font>"
				Else
					Response.write "<font color='red'><a href='Admin_Keylink.asp?Action=Show&ID=" & rsKeylink("ID") & "'><font color='red'>禁用</font></a></font>"
				End if
				Response.Write "	</td>"& vbCrLf
			Response.Write "<td><a href='Admin_Keylink.asp?Action=Modify&ID=" & rsKeylink("ID") & "'>修改</a>&nbsp;&nbsp;"
			Response.Write "<a href='Admin_Keylink.asp?Action=Del&ID=" & rsKeylink("ID") & "' onClick=""return confirm('确定要删除这个站内链接吗？');"">删除</a></td>"
			Response.Write "</tr>"
			iCount = iCount + 1
			If iCount >= MaxPerPage Then Exit Do
			rsKeylink.MoveNext
		Loop
	End If
    rsKeylink.Close
    Set rsKeylink = Nothing
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td width='200' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'> 选中本页显示的所有站内链接</td>"
    Response.Write "    <td><input name='Action' type='hidden' id='Action' value='Del'>"
    Response.Write "    <input name='Submit' type='submit' id='Submit' value='删除选中的站内链接'></td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
    Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "个站内链接", True)
End Sub

Sub Add()
    Response.Write "<form method='post' action='Admin_Keylink.asp' name='myform' onsubmit='javascript:return CheckKeyLink();'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border' >"
    Response.Write "    <tr class='title'> "
	Response.Write "      <td height='22' colspan='2'> <div align='center'><strong>新 增 站 内 连 接</strong></div></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td width='100' align='right' class='tdbg'><strong>链接文字：</strong></td><td><input name='Linkword' type='text' size='80'> <font color='#FF0000'>*</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>链接地址：</strong></td><td><input name='Linkaddress' type='text'size='80' value='http://'> <font color='#FF0000'>*</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>优先级别：</strong></td><td><input name='Priority' type='text'size='5' value='1'> <font color='#FF0000'>*</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg''><strong>替换次数：</strong></td><td><input name='Replacenumber ' type='text'size='5' value='0'> <font color='#FF0000'>替换目标的次数，为0时则全部替换</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>打开方式：</strong></td><td><input name='OpenType' type='radio' value='0' checked>原窗口&nbsp;<input name='OpenType' type='radio' value='1'>新窗口&nbsp;</td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>状态：</strong></td><td><input name='isUse' type='radio' value='1' checked>启用&nbsp;<input name='isUse' type='radio' value='0'>禁用&nbsp;</td>"
	Response.Write "    </tr>"
	Response.Write "  <tr>"
	Response.Write "  <td height='40' colspan='2' align='center' class='tdbg'>"
	Response.Write "    <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
	Response.Write "    <input  type='submit' name='Submit' value=' 添 加 ' style='cursor:hand;'>&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Keylink.asp';"" style='cursor:hand;'></td>"
    Response.Write "  </tr>"
    Response.Write "</table></form>"
End Sub

Sub Modify
    Dim KeyID
    Dim rsKey, sqlKey
    KeyID = PE_CLng(Trim(Request("ID")))
    If KeyID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的ID</li>"
        Exit Sub
    End If
    sqlKey = "Select ID,Linkword,Linkaddress,isUse,OpenType,Replacenumber ,Priority from PW_KeyLink where ID=" & KeyID
    Set rsKey = Server.CreateObject("Adodb.RecordSet")
    rsKey.Open sqlKey, Conn, 1, 1
	If rsKey.BOF And rsKey.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不存在此站内链接！</li>"
        rsKey.Close
        Set rsKey = Nothing
        Exit Sub
    End If
	Response.Write "<form method='post' action='Admin_Keylink.asp' name='myform' onsubmit='return CheckKeyLink();'>"
	Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
	Response.Write "    <tr class='title'> "
	Response.Write "      <td height='22' colspan='2'> <div align='center'><font size='2'><strong>修 改 站 内 连 接</strong></font></div></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td width='100' align='right' class='tdbg'><strong>链接文字：</strong></td><td><input name='Linkword' type='text' value='" & rsKey("Linkword") & "' size='80'> <font color='#FF0000'>*</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td align='right' class='tdbg'><strong>链接地址：</strong></td><td><input name='Linkaddress' type='text' value='" & rsKey("Linkaddress") & "' size='80'> <font color='#FF0000'>*</font></select>"
	Response.Write "    </td></tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>优先级别：</strong></td><td><input name='Priority' type='text'size='5' value='" & rsKey("Priority") & "'> <font color='#FF0000'>*</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>替换次数：</strong></td><td><input name='Replacenumber ' type='text'size='5' value='" & rsKey("Replacenumber") & "'> <font color='#FF0000'>替换目标的次数，为0时则全部替换</font></td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>打开方式：</strong></td><td><input name='OpenType' type='radio' value='0'"
	If rsKey("OpenType") = 0 Then Response.Write "checked"
	Response.Write ">原窗口&nbsp;<input name='OpenType' type='radio' value='1'"
	If rsKey("OpenType") = 1 Then Response.Write "checked"
	Response.Write ">新窗口&nbsp;</td>"
	Response.Write "    </tr>"
	Response.Write "    <tr class='tdbg'> "
	Response.Write "      <td align='right' class='tdbg'><strong>状态：</strong></td><td><input name='isUse' type='radio' value='1'"
	If rsKey("isUse") = 1 Then Response.Write "checked"
	Response.Write ">启用&nbsp;<input name='isUse' type='radio' value='0'"
	If rsKey("isUse") = 0 Then Response.Write "checked"
	Response.Write ">禁用&nbsp;</td>"
	Response.Write "    </tr>"
	Response.Write "    <tr>"
	Response.Write "      <td colspan='2' align='center' class='tdbg'>"
	Response.Write "      <input name='ID' type='hidden' id='ID' value=" & rsKey("ID") & ">"
	Response.Write "      <input name='Action' type='hidden' id='Action' value='SaveModify'>"
	Response.Write "      <input  type='submit' name='Submit' value='保存修改结果' style='cursor:hand;'>&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Keylink.asp'"" style='cursor:hand;'></td>"
	Response.Write "    </tr>"
	Response.Write "  </table>"
	Response.Write "</form>"
    rsKey.Close
    Set rsKey = Nothing
End Sub

Sub SaveAdd
    Dim Linkword, Linkaddress, isUse, Replacenumber , OpenType, Priority
    Dim rsKey, sqlKey
    Linkword = GetForm("Linkword")
    Linkaddress = GetForm("Linkaddress")
    isUse = PE_CLng(GetForm("isUse"))
    Replacenumber  = PE_CLng(GetForm("Replacenumber "))
    Priority = PE_CLng(GetForm("Priority"))
    If Priority = 0 Then Priority = 1
    OpenType = PE_CLng(GetForm("OpenType"))

    If Linkword = "" Or Linkaddress = "" Or Linkaddress="http://" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>链接文字不能为空！</li>"
    End If

    If FoundErr = True Then
        Exit Sub
    End If
    Set rsKey = Server.CreateObject("Adodb.RecordSet")
    sqlKey = "Select Linkword,Linkaddress,isUse,OpenType,Replacenumber,Priority from PW_KeyLink where Linkword='" & Linkword & "'"
    rsKey.Open sqlKey, Conn, 1, 3
    If Not (rsKey.BOF And rsKey.EOF) Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>数据库中已经存在此链接！</li>"
        rsKey.Close
        Set rsKey = Nothing
        Exit Sub
    End If
    rsKey.addnew
    rsKey("Linkword") = Linkword
    rsKey("Linkaddress") = PE_HTMLEncode(Linkaddress)
    rsKey("isUse") = isUse
    rsKey("OpenType") = OpenType
    rsKey("Replacenumber") = Replacenumber 
    rsKey("Priority") = Priority
    rsKey.Update
    rsKey.Close
    Call CloseConn
	Response.Redirect "Admin_Keylink.asp"
End Sub

Sub SaveModify
    Dim Linkword, Linkaddress, isUse, KeyID, Replacenumber , OpenType, Priority
    Dim rsKey, sqlKey
    Linkword = GetForm("Linkword")
    Linkaddress = GetForm("Linkaddress")
    isUse = PE_CLng(GetForm("isUse"))
    Replacenumber  = PE_CLng(GetForm("Replacenumber "))
    Priority = PE_CLng(GetForm("Priority"))
    If Priority = 0 Then Priority = 1
    OpenType = PE_CLng(GetForm("OpenType"))

    KeyID = GetForm("ID")
    If Linkword = "" Or Linkaddress = "" Or Linkaddress="http://" Or KeyID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请将表单填写完整！</li>"
    Else
        KeyID = PE_CLng(KeyID)
    End If

    If FoundErr = True Then
        Exit Sub
    End If
    Set rsKey = Server.CreateObject("Adodb.RecordSet")
    sqlKey = "Select ID,Linkword,Linkaddress,isUse,OpenType,Replacenumber ,Priority from PW_KeyLink where ID=" & KeyID
    rsKey.Open sqlKey, Conn, 1, 3
    If Not (rsKey.BOF And rsKey.EOF) Then
        rsKey("Linkword") = Linkword
        rsKey("Linkaddress") = PE_HTMLEncode(Linkaddress)
        rsKey("isUse") = isUse
        rsKey("OpenType") = OpenType
        rsKey("Replacenumber") = Replacenumber 
        rsKey("Priority") = Priority
        rsKey.Update
    End If
    rsKey.Close
    Call CloseConn
    Response.Redirect "Admin_Keylink.asp"
End Sub


Sub Del
    Dim KeyID
    Dim rsKey, sqlKey
    
    KeyID = GetValue("ID")
    If IsValidID(KeyID) = False Then
        KeyID = ""
    End If
    If KeyID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要删除的链接ID</li>"
        Exit Sub
    End If
    If InStr(KeyID, ",") > 0 Then
        Conn.Execute ("delete from PW_KeyLink where ID in (" & KeyID & ")")
    Else
        Conn.Execute ("delete from PW_KeyLink where ID=" & KeyID & "")
    End If
    Call CloseConn
    Response.Redirect "Admin_Keylink.asp"
End Sub


Sub SetShow()
    Dim KeylinkID,Stat
    KeylinkID = GetValue("ID")
    If KeylinkID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要设置的链接ID</li>"
        Exit Sub
    Else
        KeylinkID = PE_CLng(KeylinkID)
    End If
	Select Case Action
		Case "Show"
			Stat = 1
		Case "Dis"
			Stat = 0
		Case Else
			Stat = 0
	End Select
    Conn.Execute ("Update PW_KeyLink set isUse=" & Stat & " where ID=" & KeylinkID & "")

    Call Main
End Sub


%>