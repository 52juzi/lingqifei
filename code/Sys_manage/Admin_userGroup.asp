<!--#include file="Admin_Common.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1 检查
Const PurviewLevel_Others = "User"   '其他权限

Dim rsUserGroup, GroupSetting

GroupID = PE_CLng(GetValue("GroupID"))

Response.Write "<html>" & vbCrLf
Response.Write "<head>" & vbCrLf
Response.Write "<title>会员组管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("会 员 组 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'>管理导航：</td>" & vbCrLf
Response.Write "    <td height='30'><a href='Admin_UserGroup.asp'>会员组管理首页</a>&nbsp;|&nbsp;<a href='Admin_UserGroup.asp?Action=Add'>新增会员组</a> </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf

Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveGroup
Case "Del"
    Call Del
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn



Sub main()
    Dim Sql
    Sql = "SELECT GroupID,GroupName,GroupIntro FROM PW_UserGroup ORDER by GroupID asc"
    Set rsUserGroup = Server.CreateObject("adodb.recordset")
    rsUserGroup.Open Sql, Conn, 1, 1
    Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
    Response.Write "  <tr align='center' height='22' class='title'>" & vbCrLf
    Response.Write "    <td width='35'>ID</td>" & vbCrLf
    Response.Write "    <td width='120'>会员组名</td>" & vbCrLf
    Response.Write "    <td>会员组简介</td>" & vbCrLf
    Response.Write "    <td width='60'>会员数量</td>" & vbCrLf
    Response.Write "    <td width='150'>操 作</td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
	If rsUserGroup.BOF And rsUserGroup.EOF Then
		Response.write "<tr align='center' height='22'><td colspan='10'>没有任何会员组！</td></tr>"
	Else
		Do While Not rsUserGroup.EOF
			Response.Write "     <tr align='center' class='tdbg' onMouseOut=""this.className='tdbg'"" onMouseOver=""this.className='tdbg2'"">" & vbCrLf
			Response.Write "    <td width='35'>" & rsUserGroup("GroupID") & "</td>" & vbCrLf
			Response.Write "    <td width='120'>" & rsUserGroup("GroupName") & "</td>" & vbCrLf
			Response.Write "    <td align='left'>" & rsUserGroup("GroupIntro") & "</td>" & vbCrLf
			Response.Write "    <td width='60'>" & GetGroupNum(rsUserGroup("GroupID")) & "</td>" & vbCrLf
			Response.Write "    <td width='150'><a href='Admin_UserGroup.asp?Action=Modify&GroupID=" & rsUserGroup("GroupID") & "'>修改</a>"
			Response.Write " | <a href='Admin_UserGroup.asp?Action=Del&GroupID=" & rsUserGroup("GroupID") & "' onclick=""return confirm('确实要删除此会员组吗？');"">删除</a>" & vbCrLf
			Response.Write " | <a href='Admin_User.asp?SearchType=11&GroupID=" & rsUserGroup("GroupID") & "'>列出会员</a></td>"
			Response.Write "    </tr>" & vbCrLf
			rsUserGroup.MoveNext
		Loop
		rsUserGroup.Close
		Set rsUserGroup = Nothing
	End if
	
	Response.Write "</table>" & vbCrLf
End Sub









Sub ShowJS_Check()
    Response.Write "<script language=javascript>" & vbCrLf
    Response.Write "function CheckSubmit(){" & vbCrLf
    Response.Write "  if(document.form1.GroupName.value==''){" & vbCrLf
    Response.Write "      alert('会员组名称不能为空！');" & vbCrLf
    Response.Write "   document.form1.GroupName.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Add()
    Call ShowJS_Check
    Response.Write "<form method='post' action='Admin_UserGroup.asp' name='form1' onSubmit='javascript:return CheckSubmit();'>" & vbCrLf
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
    Response.Write "    <tr class='title'>" & vbCrLf
    Response.Write "      <td height='22' colspan='2'><div align='center'>新 增 会 员 组</div></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='15%' class='tdbg5' align='right'>会员组名称：</td>" & vbCrLf
    Response.Write "      <td><input name='GroupName' type='text' id='GroupName' size='20' maxlength='20'><font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='15%' class='tdbg5' align='right'>会员组说明：</td>" & vbCrLf
    Response.Write "      <td><input name='GroupIntro' type='text' id='GroupIntro' size='50' maxlength='200'><font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td colspan='2'>" & vbCrLf
    Response.Write "        <table width='100%' border='0' cellspacing='10' cellpadding='0'>" & vbCrLf
    Response.Write "            <tr>" & vbCrLf
    Response.Write "                <td align='center'>" & vbCrLf
    Response.Write "                    <input type='hidden' name='Action' value='SaveAdd'>" & vbCrLf
    Response.Write "                    <input type='submit' value='添加会员组'>" & vbCrLf
    Response.Write "                    <input type='button' name='cancel' value=' 取 消 ' onClick=""JavaScript:window.location.href='Admin_UserGroup.asp'"">" & vbCrLf
    Response.Write "                </td>" & vbCrLf
    Response.Write "            </tr>" & vbCrLf
	Response.Write "   		</table>" & vbCrLf
	Response.Write "   	</td>" & vbCrLf
	Response.Write "   </tr>" & vbCrLf
    Response.Write "  </table>" & vbCrLf
    Response.Write "</form>" & vbCrLf
End Sub


Sub Modify()
    If GroupID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定GroupID</li>"
        Exit Sub
    End If
    Set rsUserGroup = Conn.Execute("SELECT GroupID,GroupName,GroupIntro FROM PW_UserGroup WHERE GroupID=" & GroupID & "")
    If rsUserGroup.BOF And rsUserGroup.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的会员组</li>"
        rsUserGroup.Close
        Set rsUserGroup = Nothing
        Exit Sub
    End If
    Call ShowJS_Check
    Response.Write "<form method='post' action='Admin_UserGroup.asp' name='form1' onSubmit='javascript:return CheckSubmit();'>" & vbCrLf
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
    Response.Write "    <tr class='title'>" & vbCrLf
    Response.Write "      <td height='22' colspan='3'><div align='center'>修 改 会 员 组 设 置</div></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='15%' class='tdbg5' align='right'>会员组名称：</td>" & vbCrLf
    Response.Write "      <td><input name='GroupName' type='text' id='GroupName' value='" & rsUserGroup("GroupName") & "' size='20' maxlength='20'><font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='15%' class='tdbg5' align='right'>会员组说明：</td>" & vbCrLf
    Response.Write "      <td><input name='GroupIntro' type='text' id='GroupIntro' value='" & rsUserGroup("GroupIntro") & "' size='50' maxlength='200'><font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td colspan='2'>" & vbCrLf
    Response.Write "        <table width='100%' border='0' cellspacing='10' cellpadding='0'>" & vbCrLf
    Response.Write "            <tr>" & vbCrLf
    Response.Write "                <td align='center'>" & vbCrLf
    Response.Write "                    <input type='hidden' name='GroupID' value='" & rsUserGroup("GroupID") & "'>" & vbCrLf
    Response.Write "                    <input type='hidden' name='Action' value='SaveModify'>" & vbCrLf
    Response.Write "                    <input type='submit' value='保存修改结果'>" & vbCrLf
    Response.Write "                    <input type='button' name='cancel' value=' 取 消 ' onClick=""JavaScript:window.location.href='Admin_UserGroup.asp'"">" & vbCrLf
    Response.Write "                </td>" & vbCrLf
    Response.Write "            </tr>" & vbCrLf
	Response.Write "   		</table>" & vbCrLf
	Response.Write "   	</td>" & vbCrLf
	Response.Write "   </tr>" & vbCrLf
    Response.Write "  </table>" & vbCrLf
    Response.Write "</form>" & vbCrLf
End Sub


Sub Del()
    Conn.Execute ("delete from PW_UserGroup where GroupID=" & GroupID & "")
    Call main
End Sub


Sub SaveGroup()
	Dim GroupIntro
    GroupName = GetForm("GroupName")
    GroupIntro = GetForm("GroupIntro")
    FoundErr = False
    If GroupName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>会员组名称不能为空</li>"
        Exit Sub
    Else
        GroupName = ReplaceBadChar(GroupName)
    End If
    If FoundErr = True Then
        Exit Sub
    End If
    Set rsUserGroup = Server.CreateObject("Adodb.Recordset")
    If Action = "SaveAdd" Then
        rsUserGroup.Open "SELECT * FROM PW_UserGroup WHERE GroupName='" & GroupName & "'", Conn, 1, 3
        If Not (rsUserGroup.BOF And rsUserGroup.EOF) Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>数据库中已有同名会员组！</li>"
        Else
            rsUserGroup.addnew
			GroupID = GetNewID("PW_UserGroup", "GroupID")
            rsUserGroup("GroupID") = GroupID
        End If
    Else
        rsUserGroup.Open "SELECT * FROM PW_UserGroup WHERE GroupID=" & GroupID, Conn, 1, 3
        If rsUserGroup.BOF And rsUserGroup.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>数据库没有发现此会员组！</li>"
        End If
    End If
    If FoundErr = True Then
        rsUserGroup.Close
        Set rsUserGroup = Nothing
        Exit Sub
    End If
    rsUserGroup("GroupName") = GroupName
    rsUserGroup("GroupIntro") = GroupIntro
    rsUserGroup.Update
    rsUserGroup.Close
    Set rsUserGroup = Nothing
    Call main

End Sub




Function GetGroupNum(iGroupID)
    If Not IsNumeric(iGroupID) Then Exit Function
    Dim rsUserGroup
    Set rsUserGroup = Conn.Execute("SELECT Count(UserID) FROM PW_User WHERE GroupID=" & iGroupID & "")
    If IsNull(rsUserGroup(0)) Then
        GetGroupNum = 0
    Else
        GetGroupNum = rsUserGroup(0)
    End If
    Set rsUserGroup = Nothing
End Function



%>